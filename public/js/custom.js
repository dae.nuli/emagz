$(function () {

	// Delete Confirmation
	$(document).on('click','.delete', function(e) {
		e.preventDefault();
    	var CSRF_TOKEN = $('input[name="_token"]').attr('value');
    	var METHOD = $('input[name="_method"]').attr('value');
	    var TRClose = $(this).closest("tr");
   		var url = $(this).closest('.form').attr('action');;
	   	TRClose.css("background-color", "#FF3700");
	   $.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
        swal({
            title: "Apa Anda Yakin ?",
            text: "Menghapus data ini !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya , Hapus !",
            cancelButtonText: "Tidak ",
        }, function(isConfirm){
        	if (isConfirm) {
               $.ajax({
                    url: url,
                    type: 'post',
                    dataType: 'json',
                    data: {
                        _method: 'DELETE',
                        submit: true
                    },
			        success : function(data){
			        if(data.msg) {
			            table.ajax.reload();
			          }
			        }
                }).always(function(data) {
                    // $('#dataTableBuilder').DataTable().draw(false);
                });

           	} else {
                TRClose.css("background-color", "transparent");
            }
        });
	});

    $(document).on('click','.delete-method', function(e) {
        e.preventDefault();
        var CSRF_TOKEN = $('input[name="_token"]').attr('value');
        // var TRClose = $(this).closest("tr");
        var url = $(this).closest('.delete-method').attr('href');;
        // TRClose.css("background-color", "#FF3700");

        swal({
            title: "Apa Anda Yakin ?",
            text: "Menghapus data ini !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya , Hapus !",
            cancelButtonText: "Tidak ",
        }, function(isConfirm){
            if (isConfirm) {
                window.location.href = url;
               // $.ajax({
               //      url: url,
               //      type: 'get',
               //      dataType: 'json',
               //      success : function(data){
               //      if(data.msg) {
               //              e.preventDefault(); $(this).parents('.add-dinamic div').remove();
               //          // table.ajax.reload();
               //        }
               //      }
               //  }).always(function(data) {
               //      // $('#dataTableBuilder').DataTable().draw(false);
               //  });

            } else {
                // TRClose.css("background-color", "transparent");
            }
        });
    });

    $(document).on('click','.submit-button', function(e) {
        if ($('input[name="status"]:checked').val() == 2) {
            e.preventDefault();
            // var CSRF_TOKEN = $('input[name="_token"]').attr('value');
            // var url = $(this).closest('.delete-method').attr('href');;
            var form = $(this).parents('form');
            var price = $(this).data('price');
            var img = $(this).data('image');
            var content = '<img style="margin: 0 auto;" class="img-responsive thumbnail" width="300" src="'+img+'">';
            var nominal = '<b>Top Up Sebesar Rp '+price+'</b>';
            swal({
                title: "Apa Data TOP UP Sudah Benar ?",
                text: nominal+content,
                // text: "TopUp disetujui !",
                type: "info",
                showCancelButton: true,
                html: true,
                // confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya , Setujui !",
                cancelButtonText: "Tidak ",
            }, function(isConfirm){
                if (isConfirm) form.submit();
            });
        }
    });

    $(document).on('click','.publish', function(e) {
        e.preventDefault();
        var CSRF_TOKEN = $('input[name="_token"]').attr('value');
        var TRClose = $(this).closest("tr");
        var url = $(this).closest('.publish').attr('href');;
        TRClose.css("background-color", "#FF3700");

        swal({
            title: "Apa Anda Yakin ?",
            text: "Mempublish data ini !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya , Publish !",
            cancelButtonText: "Tidak ",
        }, function(isConfirm){
            if (isConfirm) {
               $.ajax({
                    url: url,
                    type: 'get',
                    dataType: 'json',
                    success : function(data){
                    if(data.msg) {
                        table.ajax.reload();
                      }
                    }
                }).always(function(data) {
                    // $('#dataTableBuilder').DataTable().draw(false);
                });

            } else {
                TRClose.css("background-color", "transparent");
            }
        });
    });

    $(document).on('click','.unpublish', function(e) {
        e.preventDefault();
        var CSRF_TOKEN = $('input[name="_token"]').attr('value');
        var TRClose = $(this).closest("tr");
        var url = $(this).closest('.unpublish').attr('href');;
        TRClose.css("background-color", "#FF3700");

        swal({
            title: "Apa Anda Yakin ?",
            text: "Unpublish data ini !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya , UnPublish !",
            cancelButtonText: "Tidak ",
        }, function(isConfirm){
            if (isConfirm) {
               $.ajax({
                    url: url,
                    type: 'get',
                    dataType: 'json',
                    success : function(data){
                    if(data.msg) {
                        table.ajax.reload();
                      }
                    }
                }).always(function(data) {
                    // $('#dataTableBuilder').DataTable().draw(false);
                });

            } else {
                TRClose.css("background-color", "transparent");
            }
        });
    });

	// Datatable Search on Enter
	$(".dataTables_filter input")
	.unbind()
	.bind('keyup change', function(e) {
	    if (e.keyCode == 13 || this.value == "") {
	        table
	            .search(this.value)
	            .draw();
	    }
	}); 
});
