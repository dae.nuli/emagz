@extends('admin.layouts.admin_template')

@section('head-script')
  <!-- DataTables -->
    <!-- JQuery DataTable Css -->
  <link href="{{ asset('bower_components/adminbsb-materialdesign/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
 <style type="text/css">
   .dataTables_wrapper .dt-buttons{
      margin-bottom: 20px;
   }
 </style>
  <link href="{{ asset('bower_components/adminbsb-materialdesign/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css' )}}" rel="stylesheet" />
@endsection

@section('foot-script')
  <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/momentjs/moment.js' )}}"></script> 
  <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/momentjs/moment-with-locales.min.js' )}}"></script>
 <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js' )}}"></script>
 <script type="text/javascript">
   
     
    $('.datepicker').bootstrapMaterialDatePicker({
        format: 'YYYY-MM-DD',
        clearButton: true,
        weekStart: 1,
        lang : 'id',
        time: false
    });
    
 </script>
  @include('admin.layouts.part.dt_script')
  <script>



  var table;
  $(function () {
    $.fn.dataTable.ext.errMode = 'none';
      table = $(".js-basic-example").DataTable({
          "dom": 'Bfrtip',
          "responsive": true,
          "processing": true,
          "serverSide": true,
          "ajax": {
                url: '{{ $ajax }}',
                data: function (d) {
                    d.filter = true;
                    d.tgl1 = $('input[name=tgl1]').val();
                    d.tgl2 = $('input[name=tgl2]').val();
                }
            },
          "paging":false,
          "searching": false,
          "order": [],         
          "columns": [
              {data: 'id', searchable: false, orderable: false},
              {data: 'tanggal', searchable: false, orderable: false},
              {data: 'code', searchable: false, orderable: false},
              {data: 'pembeli', searchable: false, orderable: false},
              {data: 'buletin', searchable: false, orderable: false},
              {data: 'jumlah', searchable: false, orderable: false,
             render: $.fn.dataTable.render.number( '.', ',', 0, 'Rp. ' ) }
            
          ],
          columnDefs: [{
            "targets": 0,
            "searchable": false,
            "orderable": false,
            "data": null,

            // "title": 'No.',
            "render": function (data, type, full, meta) {
                return meta.settings._iDisplayStart + meta.row + 1; 
            }
          }],
          "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
            // Total over all pages
            total_ID = api.column(5).data().reduce( function (a, b) {
                return ~~a + ~~b;
            }, 0 );
          
           var numFormat  = $.fn.dataTable.render.number( '.', ',', 0, 'Rp. ' ).display;
            $( api.column(5).footer()).html(
                numFormat(total_ID)
             // $.fn.dataTable.render.number( total_ID,'.', ',', 0, 'Rp. ' );
            );
            
        }  ,
          buttons: [
            {
              extend: 'excel',
              footer: true,
              exportOptions: {
                columns: [0, 1, 2,3,4,5]
              }
            },
            {
              extend: 'pdf',
              footer: true,
              exportOptions: {
                columns: [0, 1, 2,3,4,5]
              }
            },
            {
              extend: 'print',
              footer: true,
              exportOptions: {
                columns: [0, 1, 2,3,4,5]
              }

            },
          ]
      });

        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

  });
  </script>
   
@endsection

@section('content')
<div class="card">
  <div class="header">
     <div class="row ">
     <form action="{{ $ajax }}" method="GET" id="search-form">
        {{ Form::hidden('filter',true) }}
        <div class="col-sm-4">
            <div class="form-group">
              <div class="form-line">
                  <input type="text" name="tgl1" class="datepicker form-control" placeholder="Pilih Tanggal Awal  "  required="required" > 
              </div>
          </div>

        </div>
        <div class="col-sm-1">
          <h5>s/d</h5>
        </div>
        <div class="col-sm-4">
         <div class="form-group">
            <div class="form-line">
                  <input type="text" name="tgl2" class="datepicker form-control" placeholder="Pilih Tanggal Akhir"  required="required" >
              </div>
              </div>
        </div>
         <div class="col-sm-3">
            <input type="submit" class="btn btn-success waves-effect  " value="Tampilkan"> 
         </div>
      </form>
    </div>
  </div>
  
      
 
  <div class="body">

 
    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
      <thead>
      <tr>
        <th width="20">No</th>
        <th>Tanggal </th>
        <th>Kode Transaksi</th>
        <th>Pembeli </th>
        <th>Nama Buletin </th>
        <th>Jumlah </th>
      </tr>
      </thead>
      <tbody>
      </tbody>

      <tfoot>
        <tr>
          <th>  </th>
          <th>  </th>
          <th>  </th>
          <th>  </th>
          <th class="text-right">Total  </th>
          <th>  </th>
        </tr>
      </tfoot>
    </table>
  </div>
</div>
@endsection