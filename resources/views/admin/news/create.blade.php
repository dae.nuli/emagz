@extends('admin.layouts.admin_template')

@section('head-script')
  <link href="{{ asset('bower_components/adminbsb-materialdesign/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">
  <link href="{{ asset('bower_components/adminbsb-materialdesign/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css' )}}" rel="stylesheet" />
  <link href="{{ asset('bower_components/summernote/dist/summernote.css') }}" rel="stylesheet">
  <link href="{{ asset('bower_components/adminbsb-materialdesign/plugins/dropzone/dropzone.css') }}" rel="stylesheet">
@endsection

@section('foot-script')
  <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/jquery-validation/jquery.validate.js') }}"></script>
  {{-- // <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/tinymce/tinymce.js') }}"></script> --}}
  <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
  <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/momentjs/moment.js' )}}"></script>
  <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js' )}}"></script>
  {{-- // <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/ckeditor/ckeditor.js') }}"></script> --}}
  <script src="{{ asset('bower_components/summernote/dist/summernote.js') }}"></script>
  {{-- // <script type="text/javascript" src="{{ asset('packages/barryvdh/elfinder/js/standalonepopup.min.js') }}"></script> --}}
  <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/dropzone/dropzone.js') }}"></script>
  <script src="{{ asset('js/validation.js') }}"></script>
  <script type="text/javascript">
  $(function () {
      //TinyMCE
      // tinymce.init({
      //     selector: "textarea#tinymce",
      //     theme: "modern",
      //     height: 300,
      //     plugins: [
      //         'advlist autolink lists link image charmap print preview hr anchor pagebreak',
      //         'searchreplace wordcount visualblocks visualchars code fullscreen',
      //         'insertdatetime media nonbreaking save table contextmenu directionality',
      //         'emoticons template paste textcolor colorpicker textpattern imagetools'
      //     ],
      //     toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
      //     toolbar2: 'print preview media | forecolor backcolor emoticons',
      //     image_advtab: true
      // });
      // tinymce.suffix = ".min";
      // tinyMCE.baseURL = '{{asset("bower_components/adminbsb-materialdesign/plugins/tinymce")}}';
      // CKEDITOR.replace('ckeditor');
      // CKEDITOR.config.height = 300;
      // CKEDITOR.config.filebrowserBrowseUrl = "{{ asset('/elfinder/ckeditor') }}";
      // CKEDITOR.config.extraPlugins = 'oembed,widget';
      // Dropzone.autoDiscover = false;
      // Dropzone.options.frmFileUpload = {
      //         paramName: "file",
      //         maxFiles: 1
      //     };
      // Dropzone.autoDiscover = false;
      // $('div#frmFileUpload').dropzone({
      //   paramName: 'file_foto',
      //   autoProcessQueue: false,
      //   maxFiles: 1,
      //   parallelUploads: 1,
      //   url:'.',
      //   addRemoveLinks: true,
      //   init: function() {
      //     var myDropzone = this;

      //     // First change the button to actually tell Dropzone to process the queue.
      //     this.element.querySelector("button[type=submit]").addEventListener("click", function(e) {
      //       // Make sure that the form isn't actually being sent.
      //       e.preventDefault();
      //       e.stopPropagation();
      //       myDropzone.processQueue();
      //     });

      //     // Listen to the sendingmultiple event. In this case, it's the sendingmultiple event instead
      //     // of the sending event because uploadMultiple is set to true.
      //     this.on("sendingmultiple", function() {
      //       // Gets triggered when the form is actually being sent.
      //       // Hide the success button or the complete form.
      //     });
      //     this.on("successmultiple", function(files, response) {
      //       // Gets triggered when the files have successfully been sent.
      //       // Redirect user or notify of success.
      //     });
      //     this.on("errormultiple", function(files, response) {
      //       // Gets triggered when there was an error sending the files.
      //       // Maybe show form again, and notify user of error
      //     });
      //   }
      // })
Dropzone.options.frmFileUpload = { // The camelized version of the ID of the form element

  // The configuration we've talked about above
  autoProcessQueue: false,
  maxFiles: 1,
  parallelUploads: 1,
  addRemoveLinks: true,
  paramName: 'file_foto',

  // The setting up of the dropzone
  init: function() {
    var myDropzone = this;

    // First change the button to actually tell Dropzone to process the queue.
    this.element.querySelector("button[type=submit]").addEventListener("click", function(e) {
      // Make sure that the form isn't actually being sent.
      e.preventDefault();
      e.stopPropagation();
      myDropzone.processQueue();
    });

    // Listen to the sendingmultiple event. In this case, it's the sendingmultiple event instead
    // of the sending event because uploadMultiple is set to true.
    this.on("sending", function(file, xhr, formData) {
      // Gets triggered when the form is actually being sent.
      // Hide the success button or the complete form.
      // $("form").find("input").each(function(){
      // formData.append("content", $("#summernote").summernote('code'));
          // formData.append($(this).attr("name"), $(this).val());
          // console.log($(this).attr("name")+'-'+$(this).val());
      // });
      // formData.append('content', jQuery("#summernote").summernote('code'));
      // formData.append("title", 'cx');
      // formData.append('title', jQuery('#title').val());

      // formData.append("tag", $('input[name="tag"]').val());
      // formData.append("content", $("#summernote").summernote('code'));
      // formData.append("date_published", $('input[name="date_published"]').val());
      // formData.append("is_published", $('input[name="is_published"]').val());
    });
    this.on("success", function(files, response) {
      console.log('sds');
      // Gets triggered when the files have successfully been sent.
      // Redirect user or notify of success.
    });
    this.on("error", function(files, response) {
      // Gets triggered when there was an error sending the files.
      // Maybe show form again, and notify user of error
    });
  }

}
      // var myDropzone = new Dropzone("#frmFileUpload");
      // myDropzone.on("sending", function(file, xhr, formData) {
      //   // Will send the filesize along with the file as POST data.
      //   formData.append("filesize", file.size);
      // });
      $('.datetimepicker').bootstrapMaterialDatePicker({
          format: 'YYYY/MM/DD HH:mm',
          clearButton: true,
          weekStart: 1
      });
      $('#summernote').summernote({
        height: 300,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null,
      });
  $("#imgInp").change(function(){
      readURL(this);
  });
  function readURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();
          
          reader.onload = function (e) {
              $('#blah').attr('src', e.target.result);
              $('#blah').show();
          }
          
          reader.readAsDataURL(input.files[0]);
      }
  }
  
  });
  </script>
@endsection

@section('content')
<div class="card">
  <div class="header">
      <h2>{{$title}}</h2>
      <ul class="header-dropdown m-r--5">
          <li class="dropdown">
          </li>
      </ul>
  </div>
  <div class="body">
    {!! form_start($form, ['class' => 'form-horizontal']) !!}
    {{-- {!! form_start($form, ['class' => 'form-horizontal dropzone', 'id' => 'frmFileUpload']) !!} --}}
    {!! form_rest($form) !!}
    <div class="row clearfix">
        <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
            <a href="{{$url}}" class="btn btn-default btn-lg m-t-15 waves-effect">Cancel</a>
            <button  type="submit" class="btn btn-primary btn-lg m-t-15 waves-effect" id="submit-all">Submit</button>
        </div>
    </div>
    {!! form_end($form) !!}
  </div>
</div>
@endsection