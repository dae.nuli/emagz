<div class="row clearfix">
  <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
    <label for="attachment">Attachment</label>
  </div>
  <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
    <div class="form-group">
      <div class="form-line" id="aniimated-thumbnials">
        <a href="{{ asset('topupattachment/'.$options['value']) }}" data-sub-html="Image">
            <img class="img-responsive thumbnail" width="200" src="{{ asset('topupattachment/'.$options['value']) }}">
        </a>
        {{-- <img id="blah" src="{{ asset('topupattachment/'.$options['value']) }}" alt="" width="200" /> --}}
      </div>
    </div>
  </div>
</div>