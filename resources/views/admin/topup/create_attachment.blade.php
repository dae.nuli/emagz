<div class="row clearfix">
  <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
    <label for="attachment">Attachment</label>
  </div>
  <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
    <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="attachment" type="file" id="imgInp">
      </div>
    </div>
  </div>
</div>