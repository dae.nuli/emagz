@extends('admin.layouts.admin_template')

@section('head-script')
  <link href="{{ asset('bower_components/adminbsb-materialdesign/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">
  <link href="{{ asset('bower_components/adminbsb-materialdesign/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css' )}}" rel="stylesheet" />
  <!-- Light Gallery Plugin Css -->
  <link href="{{ asset('bower_components/adminbsb-materialdesign/plugins/light-gallery/css/lightgallery.css') }}" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/adminbsb-materialdesign/plugins/sweetalert/sweetalert.css') }}">
@endsection

@section('foot-script')
  <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/sweetalert/sweetalert.min.js') }}"></script>
  <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/light-gallery/js/lightgallery-all.js') }}"></script>
  <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/jquery-validation/jquery.validate.js') }}"></script>
  <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/momentjs/moment.js' )}}"></script>
  <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js' )}}"></script>
  <script src="{{ asset('js/validation.js') }}"></script>
  <script src="{{ asset('js/custom.js') }}"></script>
  <script type="text/javascript">
  $(function () {
      $('input[type="text"]').attr('disabled', '');
      // $('input[name=status]').removeAttr('disabled', '');
      // $('select[name="bp_user_id"]').attr('disabled', '');
      // $('select[name="bank_id"]').attr('disabled', '');

      $('.datetimepicker').bootstrapMaterialDatePicker({
          format: 'YYYY/MM/DD HH:mm',
          clearButton: true,
          weekStart: 1
      });
      $('#aniimated-thumbnials').lightGallery({
          thumbnail: true,
          selector: 'a'
      });
      $("#imgInp").change(function(){
          readURL(this);
      });
      function readURL(input) {
          if (input.files && input.files[0]) {
              var reader = new FileReader();
              
              reader.onload = function (e) {
                  $('#blah').attr('src', e.target.result);
                  $('#blah').show();
              }
              
              reader.readAsDataURL(input.files[0]);
          }
      }
      if ($('input[name="status"]:checked').val() == 2) {
        $('input[name="status"]').attr('disabled', '');
      }
  
  });
  </script>
@endsection

@section('content')
<div class="card">
  <div class="header">
      <h2>{{$title}}</h2>
      <ul class="header-dropdown m-r--5">
          <li class="dropdown">
          </li>
      </ul>
  </div>
  <div class="body">
    {!! form_start($form, ['class' => 'form-horizontal']) !!}
    {!! form_rest($form) !!}
    <div class="row clearfix">
        <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
            <a href="{{$url}}" class="btn btn-default btn-lg m-t-15 waves-effect">Cancel</a>
            @if($users->status == 0)
              <button  type="submit" data-price="{{number_format($users->amount, 0, '', '.')}}" data-image="{{ asset('topupattachment/'.$users->file_attachment) }}" class="btn btn-primary btn-lg m-t-15 waves-effect submit-button" id="submit-all">Submit</button>
            @endif
        </div>
    </div>
    {!! form_end($form) !!}
  </div>
</div>
@endsection