@extends('admin.layouts.admin_template')

@section('head-script')
  <link href="{{ asset('bower_components/adminbsb-materialdesign/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">
  <link href="{{ asset('bower_components/adminbsb-materialdesign/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css' )}}" rel="stylesheet" />
@endsection

@section('foot-script')
  <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/jquery-validation/jquery.validate.js') }}"></script>
  <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/momentjs/moment.js' )}}"></script>
  <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js' )}}"></script>
  <script src="{{ asset('js/validation.js') }}"></script>
  <script type="text/javascript">
  $(function () {
    $('.datetimepicker').bootstrapMaterialDatePicker({
        format: 'YYYY/MM/DD HH:mm',
        clearButton: true,
        weekStart: 1
    });

    $("#imgInp").change(function(){
        readURL(this);
    });
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
                $('#blah').show();
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
  });
  </script>
@endsection

@section('content')

{{Form::open(array('url'=>$action, 'method'=>$method, 'class'=>'form-horizontal', 'files' => true))}}
<div class="card">
  <div class="header">
      <h2>{{$title}}</h2>
      <ul class="header-dropdown m-r--5">
          <li class="dropdown">
          </li>
      </ul>
  </div>
  <div class="body">
    {!! form_rest($topUpForm) !!}
    <div class="row clearfix">
      <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
        <label for="file_logo">Logo</label>
      </div>
      <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
        <div class="form-group">
          <div class="form-line">
            <input class="form-control" name="file_logo" type="file" id="imgInp">
            <img id="blah" src="{{ asset('logobank/'.$index->logo) }}" alt="" width="200" />
          </div>
        </div>
      </div>
    </div>
    <div class="row clearfix">
        <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
            <a href="{{$url}}" class="btn btn-default btn-lg m-t-15 waves-effect">Cancel</a>
            <button  type="submit" class="btn btn-primary btn-lg m-t-15 waves-effect" id="submit-all">Submit</button>
        </div>
    </div>
  </div>
</div>
{{Form::close()}}
@endsection