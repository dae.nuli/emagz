@extends('admin.layouts.admin_template')

@section('head-script')
  @parent
  <style type="text/css">
  .admin-chat{
    margin-left: 50px;
  }
  .customer-chat{
    margin-right: 50px;
  }
  </style>
@endsection

@section('foot-script')
  <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/jquery-validation/jquery.validate.js') }}"></script>
  <script src="{{ asset('js/validation.js') }}"></script>
@endsection

@section('content')

<div class="card">
  <div class="header">
      <h2>{{$title}}</h2>
  </div>
  <div class="body">
    <div class="media">
      <div class="media-body alert alert-info customer-chat">
          <h4 class="media-heading">{{$message->title}}</h4> 
          <small class="pull-right">{{$message->created_at}}</small>
          <i>({{isset($message->bpUser->full_name) ? $message->bpUser->full_name : 'customer'}})</i> : {{$message->content}}
          {{$message->content}}
      </div>
    </div>
    {{-- <div class="alert alert-info customer-chat">
      <h6>{{$message->title}}</h6>
      <small class="pull-right">{{$message->created_at}}</small>
      <i>({{isset($message->bpUser->full_name) ? $message->bpUser->full_name : 'customer'}})</i> : {{$message->content}}
    </div> --}}
    @foreach($reply as $row)
      @if($row->is_admin)
        <div class="alert alert-success admin-chat">
          <small class="pull-right">{{$row->created_at}}</small>
          <i>(Admin)</i> : {{$row->content}}
        </div>
      @else
        <div class="alert alert-info customer-chat">
          <small class="pull-right">{{$row->created_at}}</small>
          <i>({{$row->user($row->user_id, 0)}})</i> : {{$row->content}}
        </div>
      @endif
    @endforeach
  </div>
</div>

{{Form::open(array('url'=>$action,'method'=>$method,'class'=>'form-horizontal'))}}
<input class="form-control" name="msg_id" type="hidden" value="{{$message->id}}">
<div class="card">
  <div class="header">
      {{-- <h2>{{$title}}</h2> --}}
      <ul class="header-dropdown m-r--5">
          <li class="dropdown">
          </li>
      </ul>
  </div>
  <div class="body">
    <div class="row clearfix">
      <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
        <label for="content">Pesan</label>
      </div>
      <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
        <div class="form-group">
          <div class="form-line">
            <textarea class="form-control" required name="content" id="content" rows="5"></textarea>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="card">
  <div class="body">
    <div class="row clearfix">
        <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
            <a href="{{$url}}" class="btn btn-default btn-lg m-t-15 waves-effect">Cancel</a>
            <button  type="submit" class="btn btn-primary btn-lg m-t-15 waves-effect" id="submit-all">Submit</button>
        </div>
    </div>
  </div>
</div>
{{Form::close()}}
@endsection