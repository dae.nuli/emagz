<?php 
$uri = (request()->segment(1)=='en')?request()->segment(3):request()->segment(2);
?>
<!-- Menu -->
<div class="menu">
    <ul class="list">
        <li class="header">MAIN NAVIGATION</li>
        <li {{ ($uri=='home') ? "class=active" :'' }}>
            <a href="{{ url('admin/home') }}">
                <i class="material-icons">home</i>
                <span>Home</span>
            </a>
        </li>
        @if(auth()->user()->can('list_customer'))
        <li {{ ($uri=='customers') ? "class=active" :'' }}>
            <a href="{{ url('admin/customers') }}">
                <i class="material-icons">people</i>
                <span>Customers</span>
            </a>
        </li>
        @endif

        @if(auth()->user()->can('list_news'))
        <li {{ ($uri=='news') ? "class=active" :'' }}>
            <a href="{{ url('admin/news') }}">
                <i class="material-icons">library_books</i>
                <span>News</span>
            </a>
        </li>
        @endif

        @if(auth()->user()->can('list_buletin'))
        <li {{ ($uri=='buletin') ? "class=active" :'' }}>
            <a href="{{ url('admin/buletin') }}">
                <i class="material-icons">book</i>
                <span>Buletin</span>
            </a>
        </li>
        @endif

        @if(auth()->user()->can('list_comment'))
        <li {{ ($uri=='comment') ? "class=active" :'' }}>
            <a href="{{ url('admin/comment') }}">
                <i class="material-icons">comment</i>
                <span>Comment</span>
            </a>
        </li>
        @endif

        @if(auth()->user()->can('list_push'))
        <li {{ ($uri=='announcement') ? "class=active" :'' }}>
            <a href="{{ url('admin/announcement') }}">
                <i class="material-icons">notifications_active</i>
                <span>Pengumuman/Notifikasi</span>
            </a>
        </li>
        @endif

        @if(auth()->user()->can('list_term'))
        <li {{ ($uri=='term') ? "class=active" :'' }}>
            <a href="{{ url('admin/term') }}">
                <i class="material-icons">assignment</i>
                <span>Term and Condition</span>
            </a>
        </li>
        @endif

        @if(auth()->user()->can('list_privacy'))
        <li {{ ($uri=='privacy') ? "class=active" :'' }}>
            <a href="{{ url('admin/privacy') }}">
                <i class="material-icons">assignment</i>
                <span>Privacy Policy</span>
            </a>
        </li>
        @endif

        @if(auth()->user()->can('list_setting'))
         <li {{ ($uri=='setting') ? "class=active" :'' }}>
            <a href="{{ url('admin/setting') }}">
                <i class="material-icons">assignment</i>
                <span>Setting</span>
            </a>
        </li>
        @endif

        @if(auth()->user()->can('list_bank'))
         <li {{ ($uri=='bank') ? "class=active" :'' }}>
            <a href="{{ url('admin/bank') }}">
                <i class="material-icons">assignment</i>
                <span>BANK</span>
            </a>
        </li>
        @endif

        @if(auth()->user()->can('list_topup'))
         <li {{ ($uri=='topUp') ? "class=active" :'' }}>
            <a href="{{ url('admin/topUp') }}">
                <i class="material-icons">assignment</i>
                <span>TOPUP</span>
            </a>
        </li>
        @endif

        @if(auth()->user()->can('list_feedback'))
         <li {{ ($uri=='feedback') ? "class=active" :'' }}>
            <a href="{{ url('admin/feedback') }}">
                <i class="material-icons">markunread</i>
                <span>FEEDBACK</span>
            </a>
        </li>
        @endif

        @if(auth()->user()->can('list_slide'))
         <li {{ ($uri=='slide') ? "class=active" :'' }}>
            <a href="{{ url('admin/slide') }}">
                <i class="material-icons">assignment</i>
                <span>Marketing</span>
            </a>
        </li>
        @endif

        @if(auth()->user()->can('list_analytics'))
         <li {{ ($uri=='analytics') ? "class=active" :'' }}>
            <a href="{{ url('admin/analytics') }}">
                <i class="material-icons">show_chart</i>
                <span>ANALYTICS</span>
            </a>
        </li>
        @endif

        @if(auth()->user()->can('list_log'))
         <li {{ ($uri=='log') ? "class=active" :'' }}>
            <a href="{{ url('admin/log') }}">
                <i class="material-icons">assignment</i>
                <span>Log Admin</span>
            </a>
        </li>
        @endif
        
        <li class="header">MANAJEMEN</li>

        @if(auth()->user()->can('list_user'))
        <li {{ ($uri=='users') ? "class=active" :'' }}>
            <a href="{{ url('admin/users') }}">
                <i class="material-icons">people</i>
                <span>Admin Users</span>
            </a>
        </li>
        @endif

        <li>
            <a href="javascript:void(0);" class="menu-toggle waves-effect waves-block">
                <i class="material-icons">assessment</i>
                <span>Laporan</span>
            </a>
            <ul class="ml-menu" style="display: none;">
                <li>
                    <a href="{{ url('admin/laporan/buletin') }}" class=" waves-effect waves-block">
                         <i class="material-icons">book</i> 
                        <span>Pembelian Buletin</span>
                    </a>
                </li>
                <li>
                    <a href="{{ url('admin/laporan/topup') }}" class=" waves-effect waves-block">
                      <i class="material-icons">local_atm</i>
                        <span>Topup Masuk</span>
                    </a>
                </li>
                 
            </ul>
        </li>

{{--         @if(auth()->user()->can('list_role'))
        <li {{ ($uri=='role') ? "class=active" :'' }}>
            <a href="{{ url('admin/role') }}">
                <i class="material-icons">person</i>
                <span>Role</span>
            </a>
        </li>
        @endif

        @if(auth()->user()->can('list_module'))
        <li {{ ($uri=='module') ? "class=active" :'' }}>
            <a href="{{ url('admin/module') }}">
                <i class="material-icons">person</i>
                <span>Module</span>
            </a>
        </li>
        @endif
 --}}
        @if(auth()->user()->can('list_role') && auth()->user()->can('list_module'))
        <li {{ ($uri=='role' || $uri=='module') ? "class=active" :'' }}>
            <a href="javascript:void(0);" class="menu-toggle">
                <i class="material-icons">settings</i>
                <span>Setting Hak Akses</span>
            </a>
            <ul class="ml-menu">
                @if(auth()->user()->can('list_role'))
                <li {{ ($uri=='role') ? "class=active" :'' }}>
                    <a href="{{ url('admin/role') }}">Role</a>
                </li>
                @endif
                
                @if(auth()->user()->can('list_module'))
                <li {{ ($uri=='module') ? "class=active" :'' }}>
                    <a href="{{ url('admin/module') }}">Module</a>
                </li>
                @endif
            </ul>
        </li>
        @endif

        @if(auth()->user()->can('list_topuptype') && auth()->user()->can('list_topupstatus'))
        <li {{ ($uri=='topUpType' || $uri=='topUpStatus') ? "class=active" :'' }}>
            <a href="javascript:void(0);" class="menu-toggle">
                <i class="material-icons">settings</i>
                <span>Setting TopUp</span>
            </a>
            <ul class="ml-menu">
                @if(auth()->user()->can('list_topuptype'))
                <li {{ ($uri=='topUpType') ? "class=active" :'' }}>
                    <a href="{{ url('admin/topUpType') }}">Topup Type</a>
                </li>
                @endif
                
                @if(auth()->user()->can('list_topupstatus'))
                <li {{ ($uri=='topUpStatus') ? "class=active" :'' }}>
                    <a href="{{ url('admin/topUpStatus') }}">Topup Status</a>
                </li>
                @endif
            </ul>
        </li>
        @endif

    </ul>
</div>
<!-- #Menu -->
<!-- Footer -->
{{-- <div class="legal">
    <div class="copyright">
        &copy; 2016 <a href="javascript:void(0);">Emagz</a>.
    </div>
    <div class="version">
        <b>Version: </b> 1.0.4
    </div>
</div> --}}
<!-- #Footer -->
