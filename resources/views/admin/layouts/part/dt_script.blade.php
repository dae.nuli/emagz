
  <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
  <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
  <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
  <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
  <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
  <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
  <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
  <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
  <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>
  <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/sweetalert/sweetalert.min.js') }}"></script>
  {{-- // <script src="{{ asset('bower_components/adminbsb-materialdesign/js/pages/tables/jquery-datatable.js') }}"></script> --}}
  {{-- // <script src="{{ asset('bower_components/adminbsb-materialdesign/dist/js/custom.js') }}"></script> --}}