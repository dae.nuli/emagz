@extends('admin.layouts.admin_template')

@section('head-script')
  <!-- DataTables -->
    <!-- JQuery DataTable Css -->
  <link href="{{ asset('bower_components/adminbsb-materialdesign/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/adminbsb-materialdesign/plugins/sweetalert/sweetalert.css') }}">
@endsection

@section('foot-script')
  @include('admin.layouts.part.dt_script')
  <script>
  var table;
  // var tableNews;
  $(function () {
      table = $(".js-basic-example").DataTable({
          "dom": 'Bfrtip',
          "responsive": true,
          "processing": true,
          "serverSide": true,
          "ajax": '{{$buletin}}',
          "order": [[5, 'desc'], [4, 'desc']],
          "columns": [
              {data: 'id', searchable: false, orderable: false},
              {data: 'article', orderable: false},
              {data: 'buletin_title', orderable: false},
              {data: 'edition', searchable: false, orderable: false},
              {data: 'rating', searchable: false, orderable: true},
              {data: 'sold', searchable: false, orderable: true},
              {data: 'testimoni', searchable: false, orderable: false},
              // {data: 'action', searchable: false, orderable: false}
          ],
          columnDefs: [{
            "targets": 0,
            "searchable": false,
            "orderable": false,
            "data": null,
            // "title": 'No.',
            "render": function (data, type, full, meta) {
                return meta.settings._iDisplayStart + meta.row + 1; 
            }
          }],
          buttons: [
            {
              extend: 'excel',
              exportOptions: {
                columns: [0, 1, 2, 3, 4, 5, 6]
              }
            },
            {
              extend: 'pdf',
              exportOptions: {
                columns: [0, 1, 2, 3, 4, 5, 6]
              }
            },
            {
              extend: 'print',
              exportOptions: {
                columns: [0, 1, 2, 3, 4, 5, 6]
              }
            },
          ]
      });

      // tableNews = $(".js-basic-example-news").DataTable({
      //     "dom": 'Bfrtip',
      //     "responsive": true,
      //     "processing": true,
      //     "serverSide": true,
      //     "ajax": '{{$news}}',
      //     "order": [],
      //     "columns": [
      //         {data: 'id', searchable: false, orderable: false},
      //         {data: 'title', orderable: false},
      //         {data: 'date_published', searchable: false, orderable: false},
      //         {data: 'rating', searchable: false, orderable: false},
      //         {data: 'read', searchable: false, orderable: false},
      //         {data: 'testimoni', searchable: false, orderable: false},
      //     ],
      //     columnDefs: [{
      //       "targets": 0,
      //       "searchable": false,
      //       "orderable": false,
      //       "data": null,
      //       // "title": 'No.',
      //       "render": function (data, type, full, meta) {
      //           return meta.settings._iDisplayStart + meta.row + 1; 
      //       }
      //     }],
      //     buttons: [
      //       {
      //         extend: 'excel',
      //         exportOptions: {
      //           columns: [0, 1, 2, 3, 4, 5]
      //         }
      //       },
      //       {
      //         extend: 'pdf',
      //         exportOptions: {
      //           columns: [0, 1, 2, 3, 4, 5]
      //         }
      //       },
      //       {
      //         extend: 'print',
      //         exportOptions: {
      //           columns: [0, 1, 2, 3, 4, 5]
      //         }
      //       },
      //     ]
      // });

  });
  </script>
  <script src="{{ asset('js/custom.js') }}"></script>
@endsection

@section('content')
<div class="card">
  <div class="header">
      <h2>Buletin Terbaik</h2>
  </div>
  <div class="body">
    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
      <thead>
      <tr>
        <th width="20">No</th>
        <th>Judul Artikel</th>
        <th>Judul Buletin</th>
        <th>Edisi</th>
        <th>Rating</th>
        <th>Terbeli</th>
        <th>Jumlah Testimoni</th>
        {{-- <th>Actions</th> --}}
      </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
  </div>
</div>
{{-- <div class="card">
  <div class="header">
      <h2>Berita Terpopuler</h2>
  </div>
  <div class="body">
    <table class="table table-bordered table-striped table-hover js-basic-example-news dataTable">
      <thead>
      <tr>
        <th width="20">No</th>
        <th>Judul</th>
        <th>Tanggal Post</th>
        <th>Rating</th>
        <th>Terbaca</th>
        <th>Jumlah Testimoni</th>
      </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
  </div>
</div> --}}
@endsection