@extends('admin.layouts.admin_template')

@section('content')
    <!-- Widgets -->
    <div class="row clearfix">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-orange hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">person_add</i>
                </div>
                <div class="content">
                    <div class="text">TOTAL PENGGUNA</div>
                    <div class="number count-to" data-from="0" data-to="{{$totalCustomer}}" data-speed="1000" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-pink hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">attach_money</i>
                </div>
                <div class="content">
                    <div class="text">TOPUP</div>
                    <div class="number">{{number_format($totalTopup, 0, '', '.')}}</div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-cyan hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">markunread</i>
                </div>
                <div class="content">
                    <div class="text">NEW FEEDBACK</div>
                    <div class="number count-to" data-from="0" data-to="{{$totalMessage}}" data-speed="1000" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-light-green hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">forum</i>
                </div>
                <div class="content">
                    <div class="text">NEW COMMENTS</div>
                    <div class="number count-to" data-from="0" data-to="{{$totalComment}}" data-speed="1000" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="row clearfix">
        <!-- Task Info -->
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="card">
                <div class="header">
                    <h2>KONFIRMASI TOPUP</h2>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-hover dashboard-task-infos">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nama</th>
                                    <th>Total</th>
                                    <th>Gambar</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $nomor = 1;
                                ?>
                                @foreach($topUp as $top)
                                <tr>
                                    <td>{{$nomor++}}</td>
                                    <td>{{@$top->users->full_name}}</td>
                                    <td>Rp {{number_format($top->amount, 0, '', '.')}}</td>
                                    <td><img src="{{asset('topupattachment/'.$top->file_attachment)}}" width="50"></td>
                                    <td>
                                        {!!(auth()->user()->can('detail_topup')) ? "<a href=".route('admin.home.topup.show',$top->id)." class='btn btn-success btn-xs'>Detail</a>" : ''!!}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                            <a href="{{route('admin.topUp.index')}}" class="btn btn-info btn-lg m-t-15 waves-effect">Halaman Top Up</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Task Info -->
        <!-- Browser Usage -->
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="card">
                <div class="header">
                    <h2>BULETIN TERBAIK</h2>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-hover dashboard-task-infos">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Judul</th>
                                    <th>Penulis</th>
                                    <th>Download</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $nomor = 1;
                                ?>
                                @foreach($bestBuletin as $row)
                                @if(isset($row->buletin->getArtikel))
                                    <tr>
                                        <td>{{$nomor++}}</td>
                                        <td>{{isset($row->buletin->getArtikel->title)?str_limit($row->buletin->getArtikel->title, 15) : '-'}}</td>
                                        <td>{{isset($row->buletin->getArtikel->author)?$row->buletin->getArtikel->author : '-'}}</td>
                                        <td>{{$row->total_buletin}}</td>
                                    </tr>
                                @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                            <a href="{{route('admin.analytics.index')}}" class="btn btn-info btn-lg m-t-15 waves-effect">Halaman Analytics</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Browser Usage -->
    </div>

    <div class="row clearfix">
        <!-- Task Info -->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="header">
                    <h2>FEEDBACK</h2>
                </div>
                <div class="body">
                    @if(!empty($feed))
                    <div class="row clearfix">
                      <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                        <label>Dari</label>
                      </div>
                      <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                        <div class="form-group">
                          <div class="form-line">
                            <input class="form-control" disabled="disabled" type="text" value="{{isset($feed->bpUser->full_name) ? $feed->bpUser->full_name : '-'}}">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row clearfix">
                      <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                        <label>Email</label>
                      </div>
                      <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                        <div class="form-group">
                          <div class="form-line">
                            <input class="form-control" disabled="disabled" type="text" value="{{isset($feed->bpUser->email) ? $feed->bpUser->email : '-'}}">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row clearfix">
                      <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                        <label>Subject</label>
                      </div>
                      <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                        <div class="form-group">
                          <div class="form-line">
                            <input class="form-control" disabled="disabled" type="text" value="{{($feed->title) ? $feed->title : '-'}}">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row clearfix">
                      <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                        <label>Content</label>
                      </div>
                      <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                        <div class="form-group">
                          <div class="form-line">
                            <textarea class="form-control" rows="5" disabled="disabled">{{$feed->content}}</textarea>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                            <a href="{{route('admin.feedback.reply', $feed->id)}}" class="btn btn-primary btn-lg m-t-15 waves-effect">Detail</a>
                            <a href="{{route('admin.feedback.index')}}" class="btn btn-info btn-lg m-t-15 waves-effect">Halaman Feedback</a>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection


@section('foot-script')

    <!-- Jquery CountTo Plugin Js -->
    <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/jquery-countto/jquery.countTo.js') }}"></script>

    <!-- Morris Plugin Js -->
    <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/morrisjs/morris.js') }}"></script>

    <!-- ChartJs -->
    <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/chartjs/Chart.bundle.js') }}"></script>

    <!-- Flot Charts Plugin Js -->
    <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/flot-charts/jquery.flot.js') }}"></script>
    <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/flot-charts/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/flot-charts/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/flot-charts/jquery.flot.categories.js') }}"></script>
    <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/flot-charts/jquery.flot.time.js') }}"></script>

    <!-- Sparkline Chart Plugin Js -->
    <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/jquery-sparkline/jquery.sparkline.js') }}"></script>
    <script src="{{ asset('bower_components/adminbsb-materialdesign/js/pages/index.js') }}"></script>
@endsection