@extends('admin.layouts.admin_template')


@section('head-script')
  <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/adminbsb-materialdesign/plugins/sweetalert/sweetalert.css') }}">
  <link href="{{ asset('bower_components/summernote/dist/summernote.css') }}" rel="stylesheet">
@endsection

@section('foot-script')
  <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/jquery-validation/jquery.validate.js') }}"></script>
  <script src="{{ asset('bower_components/summernote/dist/summernote.js') }}"></script>
  <script src="{{ asset('js/validation.js') }}"></script>
  <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/sweetalert/sweetalert.min.js') }}"></script>
  <script src="{{ asset('js/custom.js') }}"></script>
  <script type="text/javascript">
        var max_fields      = 10; //maximum input boxes allowed
        var wrapper         = $(".add-dinamic"); //Fields wrapper
        var add_button      = $(".add-item"); //Add button ID
        
        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div class="card"><div class="header"><button type="button" class="btn btn-danger btn-circle waves-effect waves-circle waves-float pull-right delete-item"><i class="material-icons">clear</i></button></div><div class="body"><div class="row clearfix"><div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label"><label for="title_method" class="">Title</label></div><div class="col-lg-10 col-md-10 col-sm-8 col-xs-7"><div class="form-group"><div class="form-line"><input class="form-control" required="" name="title_method[]" type="text" id="title_method"></div></div></div></div><div class="row clearfix"><div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label"><label for="description_method" class="">Description</label></div><div class="col-lg-10 col-md-10 col-sm-8 col-xs-7"><div class="form-group"><div class="form-line"><textarea class="form-control summernote" required="" name="description_method[]" id="description_method"></textarea></div></div></div></div></div></div>').ready(function () {
                  $('.summernote').summernote({
                    height: 150,                 // set editor height
                    minHeight: null,             // set minimum height of editor
                    maxHeight: null,
                  });
                }); //add input box
            }
        });
        
        $(document).on("click",".delete-item", function(e){ //user click on remove text
            e.preventDefault(); $(this).parents('.add-dinamic div').remove(); x--;
        });

        $('.summernote').summernote({
          height: 150,                 // set editor height
          minHeight: null,             // set minimum height of editor
          maxHeight: null,
        });

        $("#imgInp").change(function(){
          readURL(this);
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                    $('#blah').show();
                    // $('#blah').attr('src', e.target.result);
                    // $('#blah').show();
                }
                
                reader.readAsDataURL(input.files[0]);
            }
        }
  </script>
@endsection

@section('content')

{{Form::open(array('url'=>$action,'method'=>$method,'class'=>'form-horizontal', 'files' => true))}}
<div class="card">
  <div class="header">
      <h2>{{$title}}</h2>
      <ul class="header-dropdown m-r--5">
          <li class="dropdown">
          </li>
      </ul>
  </div>
  <div class="body">
    <div class="row clearfix">
      <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
        <label for="bank_name">Nama Bank</label>
      </div>
      <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
        <div class="form-group">
          <div class="form-line">
            <input class="form-control" required value="{{$index->bank_name}}" name="bank_name" type="text" id="bank_name">
          </div>
        </div>
      </div>
    </div>
    <div class="row clearfix">
      <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
        <label for="account_name">Nama Akun</label>
      </div>
      <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
        <div class="form-group">
          <div class="form-line">
            <input class="form-control" required value="{{$index->account_name}}" name="account_name" type="text" id="account_name">
          </div>
        </div>
      </div>
    </div>
    <div class="row clearfix">
      <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
        <label for="file_logo">Logo</label>
      </div>
      <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
        <div class="form-group">
          <div class="form-line">
            <input class="form-control" name="file_logo" type="file" id="imgInp">
            <img id="blah" src="{{ asset('logobank/'.$index->logo) }}" alt="" width="200" />
          </div>
        </div>
      </div>
    </div>
    <div class="row clearfix">
      <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
        <label for="description">Keterangan</label>
      </div>
      <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
        <div class="form-group">
          <div class="form-line">
            <textarea class="form-control" cols="50" rows="10" required name="description" id="description">{{$index->description}}</textarea>
          </div>
        </div>
      </div>
    </div>
    <div class="row clearfix">
      <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
        <label for="account_no">Nomor Rekening</label>
      </div>
      <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
        <div class="form-group">
          <div class="form-line">
            <input class="form-control" required value="{{$index->account_no}}" name="account_no" type="text" id="account_no">
          </div>
        </div>
      </div>
    </div>
    <div class="row clearfix">
      <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
        <label for="is_active" class="">Status</label>
      </div>
        <div class="radio">
          <input id="is_active_1" name="is_active" type="radio" value="1" {{($index->is_active == 1) ? 'checked' : ''}}>
          <label for="is_active_1" class="col-lg-10 col-md-10 col-sm-8 col-xs-7">AKTIF</label>
        </div>
        <div class="radio">
          <input id="is_active_0" name="is_active" type="radio" value="0" {{($index->is_active == 0) ? 'checked' : ''}}>
          <label for="is_active_0" class="col-lg-10 col-md-10 col-sm-8 col-xs-7">TIDAK AKTIF</label>
        </div>
    </div>
  </div>
</div>
<div class="card">
  <div class="header">
      <h2>Metode</h2>
      <button type="button" class="btn btn-primary btn-circle waves-effect waves-circle waves-float pull-right add-item">
          <i class="material-icons">add</i>
      </button>
  </div>
</div>
@if(count($methods))
@foreach($methods as $key => $row)
<div class="card">
  <div class="header">
      <a type="button" href="{{ $url.'/deleteMethod/'.$row->id }}" class="btn btn-danger btn-circle waves-effect waves-circle waves-float pull-right delete-method">
          <i class="material-icons">clear</i>
      </a>
  </div>
  <div class="body">
    <input type="hidden" name="method_id[{{$key}}]" value="{{$row->id}}">
    <div class="row clearfix">
      <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
        <label for="title_method[]">Judul</label>
      </div>
      <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
        <div class="form-group">
          <div class="form-line">
            <input class="form-control" required name="title_method[{{$key}}]" value="{{$row->title}}" type="text" id="title_method[]">
          </div>
        </div>
      </div>
    </div>
    <div class="row clearfix">
      <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
        <label for="description_method[]">Description</label>
      </div>
      <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
        <div class="form-group">
          <div class="form-line">
            <textarea class="form-control summernote" required name="description_method[{{$key}}]" id="description_method[]">{{$row->description}}</textarea>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endforeach
@else
<div class="card">
  <div class="body">
    <div class="row clearfix">
      <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
        <label for="title_method[]" class="">Title</label>
      </div>
      <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
        <div class="form-group">
          <div class="form-line">
            <input class="form-control" required="" name="title_method[]" type="text" id="title_method[]" aria-required="true">
          </div>
        </div>
      </div>
    </div>
    <div class="row clearfix">
      <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
        <label for="description_method[]" class="">Description</label>
      </div>
      <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
        <div class="form-group">
          <div class="form-line">
            <textarea class="form-control summernote" required="" name="description_method[]" id="description_method[]" aria-required="true"></textarea>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endif

<div class="add-dinamic">

</div>

<div class="card">
  <div class="body">
    <div class="row clearfix">
        <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
            <a href="{{$url}}" class="btn btn-default btn-lg m-t-15 waves-effect">Cancel</a>
            <button  type="submit" class="btn btn-primary btn-lg m-t-15 waves-effect" id="submit-all">Submit</button>
        </div>
    </div>
  </div>
</div>
{{Form::close()}}
@endsection