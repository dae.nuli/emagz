@extends('admin.layouts.admin_template')

@section('head-script')
  <link href="{{ asset('bower_components/summernote/dist/summernote.css') }}" rel="stylesheet">
@endsection

@section('foot-script')
  <!-- Jquery DataTable Plugin Js -->
  <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/jquery-validation/jquery.validate.js') }}"></script>
  <script src="{{ asset('bower_components/summernote/dist/summernote.js') }}"></script>
  <script src="{{ asset('js/validation.js') }}"></script>
  <script type="text/javascript">
        var max_fields      = 10; //maximum input boxes allowed
        var wrapper         = $(".add-dinamic"); //Fields wrapper
        var add_button      = $(".add-item"); //Add button ID
        
        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div class="card"><div class="header"><button type="button" class="btn btn-danger btn-circle waves-effect waves-circle waves-float pull-right delete-item"><i class="material-icons">clear</i></button></div><div class="body"><div class="row clearfix"><div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label"><label for="title_method" class="">Title</label></div><div class="col-lg-10 col-md-10 col-sm-8 col-xs-7"><div class="form-group"><div class="form-line"><input class="form-control" required="" name="title_method[]" type="text" id="title_method"></div></div></div></div><div class="row clearfix"><div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label"><label for="description_method" class="">Description</label></div><div class="col-lg-10 col-md-10 col-sm-8 col-xs-7"><div class="form-group"><div class="form-line"><textarea class="form-control summernote" required="" cols="50" rows="10" name="description_method[]" id="description_method"></textarea></div></div></div></div></div></div>').ready(function () {
                  $('.summernote').summernote({
                    height: 150,                 // set editor height
                    minHeight: null,             // set minimum height of editor
                    maxHeight: null,
                  });
                }); //add input box
            }
        });
        
        $(document).on("click",".delete-item", function(e){ //user click on remove text
            e.preventDefault(); $(this).parents('.add-dinamic div').remove(); x--;
        });

        $('.summernote').summernote({
          height: 150,                 // set editor height
          minHeight: null,             // set minimum height of editor
          maxHeight: null,
        });
  </script>
@endsection

@section('content')
{{-- <div class="card">
  <div class="header">
      <h2>{{$title}}</h2>
      <ul class="header-dropdown m-r--5">
          <li class="dropdown">
          </li>
      </ul>
  </div>
  <div class="body">
    {!! form_start($form, ['class' => 'form-horizontal']) !!}
    {!! form_rest($form) !!}
    <div class="row clearfix">
        <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
            <a href="{{$url}}" class="btn btn-default btn-lg m-t-15 waves-effect">Cancel</a>
            <button  type="submit" class="btn btn-primary btn-lg m-t-15 waves-effect">Submit</button>
        </div>
    </div>
    {!! form_end($form) !!}
  </div>
</div> --}}
{{Form::open(array('url'=>$action,'method'=>$method,'class'=>'form-horizontal', 'files' => true))}}
{{-- {!! form_start($form, ['class' => 'form-horizontal']) !!} --}}
<div class="card">
  <div class="header">
      <h2>{{$title}}</h2>
      <ul class="header-dropdown m-r--5">
          <li class="dropdown">
          </li>
      </ul>
  </div>
  <div class="body">
    {!! form_rest($bankForm) !!}
{{--     <div class="row clearfix">
      <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
        <label for="buletin_title" class="">Judul</label>
      </div>
      <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
        <div class="form-group">
          <div class="form-line">
            <input class="form-control" required name="buletin_title" type="text" id="buletin_title">
          </div>
        </div>
      </div>
    </div> --}}
    {{-- {!! form_rest($form) !!} --}}
  </div>
</div>

{{-- <div class="block-header">
    <h2>Artikel</h2>
</div> --}}

<div class="card">
  <div class="header">
      <h2>Metode</h2>
      <button type="button" class="btn btn-primary btn-circle waves-effect waves-circle waves-float pull-right add-item">
          <i class="material-icons">add</i>
      </button>
  </div>
</div>
<div class="card">
  <div class="body">
    {!! form_rest($methodForm) !!}
  </div>
</div>
<div class="add-dinamic">

</div>

<div class="card">
  <div class="body">
    <div class="row clearfix">
        <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
            <a href="{{$url}}" class="btn btn-default btn-lg m-t-15 waves-effect">Cancel</a>
            <button  type="submit" class="btn btn-primary btn-lg m-t-15 waves-effect" id="submit-all">Submit</button>
        </div>
    </div>
  </div>
</div>
{{Form::close()}}
@endsection