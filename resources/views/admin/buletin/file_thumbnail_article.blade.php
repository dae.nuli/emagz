<div class="row clearfix">
	<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
		<label for="thumbnail_file_article">File Thumbnail</label>
	</div>

	<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
	    <div class="form-group">
	        <div class="form-line">
	        {{-- <div class="form-line" style="border-bottom: none;"> --}}
				<input class="form-control" name="file_thumbnail_article[]" type="file" id="imgInp" required>
	          	<img id="blah" src="#" alt="" width="200" style="display:none" />
			</div>
		</div>
	</div>
</div>