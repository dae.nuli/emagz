@extends('admin.layouts.admin_template')

@section('head-script')
  <!-- DataTables -->
    <!-- JQuery DataTable Css -->
  <link href="{{ asset('bower_components/adminbsb-materialdesign/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/adminbsb-materialdesign/plugins/sweetalert/sweetalert.css') }}">
@endsection

@section('foot-script')
  <!-- Jquery DataTable Plugin Js -->
  @include('admin.layouts.part.dt_script')
  <script>
  var table;
  $(function () {
      table = $(".js-basic-example").DataTable({
          "dom": 'Bfrtip',
          "responsive": true,
          "processing": true,
          "serverSide": true,
          "ajax": '{{$ajax}}',
          "order": [],
          "columns": [
              {data: 'id', searchable: false, orderable: false},
              {data: 'title', orderable: false},
              {data: 'thumbnail', orderable: false, orderable: false},
              {data: 'author', searchable: false, orderable: false},
              {data: 'comment', searchable: false, orderable: false},
              {data: 'action', searchable: false, orderable: false}
          ],
          columnDefs: [{
            "targets": 0,
            "searchable": false,
            "orderable": false,
            "data": null,
            // "title": 'No.',
            "render": function (data, type, full, meta) {
                return meta.settings._iDisplayStart + meta.row + 1; 
            }
          }],
          buttons: [
            {
              extend: 'excel',
              exportOptions: {
                columns: [0, 1, 3, 4]
              }
            },
            {
              extend: 'pdf',
              exportOptions: {
                columns: [0, 1, 3, 4]
              }
            },
            {
              extend: 'print',
              exportOptions: {
                columns: [0, 1, 3, 4]
              }
            },
          ]
      });

  });
  </script>
  <script src="{{ asset('js/custom.js') }}"></script>
@endsection

@section('content')
<div class="card">
  <div class="header">
      <h2>Buletin : {{$index->buletin_title}}</h2>
      <a href="{{$url}}" class="btn btn-primary btn-circle waves-effect waves-circle waves-float pull-right delete-item"><i class="material-icons">arrow_back</i></a>
  </div>
  <div class="body">
    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
      <thead>
      <tr>
        <th width="10">No</th>
        <th>Judul</th>
        <th>Thumbnail</th>
        <th>Penulis</th>
        <th>Komentar</th>
        <th>Actions</th>
      </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
  </div>
</div>
@endsection