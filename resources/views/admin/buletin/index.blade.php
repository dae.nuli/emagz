@extends('admin.layouts.admin_template')

@section('head-script')
  <!-- DataTables -->
    <!-- JQuery DataTable Css -->
  <link href="{{ asset('bower_components/adminbsb-materialdesign/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/adminbsb-materialdesign/plugins/sweetalert/sweetalert.css') }}">
@endsection

@section('foot-script')
  <!-- Jquery DataTable Plugin Js -->
  @include('admin.layouts.part.dt_script')
  <script>
  var table;
  $(function () {
      table = $(".js-basic-example").DataTable({
          "dom": 'Bfrtip',
          "responsive": true,
          "processing": true,
          "serverSide": true,
          "ajax": '{{$ajax}}',
          "order": [],
          "columns": [
              {data: 'id', searchable: false, orderable: false},
              {data: 'thumbnail_filename', searchable: false, orderable: false},
              {data: 'edition', searchable: false, orderable: false},
              {data: 'buletin_title', orderable: false},
              {data: 'date_published', searchable: false, orderable: false},
              {data: 'price', searchable: false, orderable: false},
              {data: 'is_published', searchable: false, orderable: false},
              {data: 'comment', searchable: false, orderable: false},
              {data: 'action', searchable: false, orderable: false}
          ],
          columnDefs: [{
            "targets": 0,
            "searchable": false,
            "orderable": false,
            "data": null,
            // "title": 'No.',
            "render": function (data, type, full, meta) {
                return meta.settings._iDisplayStart + meta.row + 1; 
            }
          }],
          buttons: [
            {
              extend: 'excel',
              exportOptions: {
                columns: [0, 1, 2, 3, 4, 5, 6, 7]
              }
            },
            {
              extend: 'pdf',
              exportOptions: {
                columns: [0, 1, 2, 3, 4, 5, 6, 7]
              }
            },
            {
              extend: 'print',
              exportOptions: {
                columns: [0, 1, 2, 3, 4, 5, 6, 7]
              }
            },
          ]
      });

  });
  </script>
  <script src="{{ asset('js/custom.js') }}"></script>
@endsection

@section('content')
<div class="card">
  <div class="header">
      <h2>{{$title}}</h2>
      <ul class="header-dropdown m-r--5">
          <li class="dropdown">
              <a href="{{$create}}" class="btn btn-primary waves-effect pull-right">Create</a>
          </li>
      </ul>
  </div>
  <div class="body">
    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
      <thead>
      <tr>
        <th width="10">No</th>
        <th>Thumbnail</th>
        <th>Edisi</th>
        <th>Judul</th>
        <th>Tanggal</th>
        <th>Harga</th>
        <th>Status</th>
        <th>Komentar</th>
        <th>Actions</th>
      </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
  </div>
</div>
@endsection