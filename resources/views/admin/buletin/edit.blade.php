@extends('admin.layouts.admin_template')

@section('head-script')
  <link href="{{ asset('bower_components/adminbsb-materialdesign/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">
  <link href="{{ asset('bower_components/adminbsb-materialdesign/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css' )}}" rel="stylesheet" />
@endsection

@section('foot-script')
  <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
  <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/momentjs/moment.js' )}}"></script>
  <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js' )}}"></script>
  <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/jquery-validation/jquery.validate.js') }}"></script>
  <script src="{{ asset('js/jquery.price_format.2.0.min.js') }}"></script>
  <script src="{{ asset('js/validation.js') }}"></script>
  <script type="text/javascript">
        var max_fields      = 21; //maximum input boxes allowed
        var wrapper         = $(".add-dinamic"); //Fields wrapper
        var add_button      = $(".add-item"); //Add button ID
        
        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div class="card"><div class="header"><button type="button" class="btn btn-danger btn-circle waves-effect waves-circle waves-float pull-right delete-item"><i class="material-icons">clear</i></button></div><div class="body"><div class="row clearfix"><div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label"><label for="title_article[]" class="">Judul</label></div><div class="col-lg-10 col-md-10 col-sm-8 col-xs-7"><div class="form-group"><div class="form-line"><input class="form-control" required="" name="title_article[]" type="text" id="title_article[]"></div></div></div></div><div class="row clearfix"><div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label"><label for="description_article[]" class="">Deskripsi</label></div><div class="col-lg-10 col-md-10 col-sm-8 col-xs-7"><div class="form-group"><div class="form-line"><textarea class="form-control no-resize" required="" cols="30" rows="5" name="description_article[]" id="description_article[]"></textarea></div></div></div></div><div class="row clearfix"><div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label"><label for="author_article[]" class="">Penulis</label></div><div class="col-lg-10 col-md-10 col-sm-8 col-xs-7"><div class="form-group"><div class="form-line"><input class="form-control" required="" name="author_article[]" type="text" id="author_article[]"></div></div></div></div><div class="row clearfix"><div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label"><label for="file_thumbnail_article[]" class="">File Thumbnail</label></div><div class="col-lg-10 col-md-10 col-sm-8 col-xs-7"><div class="form-group"><div class="form-line"><input class="form-control" name="file_thumbnail_article[]" type="file" id="file_thumbnail_article[]" required></div></div></div></div></div></div>'); //add input box
            }
        });
        
        $(document).on("click",".delete-item", function(e){ //user click on remove text
            e.preventDefault(); $(this).parents('.add-dinamic div').remove(); x--;
        });

      $('.datetimepicker').bootstrapMaterialDatePicker({
          format: 'YYYY/MM/DD HH:mm',
          clearButton: true,
          weekStart: 1
      });

      $("#imgInp").change(function(){
          readURL(this);
      });

      function readURL(input) {
          if (input.files && input.files[0]) {
              var reader = new FileReader();
              
              reader.onload = function (e) {
                  $('#blah').attr('src', e.target.result);
                  $('#blah').show();
                  // $('#blah').attr('src', e.target.result);
                  // $('#blah').show();
              }
              
              reader.readAsDataURL(input.files[0]);
          }
      }
    $('.price').priceFormat({
        prefix:'',
        centsSeparator:'',
        centsLimit:'',
        clearPrefix:true,
        thousandsSeparator:'.'
    });
  </script>
@endsection

@section('content')
{{Form::open(array('url'=>$action,'method'=>$method,'class'=>'form-horizontal', 'files' => true))}}
{{-- {!! form_start($form, ['class' => 'form-horizontal']) !!} --}}
<div class="card">
  <div class="header">
      <h2>{{$title}}</h2>
      <ul class="header-dropdown m-r--5">
          <li class="dropdown">
          </li>
      </ul>
  </div>
  <div class="body">
    <div class="row clearfix">
      <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
        <label for="buletin_title" class="">Judul</label>
      </div>
      <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
        <div class="form-group">
          <div class="form-line">
            <input class="form-control" required value="{{$index->buletin_title}}" name="buletin_title" type="text" id="buletin_title">
          </div>
        </div>
      </div>
    </div>
    <div class="row clearfix">
      <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
        <label for="edition" class="">Edisi</label>
      </div>
      <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
        <div class="form-group">
          <div class="form-line">
            <input class="form-control" required value="{{$index->edition}}" name="edition" type="text" id="edition">
          </div>
        </div>
      </div>
    </div>
    <div class="row clearfix">
      <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
        <label for="buletin_code" class="">Kode Buletin</label>
      </div>
      <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
        <div class="form-group">
          <div class="form-line">
            <input class="form-control" required value="{{$index->buletin_code}}" name="buletin_code" type="text" id="buletin_code">
          </div>
        </div>
      </div>
    </div>
    <div class="row clearfix">
      <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
        <label for="price" class="">Harga</label>
      </div>
      <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
        <div class="form-group">
          <div class="form-line">
            <input class="form-control price" required value="{{$index->price}}" name="price" type="text" id="price">
          </div>
        </div>
      </div>
    </div>
    <div class="row clearfix">
      <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
        <label for="tag" class="">Tag</label>
      </div>
      <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
        <div class="form-group">
          <div class="form-line">
            <input class="form-control" data-role="tagsinput" required value="{{$index->tag}}" name="tag" type="text" id="tag">
          </div>
        </div>
      </div>
    </div>
    <div class="row clearfix">
      <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
        <label for="date_published" class="">Tanggal</label>
      </div>
      <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
        <div class="form-group">
          <div class="form-line">
            <input class="form-control datetimepicker" required value="{{$index->date_published}}" name="date_published" type="text" id="date_published">
          </div>
        </div>
      </div>
    </div>
    <div class="row clearfix">
      <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
        <label for="file_buletin">File Buletin</label>
      </div>
      <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
        <div class="form-group">
          <div class="form-line">
            <input class="form-control" name="file_buletin" type="file" accept=".epub">
            <small>{{$index->filename}}</small>
          </div>
        </div>
      </div>
    </div> 
    <div class="row clearfix">
      <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
        <label for="file_thumbnail">File Thumbnail</label>
      </div>
      <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
        <div class="form-group">
          <div class="form-line">
            <input class="form-control" name="file_thumbnail" type="file" id="imgInp">
            <img id="blah" src="{{ asset('buletin/thumbnail/'.$index->thumbnail_filename) }}" alt="" width="200" />
            {{-- <img id="blah" src="#" alt="" width="200" style="display:none" /> --}}
          </div>
        </div>
      </div>
    </div> 
    <div class="row clearfix">
        <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
          <input id="is_free" name="is_free" type="checkbox" value="1">
          <label for="is_free" class="">Gratis</label>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
          <input id="is_published" checked="checked" name="is_published" type="checkbox" value="1">
          <label for="is_published" class="">Published</label>
        </div>
    </div>
  </div>
</div>

{{-- <div class="block-header">
    <h2>Artikel</h2>
</div> --}}

<div class="card">
  <div class="header">
      <h2>Artikel</h2>
      <button type="button" class="btn btn-primary btn-circle waves-effect waves-circle waves-float pull-right add-item">
          <i class="material-icons">add</i>
      </button>
  </div>
</div>
@foreach($articles as $key => $row)
<div class="card">
  <div class="header">
      <a type="button" href="{{ $url.'/delete-article/'.$row->id }}" class="btn btn-danger btn-circle waves-effect waves-circle waves-float pull-right">
          <i class="material-icons">clear</i>
      </a>
  </div>
  <div class="body">
    <input type="hidden" name="id_article[{{$key}}]" value="{{$row->id}}">
    <div class="row clearfix">
      <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
        <label for="title_article[]" class="">Judul</label>
      </div>
      <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
        <div class="form-group">
          <div class="form-line">
            <input class="form-control" required name="title_article[{{$key}}]" value="{{$row->title}}" type="text" id="title_article[]">
          </div>
        </div>
      </div>
    </div>
    <div class="row clearfix">
      <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
        <label for="description_article[]" class="">Deskripsi</label>
      </div>
      <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
        <div class="form-group">
          <div class="form-line">
            <textarea class="form-control no-resize" required cols="30" rows="5" name="description_article[]" id="description_article[{{$key}}]">{{$row->description}}</textarea>
          </div>
        </div>
      </div>
    </div>
    <div class="row clearfix">
      <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
        <label for="author_article[]" class="">Penulis</label>
      </div>
      <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
        <div class="form-group">
          <div class="form-line">
            <input class="form-control" required name="author_article[{{$key}}]" value="{{$row->author}}" type="text" id="author_article[]">
          </div>
        </div>
      </div>
    </div>
    <div class="row clearfix">
      <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
        <label for="thumbnail_file_article">File Thumbnail</label>
      </div>
      <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
        <div class="form-group">
          <div class="form-line">
            <input class="form-control" name="file_thumbnail_article[{{$key}}]" type="file" id="imgInp">
            <img id="blah" src="{{ asset('article/'.$row->thumbnail) }}" alt="" width="200" />
          </div>
        </div>
      </div>
    </div>        
  </div>
</div>
@endforeach
<div class="add-dinamic">

</div>

<div class="card">
  <div class="body">
    <div class="row clearfix">
        <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
            <a href="{{$url}}" class="btn btn-default btn-lg m-t-15 waves-effect">Cancel</a>
            <button  type="submit" class="btn btn-primary btn-lg m-t-15 waves-effect" id="submit-all">Submit</button>
        </div>
    </div>
  </div>
</div>
  {{Form::close()}}
{{-- {!! form_end($form) !!} --}}
@endsection