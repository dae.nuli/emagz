@extends('admin.layouts.admin_template')

@section('head-script')
  <link href="{{ asset('bower_components/adminbsb-materialdesign/plugins/light-gallery/css/lightgallery.css') }}" rel="stylesheet">
@endsection

@section('foot-script')
  <!-- Jquery DataTable Plugin Js -->
  <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/light-gallery/js/lightgallery-all.js') }}"></script>
  <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/jquery-validation/jquery.validate.js') }}"></script>
  <script src="{{ asset('js/validation.js') }}"></script>
  <script type="text/javascript">
  $(function () {
      $('#aniimated-thumbnials').lightGallery({
          thumbnail: true,
          selector: 'a'
      });
      $("#imgInp").change(function(){
          readURL(this);
      });
      function readURL(input) {
          if (input.files && input.files[0]) {
              var reader = new FileReader();
              
              reader.onload = function (e) {
                  $('#blah').attr('src', e.target.result);
                  $('#blah').show();
              }
              
              reader.readAsDataURL(input.files[0]);
          }
      }
  
  });
  </script>
@endsection

@section('content')
<div class="card">
  <div class="header">
      <h2>{{$title}}</h2>
      <ul class="header-dropdown m-r--5">
          <li class="dropdown">
          </li>
      </ul>
  </div>
  <div class="body">
    {!! form_start($form, ['class' => 'form-horizontal']) !!}
    {!! form_rest($form) !!}
    <div class="row clearfix">
        <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
            <a href="{{$url}}" class="btn btn-default btn-lg m-t-15 waves-effect">Cancel</a>
            <button  type="submit" class="btn btn-primary btn-lg m-t-15 waves-effect">Submit</button>
        </div>
    </div>
    {!! form_end($form) !!}
  </div>
</div>
@endsection