@extends('admin.layouts.admin_template')

@section('foot-script')
  <!-- Jquery DataTable Plugin Js -->
  <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/jquery-validation/jquery.validate.js') }}"></script>
  <script src="{{ asset('js/validation.js') }}"></script>
  <script type="text/javascript">
  $("input[name=is_suspended]").on("change", function(){
      if($(this).is(":checked")) {
        // $('input[name=user_name]').removeAttr('required');
        $('input[name=address]').removeAttr('required');
        $('input[name=subdistrict]').removeAttr('required');
        $('input[name=city]').removeAttr('required');
        $('input[name=province]').removeAttr('required');
        $('input[name=country]').removeAttr('required');
        $('.hidden-reason').show();
      } else {
        // $('input[name=user_name]').attr('required', '');
        $('input[name=address]').attr('required', '');
        $('input[name=subdistrict]').attr('required', '');
        $('input[name=city]').attr('required', '');
        $('input[name=province]').attr('required', '');
        $('input[name=country]').attr('required', '');
        $('.hidden-reason').hide();
      }
  });
  $("input[name=is_validated]").on("change", function(){
      if($(this).is(":checked")) {
        // $('input[name=user_name]').removeAttr('required');
        $('input[name=address]').removeAttr('required');
        $('input[name=subdistrict]').removeAttr('required');
        $('input[name=city]').removeAttr('required');
        $('input[name=province]').removeAttr('required');
        $('input[name=country]').removeAttr('required');
        // $('.hidden-reason').show();
      } else {
        // $('input[name=user_name]').attr('required', '');
        $('input[name=address]').attr('required', '');
        $('input[name=subdistrict]').attr('required', '');
        $('input[name=city]').attr('required', '');
        $('input[name=province]').attr('required', '');
        $('input[name=country]').attr('required', '');
        // $('.hidden-reason').hide();
      }
  });
  if($('input[name=is_suspended]').is(":not(:checked)")) {
    $('.hidden-reason').hide();
  } else {
    $('.hidden-reason').show();
  }
  </script>
@endsection

@section('content')
<div class="card">
  <div class="header">
      <h2>{{$title}}</h2>
      <ul class="header-dropdown m-r--5">
          <li class="dropdown">
              {{-- <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">more_vert</i>
              </a> --}}
              {{-- <a href="{{$create}}" class="btn btn-primary waves-effect pull-right">Create</a> --}}
              {{-- <ul class="dropdown-menu pull-right">
                  <li><a href="javascript:void(0);">Action</a></li>
                  <li><a href="javascript:void(0);">Another action</a></li>
                  <li><a href="javascript:void(0);">Something else here</a></li>
              </ul> --}}
          </li>
      </ul>
  </div>
  <div class="body">
    {!! form_start($form, ['class' => 'form-horizontal']) !!}
    {!! form_rest($form) !!}
    <div class="row clearfix">
        <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
            <a href="{{$url}}" class="btn btn-default btn-lg m-t-15 waves-effect">Cancel</a>
            <button  type="submit" class="btn btn-primary btn-lg m-t-15 waves-effect">Submit</button>
        </div>
    </div>
    {!! form_end($form) !!}
  </div>
</div>
@endsection