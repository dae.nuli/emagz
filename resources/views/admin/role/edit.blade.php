@extends('admin.layouts.admin_template')

@section('foot-script')
  <!-- Jquery DataTable Plugin Js -->
  <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/jquery-validation/jquery.validate.js') }}"></script>
  <script src="{{ asset('js/validation.js') }}"></script>
@endsection

@section('content')
<div class="card">
  <div class="header">
      <h2>{{$title}}</h2>
      <ul class="header-dropdown m-r--5">
          <li class="dropdown">
          </li>
      </ul>
  </div>
  <div class="body">
    {{Form::open(array('url'=>$action, 'method'=>$method, 'class'=>'form-horizontal'))}}

    <div class="row clearfix">
      <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
        <label for="role" class>Role</label>
      </div>
      <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
        <div class="form-group">
          <div class="form-line">
            <input class="form-control" type="text" name="role" value="{{$index->role}}" id="role" required>
          </div>
        </div>
      </div>
    </div>

    <div class="row clearfix">
      <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
        <label for="description" class>Deskripsi</label>
      </div>
      <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
        <div class="form-group">
          <div class="form-line">
            <input class="form-control" type="text" name="description" value="{{$index->description}}" id="description" required>
          </div>
        </div>
      </div>
    </div>

    <div class="row clearfix">
      <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
        <label for="permission" class>Module</label>
      </div>
      <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
        <div class="form-group">
            <div class="demo-checkbox">
              @foreach($permission as $i => $row)
                <input type="checkbox" id="checkbox_{{$i}}" name="permission[{{$i}}]" value="{{$row->id}}" class="filled-in" {{($row->isModule($index->id, $row->id)) ? 'checked' : ''}}/>
                <label for="checkbox_{{$i}}">{{$row->module}}</label>
              @endforeach
            </div>
        </div>
      </div>
    </div>

    <div class="row clearfix">
        <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
            <a href="{{$url}}" class="btn btn-default btn-lg m-t-15 waves-effect">Cancel</a>
            <button  type="submit" class="btn btn-primary btn-lg m-t-15 waves-effect">Submit</button>
        </div>
    </div>
    {{Form::close()}}
  </div>
</div>
@endsection