@extends('admin.layouts.admin_template')

@section('foot-script')
  <!-- Jquery DataTable Plugin Js -->
  <script src="{{ asset('bower_components/adminbsb-materialdesign/plugins/jquery-validation/jquery.validate.js') }}"></script>
  <script src="{{ asset('js/validation.js') }}"></script>
@endsection

@section('content')
<div class="card">
  <div class="header">
      <h2>{{$title}}</h2>
      <ul class="header-dropdown m-r--5">
          <li class="dropdown">
          </li>
      </ul>
  </div>
  <div class="body">
    {!! form_start($form, ['class' => 'form-horizontal']) !!}
    {!! form_rest($form) !!}
    <div class="row clearfix">
        <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
            <a href="{{$url}}" class="btn btn-default btn-lg m-t-15 waves-effect">Cancel</a>
            <button  type="submit" class="btn btn-primary btn-lg m-t-15 waves-effect">Submit</button>
        </div>
    </div>
    {!! form_end($form) !!}
  </div>
</div>
@endsection