<?php if ($showLabel && $showField): ?>
    <?php if ($options['wrapper'] !== false): ?>
    <div <?= $options['wrapperAttrs'] ?> >
    <?php endif; ?>
<?php endif; ?>
<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
<?php if ($showLabel && $options['label'] !== false && $options['label_show']): ?>
    <?= Form::customLabel($name, $options['label'], $options['label_attr']) ?>
<?php endif; ?>
</div>

<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
    <div class="form-group">
        <div class="form-line">
			<div class="dz-message">
			    <div class="drag-icon-cph">
			        <i class="material-icons">touch_app</i>
			    </div>
			    <h3>Drop files here or click to upload.</h3>
			    <em>(This is just a demo dropzone. Selected files are <strong>not</strong> actually uploaded.)</em>
			</div>
			<div class="fallback">
<?php if ($showField): ?>
    <?= Form::input($type, $name, $options['value'], $options['attr']) ?>

    <?php include 'help_block.php' ?>
<?php endif; ?>
			</div>
		</div>
	</div>
</div>

<?php include 'errors.php' ?>

<?php if ($showLabel && $showField): ?>
    <?php if ($options['wrapper'] !== false): ?>
    </div>
    <?php endif; ?>
<?php endif; ?>
