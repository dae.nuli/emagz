<?php if ($showLabel && $showField): ?>
    <?php if ($options['wrapper'] !== false): ?>
    <div <?= $options['wrapperAttrs'] ?> >
    <?php endif; ?>
<?php endif; ?>

<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
<?php if ($showLabel && $options['label'] !== false && $options['label_show']): ?>
    <?= Form::customLabel($name, $options['label'], $options['label_attr']) ?>
<?php endif; ?>
</div>

<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
    <div class="form-group">
        <div class="form-line">
<?php if ($showField): ?>
    <<?= $options['tag'] ?> <?= $options['elemAttrs'] ?>><?= $options['value'] ?></<?= $options['tag'] ?>>

    <?php include 'help_block.php' ?>

<?php endif; ?>
        </div>
    </div>
</div>


<?php if ($showLabel && $showField): ?>
    <?php if ($options['wrapper'] !== false): ?>
    </div>
    <?php endif; ?>
<?php endif; ?>
