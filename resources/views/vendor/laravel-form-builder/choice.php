<?php if ($showLabel && $showField): ?>
    <?php if ($options['wrapper'] !== false): ?>
    <div <?= $options['wrapperAttrs'] ?> >
    <?php endif; ?>
<?php endif; ?>

<?php if ($showLabel && $options['label'] !== false && $options['label_show']): ?>
<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
    <?= Form::customLabel($name, $options['label'], $options['label_attr']) ?>
</div>
<?php endif; ?>

<?php if ($showField): ?>
    <?php foreach ((array)$options['children'] as $child): ?>
        <?= $child->render(['selected' => $options['selected']], true, true, false) ?>
    <?php endforeach; ?>

    <?php include 'help_block.php' ?>

<?php endif; ?>


<?php include 'errors.php' ?>

<?php if ($showLabel && $showField): ?>
    <?php if ($options['wrapper'] !== false): ?>
    </div>
    <?php endif; ?>
<?php endif; ?>
