<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Models\Buletin;
use App\Models\MessageReply;
use App\Models\Message;
// use DB;
Route::get('/', function () {
	return  redirect('admin/login');
});
 
Route::get('/tes', function () {
	// $reply = MessageReply::where('bp_message_id', 1)->delete();
			// $rate2 = Rating::select('bp_article_id', DB::raw('avg(rating) as average'))
			// 		->groupBy('bp_article_id')
			// 		->whereHas('article', function ($qq) {
			// 			$qq->whereHas('buletin', function ($rr) {
			// 				$rr->where('id', 1);
			// 			});
			// 		})
			// 		->where('average',$rate->max('average'))
			// 		->first();
			// return dd($rate2);
			// dd($rat->groupBy('buletin_id')->max('average_rating'));
			// foreach ($rate as $key => $value) {
			// 	echo "ID : ".$value->bp_article_id." - Rating : ".$value->average."<br>";
			// }
	// $bul = Buletin::with(['article' => function ($query) {
	// 	$query->with(['rate' => function ($qr) {

	// 	}]);
	// 	// $query->whereHas('rate', function ($qr) {
	// 	// 	$qr->
	// 	// })
	// }])->get();
	// foreach ($bul as $key => $value) {
	// 	echo "buletin : ".$value->buletin_title." - artikel : ";
	// 	foreach ($value->article as $i => $row) {
	// 		echo "* ".$row->title."<br>";
	// 	}
	// }
	// return bcrypt('111111');
	// $dae = 'nuli';
	// return is_null($dae) ? $dae : 'kanan';
});

 

Route::group(['prefix' => 'admin'], function () {
	//Admin Login
	Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
	Route::post('/login', 'Auth\LoginController@login');
	Route::post('logout', 'Auth\LoginController@logout');

	//Admin Passwords
	Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
	Route::post('password/reset', 'Auth\ResetPasswordController@reset');
	Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
	Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
});

Route::get('elfinder/ckeditor', 'Barryvdh\Elfinder\ElfinderController@showPopup');