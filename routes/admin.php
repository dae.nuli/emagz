<?php

Route::get('home/topup/{id}', 'Admin\HomeController@show')->name('home.topup.show');
Route::post('home/topup/confirmation/{id}', 'Admin\HomeController@postConfirmation')->name('home.topup.confirmation');
Route::get('home', 'Admin\HomeController@index')->name('home.index');

Route::get('laporan/buletin', 'Admin\ReportController@getBuletin')->name('home.laporan-buletin');
Route::get('laporan/buletin/data', 'Admin\ReportController@dataBuletin')->name('home.laporan-buletin');
Route::get('laporan/topup', 'Admin\ReportController@getTopup')->name('home.laporan-topup');
Route::get('laporan/topup/data', 'Admin\ReportController@dataTopup')->name('home.laporan-topup');


// analytics
Route::get('analytics', 'Admin\AnalyticsController@index')->name('analytics.index');
Route::get('analytics/buletin', 'Admin\AnalyticsController@dataBuletin')->name('analytics.data.buletin');
Route::get('analytics/news', 'Admin\AnalyticsController@dataNews')->name('analytics.data.news');

// CUSTOMERS
Route::get('customers/data', 'Admin\CustomersController@data')->name('customers.data');
Route::resource('customers', 'Admin\CustomersController');

// USERS
Route::get('users/data', 'Admin\UsersController@data')->name('users.data');
Route::resource('users', 'Admin\UsersController');

// Comment
Route::post('comment/approve-comment', 'Admin\CommentController@approveComment')->name('comment.approve.comment');
Route::get('comment/data', 'Admin\CommentController@data')->name('comment.data');
Route::resource('comment', 'Admin\CommentController');

// NEWS

Route::get('news/unPublish/{id?}', 'Admin\NewsController@unPublish')->name('news.unpublish');
Route::get('news/publish/{id?}', 'Admin\NewsController@publish')->name('news.publish');
Route::get('news/data', 'Admin\NewsController@data')->name('news.data');
Route::resource('news', 'Admin\NewsController');

// BULETIN
Route::delete('buletin/delete-comment/{id}', 'Admin\BuletinController@jsonDeleteComment')->name('buletin.destroyComment');
Route::delete('buletin/delete-item/{id}', 'Admin\BuletinController@jsonDeleteArticle')->name('buletin.destroyArticle');
Route::get('buletin/delete-article/{id}', 'Admin\BuletinController@destroyArticle');

Route::get('buletin/detail/{id}', 'Admin\BuletinController@showDetail')->name('buletin.detail');
Route::get('buletin/comment/{buletin}/{id}', 'Admin\BuletinController@showComment')->name('buletin.comment');

Route::post('buletin/approve-comment', 'Admin\BuletinController@approveComment');

Route::get('buletin/unPublish/{id?}', 'Admin\BuletinController@unPublish')->name('buletin.unpublish');
Route::get('buletin/publish/{id?}', 'Admin\BuletinController@publish')->name('buletin.publish');
Route::get('buletin/data', 'Admin\BuletinController@data')->name('buletin.data');
Route::get('buletin/data-buletin/{id}', 'Admin\BuletinController@dataShowDetail')->name('buletin.data.detail');
Route::get('buletin/data-comment/{id}', 'Admin\BuletinController@dataShowComment')->name('buletin.data.comment');
Route::resource('buletin', 'Admin\BuletinController');

// PUSH NOTIF
Route::get('announcement/unPublish/{id?}', 'Admin\PushNotificationController@unPublish')->name('announcement.unpublish');
Route::get('announcement/publish/{id?}', 'Admin\PushNotificationController@publish')->name('announcement.publish');
Route::get('announcement/data', 'Admin\PushNotificationController@data')->name('announcement.data');
Route::resource('announcement', 'Admin\PushNotificationController');

// ROLE
Route::get('role/data', 'Admin\RoleController@data')->name('role.data');
Route::resource('role', 'Admin\RoleController');

// MODULE
Route::get('module/data', 'Admin\ModuleController@data')->name('module.data');
Route::resource('module', 'Admin\ModuleController');

// Topup Type
Route::get('topUpType/data', 'Admin\TopupTypeController@data')->name('topUpType.data');
Route::resource('topUpType', 'Admin\TopupTypeController');

// Topup Status
Route::get('topUpStatus/data', 'Admin\TopupStatusController@data')->name('topUpStatus.data');
Route::resource('topUpStatus', 'Admin\TopupStatusController');

// Topup 
Route::post('topUp/confirmation/{id}', 'Admin\TopupController@postConfirmation')->name('topUp.confirmation');
Route::get('topUp/data', 'Admin\TopupController@data')->name('topUp.data');
Route::resource('topUp', 'Admin\TopupController');

// BANK
Route::get('bank/deleteMethod/{id}', 'Admin\BankAccountController@deleteItem')->name('bank.delete.method');
Route::get('bank/data', 'Admin\BankAccountController@data')->name('bank.data');
Route::resource('bank', 'Admin\BankAccountController');

// TERM CONDITION
Route::get('term/data', 'Admin\TermConditionController@data')->name('term.data');
Route::resource('term', 'Admin\TermConditionController');

// LOG ADMIN
Route::get('log/data', 'Admin\LogAdminController@data')->name('log.data');
Route::resource('log', 'Admin\LogAdminController');

// FEEDBACK
Route::get('feedback/data', 'Admin\FeedbackController@data')->name('feedback.data');
Route::get('feedback/reply/{id?}', 'Admin\FeedbackController@reply')->name('feedback.reply');
Route::resource('feedback', 'Admin\FeedbackController');

// MARKETING
Route::get('slide/data', 'Admin\SlideController@data')->name('slide.data');
Route::resource('slide', 'Admin\SlideController');

// PRIVACY
Route::get('privacy/data', 'Admin\PrivacyPolicyController@data')->name('privacy.data');
Route::resource('privacy', 'Admin\PrivacyPolicyController');

Route::get('setting/data', 'Admin\SettingController@data')->name('setting.data');
Route::resource('setting', 'Admin\SettingController');