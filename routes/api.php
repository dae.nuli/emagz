<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//use JWTAuth;

Route::get('download', 'Api\DownloadController@getEbook');
Route::get('decrypt', 'Api\DownloadController@decrypt');

Route::post('register', 'Api\RegisterController@register');
// Route::get('verification', 'Api\RegisterController@pushVerification');
Route::post('verification', 'Api\RegisterController@pushVerification');
Route::post('requestCode', 'Api\RegisterController@requestCodeActivation');
Route::post('profile', 'Api\RegisterController@editProfile');

Route::post('forgot', 'Api\RegisterController@forgotPassword');
Route::post('login', 'Api\AuthController@authenticate');
Route::post('logCustomerSessionLogin', 'Api\AuthController@userSessionLogin');
Route::post('logCustomerSessionLogout', 'Api\AuthController@userSessionLogout');
// Route::post('login2', 'Api\AuthController2@authenticate');

Route::get('news/notif', 'Api\NewsController@notif');
Route::get('news/{id}', 'Api\NewsController@show');
Route::get('news', 'Api\NewsController@index');
Route::get('testing', 'Api\AuthController2@index');

Route::get('buletin/notif', 'Api\BuletinController@notif');
Route::get('buletin/{id}', 'Api\BuletinController@show');
Route::get('buletin', 'Api\BuletinController@index');

// ORDER
Route::post('order', 'Api\OrderController@index');

// WALET
Route::get('wallet', 'Api\CheckWalletController@index');

// COMMENT
Route::post('postComment', 'Api\CommentController@postComment');
Route::get('listComment/{id}', 'Api\CommentController@listComment');

// HISTORY TOPUP
Route::get('topup/history', 'Api\TopUpController@historyTopup');

// 	TOPUP
Route::post('topup', 'Api\TopUpController@postTopup');

// RATING
Route::post('postRating', 'Api\RatingController@postRating');
Route::get('listRating/{id}', 'Api\RatingController@listRating');

// BANK
Route::get('bank', 'Api\BankAccountController@index');

// My BUletin
Route::get('myBuletin', 'Api\MyBuletinController@index');

// Slide Marketing
Route::get('slideMarketing', 'Api\SlideMarketingController@index');

// Filter News and Buletin
Route::get('filterNews', 'Api\FilterAndSearchController@filterNews');
Route::get('filterBuletin', 'Api\FilterAndSearchController@filterBuletin');

// Filter My Buletin
Route::get('filterMyBuletin', 'Api\FilterMyBuletinController@filterBuletin');

// List FeedBack
Route::post('feedback/reply', 'Api\FeedbackController@postReply');
Route::post('feedback/send', 'Api\FeedbackController@postFB');
Route::get('feedback/{id}', 'Api\FeedbackController@detail');
Route::get('feedback', 'Api\FeedbackController@index');

// TERM AND CONDITION
Route::get('termsandconditions', 'Api\TermConditionController@index');

// PRIVACY POLICY
Route::get('privacypolicy', 'Api\PrivacyPolicyController@index');

Route::post('password/email', 'Api\ForgotPasswordController@getResetToken');
Route::post('password/reset', 'Api\ResetPasswordController@reset');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
