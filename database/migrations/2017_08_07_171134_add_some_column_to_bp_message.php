<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSomeColumnToBpMessage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('bp_message', 'content')) {
            Schema::table('bp_message', function (Blueprint $table) {
                $table->text('content')->after('title');
            });
        }

        if (!Schema::hasColumn('bp_message', 'is_read')) {
            Schema::table('bp_message', function (Blueprint $table) {
                $table->boolean('is_read')->after('content');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('bp_message', 'content')) {
            Schema::table('bp_message', function (Blueprint $table) {
                $table->dropColumn('content');
            });
        }

        if (Schema::hasColumn('bp_message', 'is_read')) {
            Schema::table('bp_message', function (Blueprint $table) {
                $table->dropColumn('is_read');
            });
        }
    }
}
