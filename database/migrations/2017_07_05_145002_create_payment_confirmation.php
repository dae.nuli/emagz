<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentConfirmation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bp_payment_confirmation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bp_user_id');
            $table->string('topup_code');
            $table->integer('amount');
            $table->timestamp('topup_date')->nullable();
            $table->integer('account_name');
            $table->timestamp('confirmation_date')->nullable();
            $table->string('file_attachment');
            $table->integer('cfg_topup_type_id');
            $table->integer('cfg_topup_status_id');
            
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bp_payment_confirmation');
    }
}
