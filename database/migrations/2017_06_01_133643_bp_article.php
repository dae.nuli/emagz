<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BpArticle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bp_article', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bp_buletin_id');
            $table->string('title');
            $table->text('description')->nullable();
            // $table->string('thumbnail_filename');
            $table->integer('rating')->nullable();
            $table->integer('comment_count')->default(0);
            // $table->string('written_by');
            $table->string('thumbnail');
            $table->string('author')->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bp_article');
    }
}
