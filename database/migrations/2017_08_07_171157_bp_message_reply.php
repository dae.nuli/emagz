<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BpMessageReply extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bp_message_reply', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bp_message_id');
            // $table->integer('user_id')->nullable();
            $table->integer('user_id')->nullable(); // For All User (Customer and Admin)
            $table->string('content');
            $table->boolean('is_admin');
            $table->boolean('is_read');
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bp_message_reply');
    }
}
