<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropUrlColumnToLogAdmin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('log_admin', 'page_url')) {
            Schema::table('log_admin', function (Blueprint $table) {
                $table->dropColumn('page_url');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasColumn('log_admin', 'page_url')) {
            Schema::table('log_admin', function (Blueprint $table) {
                $table->dropColumn('page_url');
            });
        }
    }
}
