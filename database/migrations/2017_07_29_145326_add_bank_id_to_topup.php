<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBankIdToTopup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('bp_topup', 'bank_id')) {
            Schema::table('bp_topup', function (Blueprint $table) {
                $table->integer('bank_id')->after('bp_user_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('bp_topup', 'bank_id')) {
            Schema::table('bp_topup', function (Blueprint $table) {
                $table->dropColumn('bank_id');
            });
        }
    }
}
