<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusToTopup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('bp_topup', 'status')) {
            Schema::table('bp_topup', function (Blueprint $table) {
                $table->integer('status')->after('cfg_topup_status_id')->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('bp_topup', 'status')) {
            Schema::table('bp_topup', function (Blueprint $table) {
                $table->dropColumn('status');
            });
        }
    }
}
