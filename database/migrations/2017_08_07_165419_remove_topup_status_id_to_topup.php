<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveTopupStatusIdToTopup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('bp_topup', 'cfg_topup_status_id')) {
            Schema::table('bp_topup', function (Blueprint $table) {
                $table->dropColumn('cfg_topup_status_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasColumn('bp_topup', 'cfg_topup_status_id')) {
            Schema::table('bp_topup', function (Blueprint $table) {
                $table->dropColumn('cfg_topup_status_id');
            });
        }
    }
}
