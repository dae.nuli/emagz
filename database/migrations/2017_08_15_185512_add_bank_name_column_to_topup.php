<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBankNameColumnToTopup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('bp_topup', 'bank_name')) {
            Schema::table('bp_topup', function (Blueprint $table) {
                $table->text('bank_name')->after('account_name');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('bp_topup', 'bank_name')) {
            Schema::table('bp_topup', function (Blueprint $table) {
                $table->dropColumn('bank_name');
            });
        }
    }
}
