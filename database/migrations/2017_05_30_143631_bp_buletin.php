<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BpBuletin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bp_buletin', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('buletin_code');
            $table->string('edition');
            $table->string('buletin_title');
            $table->integer('price');
            $table->string('tag');
            $table->string('filename')->nullable();
            $table->string('thumbnail_filename')->nullable();
            $table->integer('purchased_count')->default(0);
            $table->boolean('is_free');
            $table->boolean('is_published');
            $table->timestamp('date_published')->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bp_buletin');
    }
}
