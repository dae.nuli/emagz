<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BpUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bp_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uid');
            // $table->string('user_name')->nullable();
            // $table->dateTime('birth_date')->nullable();
            // $table->char('sex', 1)->nullable(); // m and f
            $table->string('full_name')->nullable();
            $table->string('email')->unique();
            $table->integer('amount_balance')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('password', 255)->nullable();
            $table->string('photo_filename')->nullable();
            $table->string('address')->nullable();
            $table->string('subdistrict')->nullable();
            $table->string('city')->nullable();
            $table->string('province')->nullable();
            $table->string('country')->nullable();
            $table->boolean('is_validated')->default(0);
            $table->boolean('is_active')->default(0);
            $table->boolean('is_suspended')->default(0);
            $table->string('suspended_reason')->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bp_users');
    }
}
