<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TermAndCondition extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('term_condition', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('version');
            $table->string('author');
            $table->text('content');
            $table->integer('status');
            $table->timestamp('date_published')->nullable();
            // $table->string('url');
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('term_condition');
    }
}
