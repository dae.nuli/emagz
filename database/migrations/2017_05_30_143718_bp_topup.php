<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BpTopup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bp_topup', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bp_user_id');
            // $table->integer('bank_id');
            $table->string('topup_code');
            $table->integer('amount');
            $table->timestamp('topup_date')->nullable();
            $table->string('account_name');
            // $table->integer('account_name');
            $table->timestamp('confirmation_date')->nullable();
            $table->string('file_attachment');
            $table->integer('cfg_topup_type_id');
            $table->integer('cfg_topup_status_id');
            // $table->boolean('status')->default(0);
            
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bp_topup');
    }
}
