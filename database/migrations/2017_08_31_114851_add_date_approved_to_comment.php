<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDateApprovedToComment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('bp_comment', 'approved_date')) {
            Schema::table('bp_comment', function (Blueprint $table) {
                $table->timestamp('approved_date')->after('updated_by')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('bp_comment', 'approved_date')) {
            Schema::table('bp_comment', function (Blueprint $table) {
                $table->dropColumn('approved_date');
            });
        }
    }
}
