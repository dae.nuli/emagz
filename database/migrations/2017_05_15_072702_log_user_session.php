<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LogUserSession extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_user_session', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bp_user_id');
            $table->string('token', 255);
            $table->string('device_id');
            $table->timestamp('login_date')->nullable();
            $table->timestamp('logout_date')->nullable();
            $table->boolean('is_active');
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_user_session');
    }
}
