<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDescriptionToSlide extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('cfg_slide', 'description')) {
            Schema::table('cfg_slide', function (Blueprint $table) {
                $table->text('description')->after('title');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('cfg_slide', 'description')) {
            Schema::table('cfg_slide', function (Blueprint $table) {
                $table->dropColumn('description');
            });
        }
    }
}
