<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BpOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bp_order', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bp_user_id');
            $table->integer('bp_buletin_id');
            $table->string('order_code');
            $table->integer('amount');
            $table->timestamp('purchase_date')->nullable();
            $table->string('purchase_success_token');
            $table->boolean('is_purchase_succeeded');
            $table->string('payload');
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bp_order');
    }
}
