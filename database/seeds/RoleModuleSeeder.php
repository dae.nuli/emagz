<?php

use Illuminate\Database\Seeder;
use App\Models\RoleModule;
use App\Models\Role;
use App\Models\Module;

class RoleModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('cfg_role_module')->truncate();
        RoleModule::truncate();
        $role = Role::where('role', 'Administrator')->first();
        $module = Module::all();
        foreach ($module as $key => $value) {
	        $rm = new RoleModule;
	        $rm->cfg_role_id = $role->id;
	        $rm->cfg_module_id = $value->id;
	        $rm->save();
        }
    }
}
