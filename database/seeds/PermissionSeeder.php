<?php

use Illuminate\Database\Seeder;
use App\Models\Module;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Module::truncate();
        // DB::table('cfg_module')->truncate();
        Module::insert([
        // DB::table('cfg_module')->insert([
        	[
	            'module'  => 'list_news',
	            'description'  => 'Daftar Berita'
        	],
        	[
	            'module'  => 'create_news',
	            'description'  => 'Tambah Berita'
        	],
        	[
	            'module'  => 'edit_news',
	            'description'  => 'Ubah Berita'
        	],
        	[
	            'module'  => 'delete_news',
	            'description'  => 'Hapus Berita'
        	], //Buletin

        	[
	            'module'  => 'list_buletin',
	            'description'  => 'Daftar Buletin'
        	],
        	[
	            'module'  => 'create_buletin',
	            'description'  => 'Tambah Buletin'
        	],
        	[
	            'module'  => 'edit_buletin',
	            'description'  => 'Ubah Buletin'
        	],
        	[
	            'module'  => 'delete_buletin',
	            'description'  => 'Hapus Buletin'
        	],
//Customer
            [
                'module'  => 'list_customer',
                'description'  => 'Daftar Buletin'
            ],
            [
                'module'  => 'create_customer',
                'description'  => 'Tambah Buletin'
            ],
            [
                'module'  => 'edit_customer',
                'description'  => 'Ubah Buletin'
            ],
            [
                'module'  => 'delete_customer',
                'description'  => 'Hapus Buletin'
            ],
//Admin
            [
                'module'  => 'list_user',
                'description'  => 'Daftar Admin'
            ],
            [
                'module'  => 'create_user',
                'description'  => 'Tambah Admin'
            ],
            [
                'module'  => 'edit_user',
                'description'  => 'Ubah Admin'
            ],
            [
                'module'  => 'delete_user',
                'description'  => 'Hapus Admin'
            ],
//Role
            [
                'module'  => 'list_role',
                'description'  => 'Daftar Role'
            ],
            [
                'module'  => 'create_role',
                'description'  => 'Tambah Role'
            ],
            [
                'module'  => 'edit_role',
                'description'  => 'Ubah Role'
            ],
            [
                'module'  => 'delete_role',
                'description'  => 'Hapus Role'
            ],
//Module
            [
                'module'  => 'list_module',
                'description'  => 'Daftar Module'
            ],
            [
                'module'  => 'create_module',
                'description'  => 'Tambah Module'
            ],
            [
                'module'  => 'edit_module',
                'description'  => 'Ubah Module'
            ],
            [
                'module'  => 'delete_module',
                'description'  => 'Hapus Module'
            ],
//Term Condition
            [
                'module'  => 'list_term',
                'description'  => 'Daftar Term and Condition'
            ],
            [
                'module'  => 'create_term',
                'description'  => 'Tambah Term and Condition'
            ],
            [
                'module'  => 'edit_term',
                'description'  => 'Ubah Term and Condition'
            ],
            [
                'module'  => 'delete_term',
                'description'  => 'Hapus Term and Condition'
            ],
//Push Notification
            [
                'module'  => 'list_push',
                'description'  => 'Daftar Notification'
            ],
            [
                'module'  => 'create_push',
                'description'  => 'Tambah Notification'
            ],
            [
                'module'  => 'edit_push',
                'description'  => 'Ubah Notification'
            ],
            [
                'module'  => 'delete_push',
                'description'  => 'Hapus Notification'
            ],
//Setting
            [
                'module'  => 'list_setting',
                'description'  => 'Daftar Setting'
            ],
            [
                'module'  => 'create_setting',
                'description'  => 'Tambah Setting'
            ],
            [
                'module'  => 'edit_setting',
                'description'  => 'Ubah Setting'
            ],
            [
                'module'  => 'delete_setting',
                'description'  => 'Hapus Setting'
            ],
//TopUpType
            [
                'module'  => 'list_topuptype',
                'description'  => 'Daftar Topup Type'
            ],
            [
                'module'  => 'create_topuptype',
                'description'  => 'Tambah Topup Type'
            ],
            [
                'module'  => 'edit_topuptype',
                'description'  => 'Ubah Topup Type'
            ],
            [
                'module'  => 'delete_topuptype',
                'description'  => 'Hapus Topup Type'
            ],
//TopUpStatus
            [
                'module'  => 'list_topupstatus',
                'description'  => 'Daftar Topup Status'
            ],
            [
                'module'  => 'create_topupstatus',
                'description'  => 'Tambah Topup Status'
            ],
            [
                'module'  => 'edit_topupstatus',
                'description'  => 'Ubah Topup Status'
            ],
            [
                'module'  => 'delete_topupstatus',
                'description'  => 'Hapus Topup Status'
            ],
//Privacy Policy
            [
                'module'  => 'list_privacy',
                'description'  => 'Daftar Privacy Policy'
            ],
            [
                'module'  => 'create_privacy',
                'description'  => 'Tambah Privacy Policy'
            ],
            [
                'module'  => 'edit_privacy',
                'description'  => 'Ubah Privacy Policy'
            ],
            [
                'module'  => 'delete_privacy',
                'description'  => 'Hapus Privacy Policy'
            ],
//Bank Account
            [
                'module'  => 'list_bank',
                'description'  => 'Daftar Akun Bank'
            ],
            [
                'module'  => 'create_bank',
                'description'  => 'Tambah Akun Bank'
            ],
            [
                'module'  => 'edit_bank',
                'description'  => 'Ubah Akun Bank'
            ],
            [
                'module'  => 'delete_bank',
                'description'  => 'Hapus Akun Bank'
            ],
//TopUp
            [
                'module'  => 'list_topup',
                'description'  => 'Daftar Topup'
            ],
            [
                'module'  => 'create_topup',
                'description'  => 'Tambah Topup'
            ],
            [
                'module'  => 'detail_topup',
                'description'  => 'Check Topup'
            ],
            [
                'module'  => 'edit_topup',
                'description'  => 'Ubah Topup'
            ],
            [
                'module'  => 'delete_topup',
                'description'  => 'Hapus Topup'
            ],
//Slide/Marketing
            [
                'module'  => 'list_slide',
                'description'  => 'Daftar Marketing Slide'
            ],
            [
                'module'  => 'create_slide',
                'description'  => 'Tambah Marketing Slide'
            ],
            [
                'module'  => 'detail_slide',
                'description'  => 'Check Marketing Slide'
            ],
            [
                'module'  => 'edit_slide',
                'description'  => 'Ubah Marketing Slide'
            ],
            [
                'module'  => 'delete_slide',
                'description'  => 'Hapus Marketing Slide'
            ],
//Log Admin
            [
                'module'  => 'list_log',
                'description'  => 'Daftar Log Admin'
            ],
            [
                'module'  => 'delete_log',
                'description'  => 'Hapus Log Admin'
            ],
//Feedback
            [
                'module'  => 'list_feedback',
                'description'  => 'Daftar FeedBack'
            ],
            [
                'module'  => 'reply_feedback',
                'description'  => 'Reply FeedBack'
            ],
            [
                'module'  => 'delete_feedback',
                'description'  => 'Hapus FeedBack'
            ],
//Analytics
            [
                'module'  => 'list_analytics',
                'description'  => 'Daftar Analytics'
            ],
//Comment
            [
                'module'  => 'list_comment',
                'description'  => 'Daftar Comment'
            ],
            [
                'module'  => 'delet_comment',
                'description'  => 'Hapus Comment'
            ],
        ]);
    }
}
