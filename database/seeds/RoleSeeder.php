<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::truncate();
        // DB::table('cfg_role')->truncate();
        $role = new Role;
        $role->role = 'Administrator';
        $role->description = 'Super User';
        $role->save();
    }
}
