<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Models\Role;
use App\Models\UserRole;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        UserRole::truncate();
        // DB::table('users')->insert([
        //     'email'  => 'root@root.com',
        //     'password'  => bcrypt('admin'),
        //     'name'      => 'Nuli Giarsyani'
        // ]);

        $role = Role::where('role', 'Administrator')->first();

        $usr = new User;
        $usr->email = 'admin@example.com';
        $usr->password = bcrypt('111111');
        $usr->name = 'Nuli Giarsyani';
        $usr->save();
        $usr->roles()->attach($role);
    }
}
