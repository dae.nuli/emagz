<?php
namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Message;
use App\Models\MessageReply;
use Carbon\Carbon;

class FeedbackTransformer extends TransformerAbstract {
    protected $availableIncludes = [
        'content'
    ];

    public function __construct($id)
    {
        $this->msg_id = $id;
    }

	public function transform(Message $table)
    {
        // $data = ($this->msg_id) ? $this->reply($this->msg_id) : null;
        $row['id'] = (int) $table->id;
        $row['user_name'] = $table->bpUser->full_name;
        $row['title'] = $table->title;
        $row['content'] = $table->content;
        // $row['is_read'] = (boolean) $table->is_read;
        if ($this->msg_id==null) {
            $msg = MessageReply::orderBy('id', 'desc')
                        ->where('bp_message_id', $table->id)
                        ->where('is_admin', 1)
                        ->where('is_read', 0)
                        // ->take(1)
                        ->count();
            $row['reply'] = $msg;
        }
        $row['created_at'] = Carbon::parse($table->created_at)->toDateTimeString();
        if ($this->msg_id) {
            $row['data_reply'] = $this->reply($this->msg_id);
        }
        return $row;
        // return [
        //     'id' => (int) $table->id,
        //     // 'bp_user_id' => $table->bp_user_id,
        //     'title' => $table->title,
        //     'content' => $table->content,
        //     'is_read' => (boolean) $table->is_read,
        //     'message_reply' => $data
        // ];
    }

    // public function reply($id, $user)
    public function reply($id)
    {
        $data = [];
        $reply = MessageReply::orderBy('id', 'asc')->where('bp_message_id', $id)
                // ->where('user_id', $user)
                ->get();
                // ->where('is_admin', 0)->get();
        foreach ($reply as $key => $value) {
            // $data[$key]['bp_message_id'] = $value->id;
            // $data[$key]['user_id'] = $value->id;
            $data[$key]['user_name'] = ($value->is_admin) ? 'Admin' : $value->user($value->user_id, $value->is_admin);
            $data[$key]['content'] = $value->content;
            $data[$key]['is_admin'] = (boolean) $value->is_admin;
            $data[$key]['is_read'] = (boolean) $value->is_read;
            $data[$key]['created_at'] = Carbon::parse($value->created_at)->toDateTimeString();
        }
        return $data;
    }

}