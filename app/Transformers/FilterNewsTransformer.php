<?php
namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\News;

class FilterNewsTransformer extends TransformerAbstract {
    protected $availableIncludes = [
        'content'
    ];
    public function transform(News $news)
    {
        return [
            'id' => (int) $news->id,
            'title' => $news->title,
            'content' => $news->content,
            'date_published' => $news->date_published,
            'thumbnail' => asset('news_thumbnail/'.$news->thumbnail),
            'is_published' => (boolean) $news->is_published
        ];
    }

}