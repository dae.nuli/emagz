<?php
namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Buletin;
use App\Models\Comment;
use App\Models\Article;
use App\Models\Order;
use Carbon\Carbon;

class CommentTransformer extends TransformerAbstract {
    protected $availableIncludes = [
        'content'
    ];
	public function transform(Comment $table)
    {
        $rating = $table->countRating($table->bp_article_id, $table->bp_user_id);
        return [
            'id' => (int) $table->id,
            'bp_user_id' => $table->bp_user_id,
            'bp_article_id' => $table->bp_article_id,
            'rating' => isset($rating->rating) ? $rating->rating : 0,
            'comment' => $table->comment,
            'photo' => isset($table->bpuser) ? asset('customers/'.$table->bpuser->photo_filename) : null,
            'name' => isset($table->bpuser) ? $table->bpuser->full_name : null,
            'created_at' => Carbon::parse($table->created_at)->toDateTimeString()
        ];
    }

}