<?php
namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\TopUp;

class HistoryTopUpTransformer extends TransformerAbstract {
    protected $availableIncludes = [
        'content'
    ];
	public function transform(TopUp $topup)
    {
        return [
            'id' => (int) $topup->id,
            'topup_code' => $topup->topup_code,
            'amount' => $topup->amount,
            'topup_date' => $topup->topup_date,
            'account_name' => $topup->account_name,
            'confirmation_date' => $topup->confirmation_date,
            'file' => asset('topupattachment/'.$topup->file_attachment),
            'topup_status' => (boolean) $topup->status->topup_status
        ];
    }

}






