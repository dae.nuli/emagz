<?php
namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\BankAccount;
use App\Models\PaymentMethod;

class BankTransformer extends TransformerAbstract {
    protected $availableIncludes = [
        'content'
    ];
	public function transform(BankAccount $table)
    {
        $data = $this->method($table->id);
        return [
            'id' => (int) $table->id,
            'bank_name' => $table->bank_name,
            'account_name' => $table->account_name,
            'account_number' => $table->account_no,
            'image' => asset('logobank/'.$table->logo),
            'description' => $table->date_published,
            'method' => $data
        ];
    }

    public function method($id)
    {
        $data = [];
        $dataMethod = PaymentMethod::where('bank_id', $id)->get();
        foreach ($dataMethod as $key => $value) {
            $data[$key]['id'] = $value->id;
            $data[$key]['title'] = $value->title;
            $data[$key]['description'] = $value->description;
        }
        return $data;
    }

}