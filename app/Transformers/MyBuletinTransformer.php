<?php
namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Buletin;
use App\Models\Comment;
use App\Models\Article;
use App\Models\Order;
use Carbon\Carbon;

class MyBuletinTransformer extends TransformerAbstract {
    protected $availableIncludes = [
        'content'
    ];

    public function __construct($id)
    {
        $this->user = $id;
    }

	public function transform(Buletin $table)
    {
        $data = $this->article($table->id, $this->user);
        $purchased = Order::where('bp_user_id', $this->user)->where('bp_buletin_id', $table->id)->count();
        // if ($purchased) {
            return [
                'id' => (int) $table->id,
                'buletin_code' => $table->buletin_code,
                'edition' => $table->edition,
                'buletin_title' => $table->buletin_title,
                // 'price' => $table->price,
                'purchased_count' => $table->purchased_count,
                'date_published' => $table->date_published,
                'file_name' => $table->filename,
                // 'file' => asset('buletin/file/'.$table->filename),
                'thumbnail' => ($table->thumbnail_filename) ? asset('buletin/thumbnail/'.$table->thumbnail_filename) : null,
                'is_free' => (boolean) $table->is_free,
                'is_published' => (boolean) $table->is_published,
                'is_purchased' => ($purchased) ? true : false,
                'articles' => $data
            ];
        // }
    }

    public function article($id, $user)
    {
        // return;
        $data = [];
        $dataArt = Article::where('bp_buletin_id', $id)->get();
        foreach ($dataArt as $key => $value) {
            $data[$key]['id'] = $value->id;
            $data[$key]['title'] = $value->title;
            $data[$key]['description'] = $value->description;
            $data[$key]['rating'] = isset($value->rateByUser($id, $user)->rating) ? $value->rateByUser($id, $user)->rating : 0;
            // $data[$key]['rating'] = isset($value->rateByUser($id, $user)->rating) ? $value->rateByUser($id, $user)->rating : 0;
            // $data[$key]['comment_count'] = Comment::where('bp_article_id', $value->id)->count();
            $data[$key]['comment_count'] = $value->comment()->count();
            $data[$key]['author'] = $value->author;
            $data[$key]['created_at'] = Carbon::parse($value->created_at)->toDateTimeString();
        }
        return $data;
    }

}