<?php
namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Slide;

class SlideMarketingTransformer extends TransformerAbstract {
    protected $availableIncludes = [
        'content'
    ];
	public function transform(Slide $table)
    {
        return [
            'id' => (int) $table->id,
            'title' => $table->title,
            'description' => $table->description,
            'image' => asset('slidemarketing/'.$table->filename)
        ];
    }

}