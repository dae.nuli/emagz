<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Models\BpUser;
use App\Models\LogUserToken;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
class SendFcm extends Notification implements ShouldQueue
{
    use Queueable;
 private $buser;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(BpUser $buser)
    {
        //
          $this->buser = $buser;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['Fcm'];
    }


    public function toFcm($notifiable='')
    {
          $log = LogUserToken::where('bp_user_id', $this->buser->id)
                ->where('is_taken', 'f')
                ->where('expired_date', '>=',Carbon::now())
                ->orderBy('id','desc')
                ->first();

          $result=Mail::send('welcome',['data'=>$log], function ($message) use ($data) {
                            $message->to('racker2000@gmail.com')->subject('testing dari fcm');
                        });
          return $result;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
   
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'buser' => $this->buser->id
        ];
    }
}
