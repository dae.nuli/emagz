<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Models\BpUser;
use App\Models\LogUserToken;
use Carbon\Carbon;

class SendActivationEmail extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    private $buser;

    public function __construct($buser)
    {
        $this->buser = $buser;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $log = LogUserToken::where('bp_user_id', $this->buser->id)
                ->where('is_taken', 'f')
                ->where('expired_date', '>=',Carbon::now())
                ->orderBy('id','desc')
                ->first();

        return (new MailMessage)
                    ->subject('Kode Aktivasi')
                    ->line($log->code_activation)
                    ->greeting('Kode Aktivasi')
                    // ->action($log->code_activation, url('#'))
                    ->line('Terimakasih !');
                    // ->markdown('vendor.mail.welcome.index');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'buser' => $this->buser->id
        ];
    }
}
