<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Traits\CrudTrait;
use App\Models\Order;
use App\Models\TopUp;

use Datatables;
use Form;
use Carbon\Carbon;

class ReportController extends Controller
{ 

    public function __construct()
    {
       
    }

    public function getBuletin()
    {
         $data['title'] = 'Laporan Pembelian Buletin';    
        $data['url'] = url('laporan/buletin');
        $data['ajax'] = url('admin/laporan/buletin/data');
        $data['create'] = '';

        return view('admin.report.buletin',$data);
    }

    public function dataBuletin(Request $request)
    {
        if ($request->ajax()) {
           
        $query = array();                
        if($request->filter==true){

            $query = Order::selectRaw(" bp_order.id , bp_order.purchase_date as tanggal , bp_order.order_code as code , 
                     bp_users.full_name as pembeli , 
                    bp_buletin.buletin_title as buletin , bp_order.amount as jumlah  ")
            ->leftJoin('bp_users','bp_users.id','=','bp_order.bp_user_id')
            ->leftJoin('bp_buletin','bp_buletin.id','=','bp_order.bp_buletin_id')
            ->orderBy('purchase_date','DESC');
     
            $query= $query->whereRaw("
                      ( bp_order.purchase_date between '".$request->tgl1."' 
                    and '".$request->tgl2."' )
                    and bp_order.is_purchase_succeeded = 't'
                 ");
            
            $query=$query->get();
             return Datatables::of($query)             
            ->make(true);
        }

       return Datatables::empty()
       ->make(true);
           

        }
    }


    public function getTopup(Request $request)
    {
          $data['title'] = 'Laporan Topup Masuk';    
        $data['url'] = url('admin/laporan/topup');
        $data['ajax'] = url('admin/laporan/topup/data');
        $data['create'] = '';

        return view('admin.report.topup',$data);
    }

       public function dataTopup(Request $request)
    {
        if ($request->ajax()) {
           
        $query = array();                
        if($request->filter==true){

            $query = TopUp::selectRaw(" bp_topup.id ,
                bp_topup.confirmation_date as tgl ,
                bp_topup.topup_code as kode ,
                cfg_bank_account.bank_name as bank_tujuan ,
                bp_topup.bank_name as bank_asal ,
                bp_users.full_name  as nama ,
                bp_topup.amount as jumlah 

             ")
            ->leftJoin('bp_users','bp_users.id','=','bp_topup.bp_user_id')
            ->leftJoin('cfg_bank_account','cfg_bank_account.id','=','bp_topup.bank_id')
            ->orderBy('bp_topup.topup_date','DESC')
            ->where('bp_topup.status',2);
     
            $query= $query->whereRaw("
                      ( bp_topup.topup_date between '".$request->tgl1."' 
                    and '".$request->tgl2."' )
                     
                 ");
            
            $query=$query->get();
             return Datatables::of($query)             
            ->make(true);
        }

           return Datatables::empty()
           ->make(true);
               

        }
    }

    
}
