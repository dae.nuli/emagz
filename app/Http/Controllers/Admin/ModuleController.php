<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Traits\CrudTrait;
use App\Models\Module;
use Datatables;
use Form;

class ModuleController extends Controller
{
    private $folder = 'admin.module';
    private $uri = 'admin.module';
    private $title = 'Module';
    private $desc = 'Description';

    use CrudTrait;

    public function __construct(Module $table)
    {
        $this->middleware('permission:list_module', ['only' => ['index','data']]);
        $this->middleware('permission:create_module', ['only' => ['create','store']]);
        $this->middleware('permission:edit_module', ['only' => ['edit','update']]);
        $this->middleware('permission:delete_module', ['only' => ['destroy','postDeleteAll']]);
        $this->table = $table;
        $this->url = route($this->uri.'.index');
    }

    public function index()
    {
        $data['title'] = $this->title;
        // $data['subTitle'] = 'edit';
        $data['url'] = $this->url;

        $data['ajax'] = route($this->uri.'.data');
        $data['create'] = route($this->uri.'.create');

        return view($this->folder.'.index',$data);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $index = $this->table->select(['id', 'module', 'description'])
            ->orderBy('id', 'desc');
            return Datatables::of($index)
            ->addColumn('action', function ($index) {
                $tag = Form::open(array("url" => route($this->uri.'.destroy',$index->id), "method" => "DELETE", "class"=>"form"));
                $tag .= (auth()->user()->can('edit_module')) ? "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'>Edit</a>" : '';
                // $tag .= " <a href=".route($this->uri.'.show',$index->id)." class='btn btn-success btn-xs'>Show</a>";
                $tag .= (auth()->user()->can('delete_module')) ? " <button type='submit' class='delete btn btn-danger btn-xs'>Delete</button>" : '';
                $tag .= Form::close();
                return $tag;
            })
            ->rawColumns(['action'])
            ->make(true);
        }
    }

    public function create(FormBuilder $formBuilder)
    {
        $data['form'] = $formBuilder->create('App\Forms\ModuleForm', [
            'method' => 'POST',
            'url' => route($this->uri.'.store')
        ]);
        $data['title'] = $this->title;
        $data['subTitle'] = 'create';

        $data['url'] = $this->url;

        return view($this->folder.'.create', $data);
    }

    public function edit($id, FormBuilder $formBuilder)
    {
        $users = $this->table->findOrFail($id);
        $data['form'] = $formBuilder->create('App\Forms\ModuleForm', [
            'method' => 'PUT',
            'model' => $users,
            'url' => route($this->uri.'.update', $id)
        ]);
        $data['title'] = $this->title;
        $data['subTitle'] = 'edit';

        $data['url'] = $this->url;

        return view($this->folder.'.create', $data);
    }
}
