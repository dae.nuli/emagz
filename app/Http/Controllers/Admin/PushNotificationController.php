<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Kris\LaravelFormBuilder\FormBuilder;
use App\Traits\CrudTrait;
use Form;

use App\Models\PushNotif;
use App\Helpers\Activity;

use Illuminate\Support\Facades\Auth;
use LaravelFCM\Message\Topics;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;
use Carbon\Carbon;
use Datatables;

class PushNotificationController extends Controller
{
    private $folder = 'admin.announcement';
    private $uri = 'admin.announcement';
    private $title = 'Announcement';
    private $desc = 'Description';

    use CrudTrait;

    public function __construct(PushNotif $table)
    {
        $this->middleware('permission:list_push', ['only' => ['index','data']]);
        $this->middleware('permission:create_push', ['only' => ['create','store']]);
        $this->middleware('permission:edit_push', ['only' => ['edit','update']]);
        $this->middleware('permission:delete_push', ['only' => ['destroy','postDeleteAll']]);
        $this->table = $table;
        $this->url = route($this->uri.'.index');
    }

    public function index()
    {
        $data['title'] = $this->title;
        $data['url'] = $this->url;

        $data['ajax'] = route($this->uri.'.data');
        $data['create'] = route($this->uri.'.create');

        return view($this->folder.'.index',$data);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $index = $this->table->select(['id', 'title', 'content', 'is_published', 'published_date', 'feature_source'])
            ->orderBy('id', 'desc');
            return Datatables::of($index)
            ->editColumn('published_date', function ($index) {
                return ($index->published_date) ? Carbon::parse($index->published_date)->format('d F Y, H:i:s') : '-';
            })
            ->editColumn('is_published', function ($index) {
                if($index->is_published) {
                    return '<span class="badge bg-teal">PUBLISHED</span>';
                } else {
                    return '<span class="badge bg-pink">UNPUBLISHED</span>';
                }
            })
            ->addColumn('action', function ($index) {
                $tag = Form::open(array("url" => route($this->uri.'.destroy',$index->id), "method" => "DELETE", "class"=>"form"));
                $tag .= (auth()->user()->can('edit_push')) ? "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'>Edit</a>" : '';
                $tag .= ($index->is_published) ? " <a href=".route($this->uri.'.unpublish',$index->id)." class='btn btn-warning btn-xs unpublish'>UNPUBLISH</a>" : " <a href=".route($this->uri.'.publish',$index->id)." class='btn btn-success btn-xs publish'>PUBLISH</a>";
                $tag .= (auth()->user()->can('delete_push')) ? " <button type='submit' class='delete btn btn-danger btn-xs'>Delete</button>" : '';
                $tag .= Form::close();
                return $tag;
            })
            ->rawColumns(['is_published', 'content', 'action'])
            ->make(true);
        }
    }

    public function create(FormBuilder $formBuilder)
    {
        $data['form'] = $formBuilder->create('App\Forms\PushNotifForm', [
            'method' => 'POST',
            'url' => route($this->uri.'.store')
        ]);
        $data['title'] = $this->title;
        $data['subTitle'] = 'create';

        $data['url'] = $this->url;

        return view($this->folder.'.create', $data);
    }

    public function edit($id, FormBuilder $formBuilder)
    {
        $users = $this->table->findOrFail($id);
        $data['form'] = $formBuilder->create('App\Forms\PushNotifForm', [
            'method' => 'PUT',
            'model' => $users,
            'url' => route($this->uri.'.update', $id)
        ])->modify('published_date', 'text', [
            'label' => 'Tanggal',
            'attr' => [
                'required' => '',
                'class' => 'datetimepicker form-control'
            ],
            'value' => function ($val) {
                return Carbon::parse($val)->format('Y/m/d H:i');
            }
        ]);
        $data['title'] = $this->title;
        $data['subTitle'] = 'edit';

        $data['url'] = $this->url;

        return view($this->folder.'.create', $data);
    }

    public function store(Request $request)
    {

        Activity::log('create', $this->title, $request->title);
        // Activity::log('create', $this->title);
        $request->merge([
            'user_id' => Auth::id(),
            'is_published' => 0,
            'topic' => 'dev-umum',
            'feature_source' => 'pengumuman',
            'published_date' => Carbon::parse($request->published_date)->format('Y-m-d H:i:s')
        ]);
        $this->table->create($request->all());

        // $this->table->create([
        //     'user_id' => Auth::id(),
        //     'title' => $request->title,
        //     'content' => $request->content,
        //     'is_published' => 1,
        //     'published_date' => Carbon::parse($request->published_date)->format('Y-m-d H:i:s'),
        //     // 'topic' => 'dev-news',
        //     'feature_source' => $request->feature_source
        // ]);

        // if (!empty($request->is_published)) {

        //     $notificationBuilder = new PayloadNotificationBuilder($request->title);
        //     $notificationBuilder->setBody($request->content)
        //                     ->setSound('default');
                            
        //     $notification = $notificationBuilder->build();

        //     $topic = new Topics();
        //     $topic->topic('dev-news');

        //     $topicResponse = FCM::sendToTopic($topic, null, $notification, null);
        // }

        return redirect(route($this->uri.'.index'))->with('success',trans('message.create'));
    }

    public function update(Request $request, $id)
    {
        Activity::log('edit', $this->title, $request->title);
        // Activity::log('create', $this->title);
        $request->merge([
            // 'is_published' => 1,
            'published_date' => Carbon::parse($request->published_date)->format('Y-m-d H:i:s')
        ]);
        $this->table->find($id)->update($request->all());
        // $this->table->find($id)->update([
        //     'title' => $request->title,
        //     'content' => $request->content,
        //     'is_published' => 1,
        //     'published_date' => Carbon::parse($request->published_date)->format('Y-m-d H:i:s'),
        //     // 'topic' => 'dev-news',
        //     'feature_source' => $request->feature_source
        // ]);

        return redirect(route($this->uri.'.index'))->with('success', trans('message.update'));
    }
    
    public function destroy($id)
    {
        $tb = $this->table->findOrFail($id);
        Activity::log('delete', $this->title, $tb->title);
        $tb->delete();
        return response()->json(['msg' => true,'success' => trans('message.delete')]);
    }
    
    // public function destroy($id)
    // {
    //     $tb = $this->table->findOrFail($id);
    //     $tb->delete();
    //     return response()->json(['msg' => true,'success' => trans('message.delete')]);
    // }

    public function publish($id)
    {
        if (!empty($id)) {
            $tb = $this->table->find($id);

            if (!$tb->is_published) {

                $notificationBuilder = new PayloadNotificationBuilder($tb->title);
                $notificationBuilder->setBody($tb->content)
                                ->setSound('default');
                                
                $notification = $notificationBuilder->build();

                $topic = new Topics();
                $topic->topic($tb->topic);

                $topicResponse = FCM::sendToTopic($topic, null, $notification, null);
                
                $tb->update(['is_published' => 1]);
            }
            return response()->json(['msg' => true,'success' => trans('message.publish')]);
            // return redirect(route($this->uri.'.index'))->with('success', trans('message.publish'));
        }   
    }

    public function unPublish($id)
    {
        if (!empty($id)) {
            $tb = $this->table->find($id);

            if ($tb->is_published) {
                $tb->update(['is_published' => 0]);
            }
            return response()->json(['msg' => true,'success' => trans('message.unpublish')]);
            // return redirect(route($this->uri.'.index'))->with('success', trans('message.unpublish'));
        }   
    }
}
