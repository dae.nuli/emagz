<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Kris\LaravelFormBuilder\FormBuilder;
use App\Traits\CrudTrait;
use Carbon\Carbon;
use App\Helpers\Activity;

use App\Models\Comment;

use Datatables;
use Form;

class CommentController extends Controller
{
    private $folder = 'admin.comment';
    private $uri = 'admin.comment';
    private $title = 'Comment';
    private $desc = 'Description';

    use CrudTrait;

    public function __construct(Comment $table)
    {
        $this->middleware('permission:list_comment', ['only' => ['index','data']]);
        $this->middleware('permission:delete_comment', ['only' => ['destroy','postDeleteAll']]);
        $this->table = $table;
        $this->url = route($this->uri.'.index');
    }

    public function index()
    {
        $data['title'] = $this->title;
        // $data['subTitle'] = 'edit';
        $data['url'] = $this->url;

        $data['ajax'] = route($this->uri.'.data');
        $data['create'] = route($this->uri.'.create');
        $data['urlApproved'] = route($this->uri.'.approve.comment');

        return view($this->folder.'.index',$data);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $index = $this->table->select(['id', 'bp_user_id', 'bp_article_id', 'comment', 'approved_date', 'is_approved', 'created_at'])
            ->orderBy('id', 'desc');
            return Datatables::of($index)
            ->editColumn('bp_user_id', function ($index) {
                if($index->bp_user_id) {
                    return isset($index->bpuser->full_name) ? $index->bpuser->full_name : '-';
                } else {
                    return '-';
                }
            })
            ->editColumn('created_at', function ($index) {
                return ($index->created_at) ? Carbon::parse($index->created_at)->format('d/m/Y, H:i') : '-';
            })
            ->editColumn('approved_date', function ($index) {
                return ($index->approved_date) ? Carbon::parse($index->approved_date)->format('d/m/Y, H:i') : '-';
            })
            // ->addColumn('foto', function ($index) {                 
            //     return isset($index->bpuser->photo_filename) ? "<img src='".asset('customers/'.$index->bpuser->photo_filename)."' width='120' alt='img'/>" : '-';
            // })
            ->editColumn('bp_article_id', function ($index) {
                if($index->bp_article_id) {
                    return isset($index->article->title) ? str_limit($index->article->title, 20) : '-';
                } else {
                    return '-';
                }
            })
            ->editColumn('is_approved', function ($index) {
                if ($index->is_approved) {
                    return '<input id="is_approved_'.$index->id.'" name="is_approved" class="is_approved" data-id="'.$index->id.'" type="checkbox" value="1" checked>
                    <label for="is_approved_'.$index->id.'" class="">Approve</label>';
                } else {
                    return '<input id="is_approved_'.$index->id.'" name="is_approved" class="is_approved" data-id="'.$index->id.'" type="checkbox" value="1">
                    <label for="is_approved_'.$index->id.'" class="">Approve</label>';
                }
            })
            ->addColumn('action', function ($index) {
                    $tag = Form::open(array("url" => route($this->uri.'.destroy',$index->id), "method" => "DELETE", "class"=>"form"));
                    $tag .= " <button type='submit' class='delete btn btn-danger btn-xs'>Delete</button>";
                    $tag .= Form::close();
                    return $tag;
            })
            ->rawColumns(['foto','is_approved','action'])
            ->make(true);
        }
    }

    public function approveComment(Request $request)
    {
        if ($request->ajax()) {
            if (!empty($request->id)) {
                $status = $request->is_approved;
                // $status = ($request->is_approved == true) ? 1 : 0;
                // if ($status == 1) {
                Comment::where('id', $request->id)->update(['is_approved' => $status, 'approved_date' => ($request->is_approved == 1) ? Carbon::now() : null]);
                // } else {
                //     Comment::where('id', $request->id)->update(['is_approved' => $status]);
                // }
                return response()->json(['status' => true, 'is_approved' => $status]);
            }
        }
    }
    
    // public function destroy($id)
    // {
    //     $tb = $this->table->findOrFail($id);
    //     Activity::log('delete', $this->title);
    //     $tb->delete();
    //     return response()->json(['msg' => true,'success' => trans('message.delete')]);
    // }
}
