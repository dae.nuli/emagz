<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Kris\LaravelFormBuilder\FormBuilder;
use App\Traits\CrudTrait;
use App\Models\RoleModule;
use App\Models\Module;
use App\Models\Role;
use App\Helpers\Activity;
use Form;

use Datatables;

class RoleController extends Controller
{
    private $folder = 'admin.role';
    private $uri = 'admin.role';
    private $title = 'Role';
    private $desc = 'Description';

    use CrudTrait;

    public function __construct(Role $table)
    {
        $this->middleware('permission:list_role', ['only' => ['index','data']]);
        $this->middleware('permission:create_role', ['only' => ['create','store']]);
        $this->middleware('permission:edit_role', ['only' => ['edit','update']]);
        $this->middleware('permission:delete_role', ['only' => ['destroy','postDeleteAll']]);
        $this->table = $table;
        $this->url = route($this->uri.'.index');
    }

    public function index()
    {
        $data['title'] = $this->title;
        // $data['subTitle'] = 'edit';
        $data['url'] = $this->url;

        $data['ajax'] = route($this->uri.'.data');
        $data['create'] = route($this->uri.'.create');

        return view($this->folder.'.index',$data);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $index = $this->table->select(['id', 'role', 'description'])
            ->orderBy('id', 'desc');
            return Datatables::of($index)
            ->addColumn('action', function ($index) {
                $tag = Form::open(array("url" => route($this->uri.'.destroy',$index->id), "method" => "DELETE", "class"=>"form"));
                $tag .= (auth()->user()->can('edit_role')) ? "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'>Edit</a>" : '';
                // $tag .= " <a href=".route($this->uri.'.show',$index->id)." class='btn btn-success btn-xs'>Show</a>";
                $tag .= (auth()->user()->can('delete_role')) ? " <button type='submit' class='delete btn btn-danger btn-xs'>Delete</button>" : '';
                $tag .= Form::close();
                return $tag;
            })
            ->rawColumns(['action'])
            ->make(true);
        }
    }

    public function create(FormBuilder $formBuilder)
    {
        // $data['form'] = $formBuilder->create('App\Forms\RoleForm', [
        //     'method' => 'POST',
        //     'url' => route($this->uri.'.store')
        // ]);
        $data['title'] = $this->title;
        $data['subTitle'] = 'create';

        $data['method'] = 'POST';
        $data['action'] = route($this->uri.'.store');
        $data['url'] = $this->url;
        $data['permission'] = Module::orderBy('id','asc')->get();

        return view($this->folder.'.create', $data);
    }

    public function edit($id, FormBuilder $formBuilder)
    {
        $data['index'] = $this->table->findOrFail($id);
        // $data['form'] = $formBuilder->create('App\Forms\RoleForm', [
        //     'method' => 'PUT',
        //     'model' => $users,
        //     'url' => route($this->uri.'.update', $id)
        // ]);
        $data['title'] = $this->title;
        $data['subTitle'] = 'edit';

        $data['method'] = 'PUT';
        $data['action'] = route($this->uri.'.update', $id);
        $data['url'] = $this->url;
        $data['permission'] = Module::orderBy('id','asc')->get();

        return view($this->folder.'.edit', $data);
    }

    public function store(Request $request)
    {
        Activity::log('create', $this->title);
        $role = $this->table->create(['role' => $request->role, 'description' => $request->description]);

        foreach ($request->permission as $key => $value) {
            // $rm = new RoleModule;
            // $rm->fill($)
            // ::create(['cfg_role_id' => $role->id, 'cfg_module_id' => $value]);
            RoleModule::create(['cfg_role_id' => $role->id, 'cfg_module_id' => $value]);
        }

        return redirect(route($this->uri.'.index'))->with('success',trans('message.create'));
    }

    public function update(Request $request, $id)
    {
        Activity::log('edit', $this->title);
        $this->table->findOrFail($id)
            ->update(['role' => $request->role, 'description' => $request->description]);

        RoleModule::where('cfg_role_id', $id)->delete();

        foreach ($request->permission as $key => $value) {
            RoleModule::create(['cfg_role_id' => $id, 'cfg_module_id' => $value]);
        }

        return redirect(route($this->uri.'.index'))->with('success', trans('message.update'));
    }
    
    // public function destroy($id)
    // {
    //     $tb = $this->table->findOrFail($id);
    //     $tb->delete();
    //     return response()->json(['msg' => true,'success' => trans('message.delete')]);
    // }
}
