<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Kris\LaravelFormBuilder\FormBuilder;
use App\Models\BpUser;
use App\Helpers\Activity;

use Datatables;
use Form, File, Storage;
use Ramsey\Uuid\Uuid;

class CustomersController extends Controller
{
    private $folder = 'admin.customers';
    private $uri = 'admin.customers';
    private $title = 'Customers';
    private $desc = 'Description';

    public function __construct(BpUser $table)
    {
        $this->middleware('permission:list_customer', ['only' => ['index','data']]);
        $this->middleware('permission:create_customer', ['only' => ['create','store']]);
        $this->middleware('permission:edit_customer', ['only' => ['edit','update']]);
        $this->middleware('permission:delete_customer', ['only' => ['destroy','postDeleteAll']]);
        $this->table = $table;
        $this->url = route($this->uri.'.index');
    }

    public function index()
    {
        $data['title'] = $this->title;
        $data['url'] = $this->url;
        $data['ajax'] = route($this->uri.'.data');
        $data['create'] = route($this->uri.'.create');

        return view($this->folder.'.index',$data);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $index = $this->table->select(['id', 'full_name', 'email', 'address', 'amount_balance', 'is_validated'])
            ->orderBy('id', 'desc');
            return Datatables::of($index)
            ->editColumn('is_validated', function ($index) {
                if($index->is_validated) {
                    return 'YES';
                } else {
                    return 'NOT';
                }
            })
            ->editColumn('amount_balance', function ($index) {
                return number_format($index->amount_balance, 0, '', '.');
            })
            ->addColumn('action', function ($index) {
                    $tag = Form::open(array("url" => route($this->uri.'.destroy',$index->id), "method" => "DELETE", "class"=>"form"));
                    $tag .= (auth()->user()->can('edit_customer')) ? "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'>Edit</a>" : '';
                    // $tag .= " <a href=".route($this->uri.'.show',$index->id)." class='btn btn-success btn-xs'>Show</a>";
                    $tag .= (auth()->user()->can('edit_customer')) ? " <button type='submit' class='delete btn btn-danger btn-xs'>Delete</button>" : '';
                    $tag .= Form::close();
                    return $tag;
            })
            ->rawColumns(['action'])
            ->make(true);
        }
    }

    public function create(FormBuilder $formBuilder)
    {
        $data['form'] = $formBuilder->create('App\Forms\CustomersForm', [
            'method' => 'POST',
            'url' => route($this->uri.'.store')
        ]);
        $data['title'] = $this->title;
        $data['subTitle'] = 'create';

        $data['url'] = $this->url;

        return view($this->folder.'.create', $data);
    }

    public function edit($id, FormBuilder $formBuilder)
    {
        $users = $this->table->findOrFail($id);
        $data['form'] = $formBuilder->create('App\Forms\CustomersForm', [
            'method' => 'PUT',
            'model' => $users,
            'url' => route($this->uri.'.update', $id)
        ])->modify('password', 'password', [
            'attr' => ['required' => null],
            'value' => ''
        ])->modify('address', 'text', [
            'attr' => ['required' => ($users->is_validated)?null:'']
        ])->modify('subdistrict', 'text', [
            'attr' => ['required' => ($users->is_validated)?null:'']
        ])->modify('city', 'text', [
            'attr' => ['required' => ($users->is_validated)?null:'']
        ])->modify('province', 'text', [
            'attr' => ['required' => ($users->is_validated)?null:'']
        ])->modify('country', 'text', [
            'attr' => ['required' => ($users->is_validated)?null:'']
        ]);
        $data['title'] = $this->title;
        $data['subTitle'] = 'edit';

        $data['url'] = $this->url;

        return view($this->folder.'.create', $data);
    }

    public function store(Request $request)
    {
        Activity::log('create', $this->title, $request->full_name);
        // Activity::log('create', $this->title);
        $val = $request->file_foto;
        if(isset($val)) {
            $directory = base_path('public/customers');
            $name      = str_random(10).'.'.$val->extension();
            if(!File::isDirectory($directory)){
                File::makeDirectory($directory, 0777);    
            }
            $val->move($directory, $name);
            
            $request->merge(['photo_filename' => $name]);
        }else{
            unset($request['photo_filename']);
        }

        $request->merge([
            'password' => bcrypt(md5($request->password)),
            'uid' => Uuid::uuid4()->getHex()
        ]);

        $tb = $this->table->create($request->all());

        return redirect(route($this->uri.'.index'))->with('success',trans('message.create'));
    }

    public function update(Request $request, $id)
    {
        Activity::log('edit', $this->title, $request->full_name);
        // Activity::log('edit', $this->title);
    	// dd($request->all());
        $val = $request->file_foto;
        if(isset($val)) {
            $directory = base_path('public/customers');
            $name      = rand(111,9999999).'.'.$val->extension();
            if(!File::isDirectory($directory)){
                File::makeDirectory($directory, 0777);    
            }
            $val->move($directory, $name);

            $item = $this->table->findOrFail($id);
            if(!empty($item->photo_filename)){
                if(File::isDirectory($directory)){
                    Storage::delete('customers/'.$item->photo_filename);
                }
            }
            $request->merge(['photo_filename' => $name]);

        }else{
            unset($request['photo_filename']);
        }

        if(empty($request->password)){
            unset($request['password']);
        } else {
            $request->merge(['password' => bcrypt(md5($request->password))]);
        }

        if(empty($request->is_validated)){
            $request->merge(['is_validated' => 0]);
        }

        if(empty($request->is_suspended)){
            $request->merge(['is_suspended' => 0]);
        } else {
            Activity::log('banned', $this->title, $request->full_name);
        }

        $this->table->findOrFail($id)->update($request->all());

        return redirect(route($this->uri.'.index'))->with('success', trans('message.update'));
    }
    
    public function destroy($id)
    {
        $tb = $this->table->findOrFail($id);
        Activity::log('delete', $this->title, $tb->full_name);
        if (!empty($tb->photo_filename)) {
            Storage::delete('customers/'.$tb->photo_filename);
        }
        $tb->delete();
        return response()->json(['msg' => true,'success' => trans('message.delete')]);
        // return redirect(route($this->uri.'.index'))->with('success',trans('message.delete'));
    }

    public function postDeleteAll(Request $request)
    {
        $ID = $request->id;
        if(count($ID)>0){
            foreach ($ID as $key => $value) {
                $type = $this->table->findOrFail($value);
                Storage::delete('customers/'.$type->photo);
                $type->delete();
            }
            return redirect(route($this->uri.'.index'))->with('success',trans('message.delete.all'));
        }
    }
}
