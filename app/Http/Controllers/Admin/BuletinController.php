<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Buletin;
use App\Models\Comment;
use App\Models\Article;
use App\Traits\CrudTrait;
use Kris\LaravelFormBuilder\FormBuilder;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Activity;

use Datatables;
use Form, File, Storage;
use Carbon\Carbon;
use App\Models\PushNotif;

use LaravelFCM\Message\Topics;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;
class BuletinController extends Controller
{
    private $folder = 'admin.buletin';
    private $uri = 'admin.buletin';
    private $title = 'Buletin';
    private $desc = 'Description';

    use CrudTrait;

    public function __construct(Buletin $table)
    {
        $this->middleware('permission:list_buletin', ['only' => ['index','data']]);
        $this->middleware('permission:create_buletin', ['only' => ['create','store']]);
        $this->middleware('permission:edit_buletin', ['only' => ['edit','update']]);
        $this->middleware('permission:delete_buletin', ['only' => ['destroy','postDeleteAll']]);
        $this->table = $table;
        $this->url = route($this->uri.'.index');
    }

    public function index()
    {
        $data['title'] = $this->title;
        $data['desc'] = $this->desc;
        $data['url'] = $this->url;

        $data['ajax'] = route($this->uri.'.data');
        $data['create'] = route($this->uri.'.create');

        return view($this->folder.'.index',$data);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $index = $this->table->select([
	            	'id', 'thumbnail_filename', 'edition', 'buletin_title', 'date_published','price', 'is_published'
            	])
            ->orderBy('id', 'desc');
            return Datatables::of($index)
            ->editColumn('thumbnail_filename', function ($index) {
                return ($index->thumbnail_filename) ? "<img src='".asset('buletin/thumbnail/'.$index->thumbnail_filename)."' width='120' alt='img'/>" : '-';
            })
            ->editColumn('price', function ($index) {
                return ($index->price) ? 'Rp '.number_format($index->price, 0, '', '.') : '-';
            })
            ->addColumn('comment', function ($index) {
                $comment = Comment::whereHas('article', function ($query) use ($index) {
                                $query->where('bp_buletin_id', $index->id);
                            })->count();
                return ($comment) ? "<a href=".route($this->uri.'.detail',$index->id).">".$comment."</a>" : 0;
                // $comment = Article::where('bp_buletin_id', $index->id)->pluck('comment_count')->sum();
                // return ($comment) ? "<a href=".route($this->uri.'.comment',$index->id).">".$comment."</a>" : 0;
            })
            ->editColumn('date_published', function ($index) {
                return ($index->date_published) ? Carbon::parse($index->date_published)->format('d F Y, H:i:s') : '-';
            })
            ->editColumn('is_published', function ($index) {
                if($index->is_published) {
                    return '<span class="badge bg-teal">PUBLISHED</span>';
                } else {
                    return '<span class="badge bg-pink">UNPUBLISHED</span>';
                }
            })
            ->addColumn('action', function ($index) {
                    $tag = Form::open(array("url" => route($this->uri.'.destroy',$index->id), "method" => "DELETE", "class"=>"form"));
                    $tag .= (auth()->user()->can('edit_buletin')) ? "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'>Edit</a>" : '';
                    $tag .= " <a href=".route($this->uri.'.detail',$index->id)." class='btn btn-success btn-xs'>Detail</a>";
                    $tag .= ($index->is_published) ? " <a href=".route($this->uri.'.unpublish',$index->id)." class='btn btn-warning btn-xs unpublish'>UNPUBLISH</a>" : " <a href=".route($this->uri.'.publish',$index->id)." class='btn btn-success btn-xs publish'>PUBLISH</a>";
                    $tag .= (auth()->user()->can('delete_buletin')) ? " <button type='submit' class='delete btn btn-danger btn-xs'>Delete</button>" : '';
                    $tag .= Form::close();
                    return $tag;
            })
            ->rawColumns(['action', 'is_published', 'comment', 'thumbnail_filename'])
            ->make(true);
        }
    }

    public function showDetail($id)
    {
        $data['title'] = $this->title;
        $data['subTitle'] = 'artikel';

        $data['url'] = $this->url;
        $data['index'] = $this->table->find($id);
        // $data['comment'] = Comment::whereHas('article', function ($query) use ($id) {
        //                         $query->where('bp_buletin_id', $id);
        //                     })->get();
        $data['ajax'] = route($this->uri.'.data.detail', $id);

        return view($this->folder.'.detail', $data);
    }

    public function dataShowDetail(Request $request, $id)
    {
        if ($request->ajax()) {
            $index = Article::select([
                    'id', 'title', 'thumbnail', 'author'
                ])
            ->orderBy('id', 'desc')
            ->where('bp_buletin_id', $id);
            return Datatables::of($index)
            ->editColumn('thumbnail', function ($index) {
                return ($index->thumbnail) ? "<img src='".asset('article/'.$index->thumbnail)."' width='120' alt='img'/>" : '-';
            })
            ->addColumn('comment', function ($index) use ($id) {
                $comment = Comment::where('bp_article_id', $index->id)->count();
                return ($comment) ? "<a href=".route($this->uri.'.comment',['buletin' => $id, 'id' => $index->id]).">".$comment."</a>" : 0;
            })
            ->addColumn('action', function ($index) {
                    $tag = Form::open(array("url" => route($this->uri.'.destroyArticle',$index->id), "method" => "DELETE", "class"=>"form"));
                    $tag .= " <button type='submit' class='delete btn btn-danger btn-xs'>Delete</button>";
                    $tag .= Form::close();
                    return $tag;
            })
            ->rawColumns(['action', 'comment', 'thumbnail'])
            ->make(true);
        }
    }

    public function showComment($idArticle, $id)
    {
        // return $idArticle;
        $data['title'] = $this->title;
        $data['subTitle'] = 'komentar';

        $data['url'] = $this->url.'/detail/'.$idArticle;
        $data['urlApproved'] = $this->url.'/approve-comment';
        $data['index'] = Article::find($id);
        // $data['comment'] = Comment::whereHas('article', function ($query) use ($id) {
        //                         $query->where('bp_buletin_id', $id);
        //                     })->get();
        $data['ajax'] = route($this->uri.'.data.comment', $id);

        return view($this->folder.'.comment', $data);
    }

    public function dataShowComment(Request $request, $id)
    {
        if ($request->ajax()) {
            $index = Comment::select([
                    'id', 'comment', 'is_approved', 'bp_user_id'
                ])
            ->orderBy('id', 'desc')
            ->where('bp_article_id', $id);
            return Datatables::of($index)
            ->editColumn('bp_user_id', function ($index) {
                return isset($index->bpuser->full_name) ? $index->bpuser->full_name : '-';
            })
            ->addColumn('foto', function ($index) {                 
                return isset($index->bpuser->photo_filename) ? "<img src='".asset('customers/'.$index->bpuser->photo_filename)."' width='120' alt='img'/>" : '-';
            })
            ->editColumn('is_approved', function ($index) {
                if ($index->is_approved) {
                    return '<input id="is_approved_'.$index->id.'" name="is_approved" class="is_approved" data-id="'.$index->id.'" type="checkbox" value="1" checked>
                    <label for="is_approved_'.$index->id.'" class="">Approve</label>';
                } else {
                    return '<input id="is_approved_'.$index->id.'" name="is_approved" class="is_approved" data-id="'.$index->id.'" type="checkbox" value="1">
                    <label for="is_approved_'.$index->id.'" class="">Approve</label>';
                }
            })
            ->addColumn('action', function ($index) {
                    $tag = Form::open(array("url" => route($this->uri.'.destroyComment',$index->id), "method" => "DELETE", "class"=>"form"));
                    $tag .= " <button type='submit' class='delete btn btn-danger btn-xs'>Delete</button>";
                    $tag .= Form::close();
                    return $tag;
            })
            ->rawColumns(['action', 'foto', 'is_approved'])
            ->make(true);
        }
    }

    public function approveComment(Request $request)
    {
        if ($request->ajax()) {
            if (!empty($request->id)) {
                $status = $request->is_approved;
                // $status = ($request->is_approved == true) ? 1 : 0;
                Comment::where('id', $request->id)->update(['is_approved' => $status]);
                return response()->json(['status' => true, 'is_approved' => $status]);
            }
        }
    }

    public function create(FormBuilder $formBuilder)
    {
        // $data['form'] = $formBuilder->create('App\Forms\CreateBuletinForm', [
        //     'method' => 'POST',
        //     'url' => route($this->uri.'.store')
        // ]);

        $data['method'] = 'POST';
        $data['action'] = route($this->uri.'.store');

        $data['articleForm'] = $formBuilder->create('App\Forms\CreateArticleForm');
        $data['title'] = $this->title;
        $data['subTitle'] = 'create';

        $data['url'] = $this->url;

        return view($this->folder.'.create', $data);
    }

    public function edit($id, FormBuilder $formBuilder)
    {
        $data['index'] = $this->table->findOrFail($id);
        // $buletin = $this->table->findOrFail($id);

        $data['method'] = 'PUT';
        $data['action'] = route($this->uri.'.update', $id);
        
        // $data['form'] = $formBuilder->create('App\Forms\CreateBuletinForm', [
        //     'method' => 'PUT',
        //     'model' => $buletin,
        //     'url' => route($this->uri.'.update', $id)
        // ])->modify('date_published', 'text', [
        //     'label' => 'Tanggal',
        //     'attr' => [
        //         'required' => '',
        //         'class' => 'datetimepicker form-control'
        //     ],
        //     'value' => function ($val) {
        //         return Carbon::parse($val)->format('Y/m/d H:i');
        //     }
        // ])->modify('price', 'text', [
        //     'label' => 'Harga',
        //     'attr' => [
        //         'required' => '',
        //         'class' => 'price form-control'
        //     ],
        //     'value' => function ($val) {
        //         return ($val) ? 'Rp '.number_format($val, 0, '', '.') : '-';
        //     }
        // ])->modify('file_buletin', 'file', [
        //     'attr' => [
        //         'required' => ''
        //     ],
        //     'value' => function ($val) {
        //         return ($val) ? $val : '';
        //     }
        // ]);
        $data['title'] = $this->title;
        $data['subTitle'] = 'edit';

        $data['url'] = $this->url;
        $data['articles'] = Article::where('bp_buletin_id', $id)->get();

        return view($this->folder.'.edit', $data);
    }

    public function store(Request $request)
    {

        Activity::log('create', $this->title, $request->buletin_title);
        $val_buletin = $request->file_buletin;
        if(isset($val_buletin)) {
            $directory = base_path('public/buletin');
            $directory_file = base_path('public/buletin/file');
            $name_file = str_random(10).'.'.$val_buletin->extension();
            if(!File::isDirectory($directory)){
                File::makeDirectory($directory, 0777);    
            }
            if(!File::isDirectory($directory_file)){
                File::makeDirectory($directory_file, 0777);
            }
            $val_buletin->move($directory_file, $name_file);

            $request->merge(['filename' => $name_file]);
        }else{
            unset($request['filename']);
        }


        $valThumb = $request->file_thumbnail;
        if(isset($valThumb)) {
            $directory = base_path('public/buletin');
            $directory_file_thumbnail = base_path('public/buletin/thumbnail');
            $name_thumbnail = str_random(10).'.'.$valThumb->extension();
            if(!File::isDirectory($directory)){
                File::makeDirectory($directory, 0777);    
            }
            if(!File::isDirectory($directory_file_thumbnail)){
                File::makeDirectory($directory_file_thumbnail, 0777);
            }
            $valThumb->move($directory_file_thumbnail, $name_thumbnail);
            
            $request->merge(['thumbnail_filename' => $name_thumbnail]);
        }else{
            unset($request['thumbnail_filename']);
        }

        if(isset($request->price)){
            $request->merge(['price' => str_replace('.','',$request->price)]);
        }

        if(empty($request->is_free)){
            $request->merge(['is_free' => 0]);
        }

        if(empty($request->is_published)){
            $request->merge(['is_published' => 0]);
        }

      /*  $tb = $this->table->create($request->except([
                'title_article', 'description_article', 'author_article', 'file_thumbnail_article'
            ])); */
            
        $tb = new Buletin;
        $tb->fill($request->except([
                'title_article', 'description_article', 'author_article', 'file_thumbnail_article'
            ]));
        $tb->save();

        if (count($request->title_article)) {
            foreach ($request->title_article as $key => $value) {

                $valArticle = $request->file_thumbnail_article[$key];
                $thumbnailArticle = null;
                if(isset($valArticle)) {
                    $directoryArticle = base_path('public/article');
                    $thumbnailArticle = str_random(10).'.'.$valArticle->extension();
                    if(!File::isDirectory($directoryArticle)){
                        File::makeDirectory($directoryArticle, 0777);    
                    }
                    $valArticle->move($directoryArticle, $thumbnailArticle);
                }

                Article::create([
                    'bp_buletin_id' => $tb->id, 'title' => $value,
                    'description' => $request->description_article[$key],
                    'thumbnail' => $thumbnailArticle,'author' => $request->author_article[$key]
                ]);

            }
        }


        if (!empty($request->is_published)) {
            
            $notificationBuilder = new PayloadNotificationBuilder($request->buletin_title);
            $notificationBuilder->setBody($request->buletin_title)
                            ->setSound('default');
                            
            $notification = $notificationBuilder->build();

            $topic = new Topics();
            $topic->topic('dev-bul');

            $topicResponse = FCM::sendToTopic($topic, null, $notification, null);

            PushNotif::create([
                'user_id' => Auth::id(),
                'title' => $request->buletin_title,
                'content' => $request->buletin_title,
                'is_published' => 1,
                // 'is_published' => !empty($request->is_published) ? 1 : 0,
                'published_date' => Carbon::parse($request->date_published)->format('Y-m-d H:i:s'),
                'topic' => 'dev-bul',
                'feature_source' => 'buletin'
            ]);
        }

        return redirect(route($this->uri.'.index'))->with('success',trans('message.create'));
    }

    public function update(Request $request, $id)
    {

        Activity::log('edit', $this->title, $request->buletin_title);
        $val_buletin = $request->file_buletin;
        if(isset($val_buletin)) {
            $directory = base_path('public/buletin');
            $directory_file = base_path('public/buletin/file');
            $name_file = str_random(10).'.'.$val_buletin->extension();
            if(!File::isDirectory($directory)){
                File::makeDirectory($directory, 0777);    
            }
            if(!File::isDirectory($directory_file)){
                File::makeDirectory($directory_file, 0777);
            }
            $val_buletin->move($directory_file, $name_file);

            $item = $this->table->findOrFail($id);
            if(!empty($item->filename)){
                if(File::isDirectory($directory_file)){
                    Storage::delete('buletin/file/'.$item->filename);
                }
            }

            $request->merge(['filename' => $name_file]);
        }else{
            unset($request['filename']);
        }


        $valThumb = $request->file_thumbnail;
        if(isset($valThumb)) {
            $directory = base_path('public/buletin');
            $directory_file_thumbnail = base_path('public/buletin/thumbnail');
            $name_thumbnail = str_random(10).'.'.$valThumb->extension();
            if(!File::isDirectory($directory)){
                File::makeDirectory($directory, 0777);    
            }
            if(!File::isDirectory($directory_file_thumbnail)){
                File::makeDirectory($directory_file_thumbnail, 0777);
            }

            $item = $this->table->findOrFail($id);
            if(!empty($item->thumbnail_filename)){
                if(File::isDirectory($directory_file_thumbnail)){
                    Storage::delete('buletin/thumbnail/'.$item->thumbnail_filename);
                }
            }
            $valThumb->move($directory_file_thumbnail, $name_thumbnail);
            
            $request->merge(['thumbnail_filename' => $name_thumbnail]);
        }else{
            unset($request['thumbnail_filename']);
        }


        if(isset($request->price)){
            $request->merge(['price' => str_replace('.','',$request->price)]);
        }

        if(empty($request->is_free)){
            $request->merge(['is_free' => 0]);
        }

        if(empty($request->is_published)){
            $request->merge(['is_published' => 0]);
        }

        $this->table->findOrFail($id)->update($request->except([
                'title_article', 'description_article', 'author_article', 'file_thumbnail_article'
            ]));

        // PushNotif::create([
        //     'user_id' => Auth::id(),
        //     'title' => $request->buletin_title,
        //     'content' => $request->buletin_title,
        //     'is_published' => empty($request->is_published) ? 0 : 1,
        //     'published_date' => Carbon::parse($request->date_published)->format('Y-m-d H:i:s'),
        //     'topic' => 'dev-bul',
        //     'feature_source' => 'buletin'
        // ]);

        // if (!empty($request->is_published)) {
            
        //     $notificationBuilder = new PayloadNotificationBuilder($request->buletin_title);
        //     $notificationBuilder->setBody($request->buletin_title)
        //                     ->setSound('default');
                            
        //     $notification = $notificationBuilder->build();

        //     $topic = new Topics();
        //     $topic->topic('dev-bul');

        //     $topicResponse = FCM::sendToTopic($topic, null, $notification, null);
        // }

        if (count($request->title_article)) {
            // dd($request->all());
            foreach ($request->title_article as $key => $value) {

                $valArticle = isset($request->file_thumbnail_article[$key]) ? $request->file_thumbnail_article[$key]:null;
                $thumbnailArticle = null;
                if(!empty($valArticle)) {
                    $directoryArticle = base_path('public/article');
                    $thumbnailArticle = str_random(10).'.'.$valArticle->extension();
                    if(!File::isDirectory($directoryArticle)){
                        File::makeDirectory($directoryArticle, 0777);    
                    }

                    if (isset($request->id_article[$key])) {
                        $item = $this->table->findOrFail($id);
                        if(!empty($item->thumbnail)){
                            if(File::isDirectory($directoryArticle)){
                                Storage::delete('article/'.$item->thumbnail);
                            }
                        }
                    }
                    $valArticle->move($directoryArticle, $thumbnailArticle);
                }
                if (isset($request->id_article[$key])) {
                    $art = Article::find($request->id_article[$key]);
                    $art->title = $value;
                    $art->description = $request->description_article[$key];
                    $art->author = $request->author_article[$key];
                    if (!empty($valArticle)) {
                    // if (!empty($thumbnailArticle)) {
                        $art->thumbnail = $thumbnailArticle;
                    }
                    $art->save();
                } else {
                    $art = new Article;
                    $art->bp_buletin_id = $id;
                    $art->title = $value;
                    $art->description = $request->description_article[$key];
                    $art->author = $request->author_article[$key];
                    $art->thumbnail = $thumbnailArticle;
                    $art->save();
                }

                // Article::where('id', $request->id_article[$key])
                // ->update([
                //     'title' => $value, 'description' => $request->description_article[$key],
                //     'thumbnail' => $thumbnailArticle,'author' => $request->author_article[$key]
                // ]);
            }
        }

        return redirect(route($this->uri.'.index'))->with('success', trans('message.update'));
    }
    
    public function destroy($id)
    {
        // Activity::log('delete', $this->title);
        $tb = $this->table->findOrFail($id);
        Activity::log('delete', $this->title, $tb->buletin_title);
        if (!empty($tb->filename)) {
            Storage::delete('buletin/file/'.$tb->filename);
        }
        if (!empty($tb->thumbnail_filename)) {
            Storage::delete('buletin/thumbnail/'.$tb->thumbnail_filename);
        }
        $art = Article::where('bp_buletin_id', $id)->get();
        foreach ($art as $key => $value) {
            Comment::where('bp_article_id', $value->id)->delete();
            if (!empty($value->thumbnail)) {
                Storage::delete('article/'.$value->thumbnail);
            }
        }
        Article::where('bp_buletin_id', $id)->delete();
        $tb->delete();
        return response()->json(['msg' => true,'success' => trans('message.delete')]);
    }
    
    public function jsonDeleteArticle(Request $request,$id)
    {
        $art = Article::findOrFail($id);
        Activity::log('delete', 'Article', $art->title);
        Comment::where('bp_article_id', $id)->delete();
        if (!empty($art->thumbnail)) {
            Storage::delete('article/'.$art->thumbnail);
        }
        $art->delete();
        return response()->json(['msg' => true,'success' => trans('message.delete')]);
    }
    
    public function jsonDeleteComment(Request $request,$id)
    {
        Comment::findOrFail($id)->delete();
        return response()->json(['msg' => true,'success' => trans('message.delete')]);
    }
    
    public function destroyArticle($id)
    {
        $art = Article::findOrFail($id);
        Activity::log('delete', 'Article', $art->title);
        if (!empty($art->thumbnail)) {
            Storage::delete('article/'.$art->thumbnail);
        }
        $art->delete();
        return redirect()->back();
    }

    public function publish($id)
    {
        if (!empty($id)) {
            $tb = $this->table->find($id);

            if (!$tb->is_published) {

                $notificationBuilder = new PayloadNotificationBuilder($tb->buletin_title);
                $notificationBuilder->setBody($tb->buletin_title)
                                ->setSound('default');
                                
                $notification = $notificationBuilder->build();

                $topic = new Topics();
                $topic->topic('dev-bul');

                $topicResponse = FCM::sendToTopic($topic, null, $notification, null);
                
                PushNotif::create([
                    'user_id' => Auth::id(),
                    'title' => $tb->buletin_title,
                    'content' => $tb->buletin_title,
                    'is_published' => 1,
                    'published_date' => Carbon::parse($tb->date_published)->format('Y-m-d H:i:s'),
                    'topic' => 'dev-bul',
                    'feature_source' => 'buletin'
                ]);

                $tb->update(['is_published' => 1]);
            }
            return response()->json(['msg' => true,'success' => trans('message.publish')]);
            // return redirect(route($this->uri.'.index'))->with('success', trans('message.publish'));
        }   
    }

    public function unPublish($id)
    {
        if (!empty($id)) {
            $tb = $this->table->find($id);

            if ($tb->is_published) {
                $tb->update(['is_published' => 0]);
            }
            return response()->json(['msg' => true,'success' => trans('message.unpublish')]);
            // return redirect(route($this->uri.'.index'))->with('success', trans('message.unpublish'));
        }   
    }
}
