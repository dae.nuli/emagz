<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kris\LaravelFormBuilder\FormBuilder;
// use App\Traits\CrudTrait;
use App\Models\Message;
use App\Models\MessageReply;
use Datatables;
use Form;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Activity;

class FeedbackController extends Controller
{
    private $folder = 'admin.feedback';
    private $uri = 'admin.feedback';
    private $title = 'FeedBack';
    private $desc = 'Description';

    // use CrudTrait;

    public function __construct(Message $table)
    {
        $this->middleware('permission:list_feedback', ['only' => ['index','data']]);
        $this->middleware('permission:reply_feedback', ['only' => ['reply','store']]);
        // $this->middleware('permission:edit_feedback', ['only' => ['edit','update']]);
        $this->middleware('permission:delete_feedback', ['only' => ['destroy','postDeleteAll']]);
        $this->table = $table;
        $this->url = route($this->uri.'.index');
    }

    public function index()
    {
        $data['title'] = $this->title;
        // $data['subTitle'] = 'edit';
        $data['url'] = $this->url;

        $data['ajax'] = route($this->uri.'.data');

        return view($this->folder.'.index',$data);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $index = $this->table->select(['id', 'bp_user_id', 'title', 'content', 'is_read', 'created_at'])
            ->orderBy('id', 'desc');
            return Datatables::of($index)
            ->editColumn('bp_user_id', function ($index) {
                return isset($index->bpUser->full_name) ? $index->bpUser->full_name : '-';
            })
            ->editColumn('content', function ($index) {
                return str_limit($index->content, 10);
            })
            ->addColumn('replied', function ($index) {
                $msg = MessageReply::orderBy('id', 'desc')
                        ->where('bp_message_id', $index->id)
                        ->where('is_admin', 0)
                        ->where('is_read', 0)
                        // ->take(1)
                        ->count();
                return '<span class="badge bg-orange">'.$msg.'</span>';
            })
            ->editColumn('is_read', function ($index) {
                if ($index->is_read == 0) {
                    return '<span class="badge bg-orange">NEW</span>';
                } elseif ($index->is_read == 1) {
                    return '<span class="badge bg-teal">READ</span>';
                }
            })
            ->editColumn('created_at', function ($index) {
                return ($index->created_at) ? Carbon::parse($index->created_at)->format('d F Y, H:i:s') : '-';
            })
            ->addColumn('action', function ($index) {
                $tag = Form::open(array("url" => route($this->uri.'.destroy', $index->id), "method" => "DELETE", "class"=>"form"));
                // $tag .= (auth()->user()->can('edit_feedback')) ? "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'>Edit</a>" : '';
                $tag .= " <a href=".route($this->uri.'.reply', $index->id)." class='btn btn-success btn-xs'>Reply</a>";
                $tag .= (auth()->user()->can('delete_feedback')) ? " <button type='submit' class='delete btn btn-danger btn-xs'>Delete</button>" : '';
                $tag .= Form::close();
                return $tag;
            })
            ->rawColumns(['replied', 'is_read', 'action'])
            ->make(true);
        }
    }

    public function reply($id)
    {
        $data['method'] = 'POST';
        $data['action'] = route($this->uri.'.store');

        $data['title'] = $this->title;

        $data['subTitle'] = 'reply';

        $data['url'] = $this->url;
        $data['message'] = Message::find($id);
        $data['reply'] = MessageReply::orderBy('id','asc')->where('bp_message_id', $id)->get();
        Message::find($id)->update(['is_read' => 1]);
        MessageReply::where('bp_message_id', $id)->where('is_admin',0)->where('is_read', 0)->update(['is_read' => 1]);
        return view($this->folder.'.reply', $data);
    }

    public function store(Request $request)
    {
        Activity::log('create', $this->title);
        MessageReply::create([
            'bp_message_id' => $request->msg_id,
            'user_id' => Auth::id(),
            'content' => $request->content,
            'is_admin' => 1,
            'is_read' => 0,
            'created_by' => Auth::user()->name
        ]);

        return redirect()->back()->with('success',trans('message.create'));
        // return redirect(route($this->uri.'.index'))->with('success',trans('message.create'));
    }

    public function destroy($id)
    {
        Activity::log('delete', $this->title);
        $tb = $this->table->findOrFail($id);
        MessageReply::where('bp_message_id', $id)->delete();
        $tb->delete();
        return response()->json(['msg' => true,'success' => trans('message.delete')]);
    }
}
