<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Kris\LaravelFormBuilder\FormBuilder;
use App\Traits\CrudTrait;
use App\Models\TopUpStatus;

use Datatables;
use Form;

class TopupStatusController extends Controller
{
    private $folder = 'admin.topup_status';
    private $uri = 'admin.topUpStatus';
    private $title = 'Topup Status';
    private $desc = 'Description';

    use CrudTrait;

    public function __construct(TopUpStatus $table)
    {
        $this->middleware('permission:list_topupstatus', ['only' => ['index','data']]);
        $this->middleware('permission:create_topupstatus', ['only' => ['create','store']]);
        $this->middleware('permission:edit_topupstatus', ['only' => ['edit','update']]);
        $this->middleware('permission:delete_topupstatus', ['only' => ['destroy','postDeleteAll']]);
        $this->table = $table;
        $this->url = route($this->uri.'.index');
    }

    public function index()
    {
        $data['title'] = $this->title;
        $data['subTitle'] = 'edit';
        $data['url'] = $this->url;

        $data['ajax'] = route($this->uri.'.data');
        $data['create'] = route($this->uri.'.create');

        return view($this->folder.'.index',$data);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $index = $this->table->select(['id', 'topup_status', 'is_default'])
            ->orderBy('id', 'desc');
            return Datatables::of($index)
            ->editColumn('is_default', function ($index) {
                if($index->is_default) {
                    return '<span class="badge bg-teal">YES</span>';
                } else {
                    return '<span class="badge bg-pink">NO</span>';
                }
            })
            ->addColumn('action', function ($index) {
                $tag = Form::open(array("url" => route($this->uri.'.destroy',$index->id), "method" => "DELETE", "class"=>"form"));
                $tag .= (auth()->user()->can('edit_topupstatus')) ? "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'>Edit</a>" : '';
                // $tag .= " <a href=".route($this->uri.'.show',$index->id)." class='btn btn-success btn-xs'>Show</a>";
                $tag .= (auth()->user()->can('delete_topupstatus')) ? " <button type='submit' class='delete btn btn-danger btn-xs'>Delete</button>" : '';
                $tag .= Form::close();
                return $tag;
            })
            ->rawColumns(['is_default','action'])
            ->make(true);
        }
    }

    public function create(FormBuilder $formBuilder)
    {
        $data['form'] = $formBuilder->create('App\Forms\TopupStatusForm', [
            'method' => 'POST',
            'url' => route($this->uri.'.store')
        ]);
        $data['title'] = $this->title;
        $data['subTitle'] = 'create';

        $data['url'] = $this->url;

        return view($this->folder.'.create', $data);
    }

    public function edit($id, FormBuilder $formBuilder)
    {
        $users = $this->table->findOrFail($id);
        $data['form'] = $formBuilder->create('App\Forms\TopupStatusForm', [
            'method' => 'PUT',
            'model' => $users,
            'url' => route($this->uri.'.update', $id)
        ]);
        $data['title'] = $this->title;
        $data['subTitle'] = 'edit';

        $data['url'] = $this->url;

        return view($this->folder.'.create', $data);
    }
}
