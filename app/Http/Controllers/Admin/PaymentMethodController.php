<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Kris\LaravelFormBuilder\FormBuilder;
use Illuminate\Support\Facades\Auth;
use App\Traits\CrudTrait;
use App\Models\BankAccount;
use App\Models\PaymentMethod;
use App\Helpers\Activity;

use Datatables;
use Form, File, Storage;

class PaymentMethodController extends Controller
{
    private $folder = 'admin.paymentMethod';
    private $uri = 'admin.paymentMethod';
    private $title = 'Akun Bank';
    private $desc = 'Description';

    use CrudTrait;

    public function __construct(PaymentMethod $table, BankAccount $bank)
    {
        $this->middleware('permission:list_payment_method', ['only' => ['index','data']]);
        $this->middleware('permission:create_payment_method', ['only' => ['create','store']]);
        $this->middleware('permission:edit_payment_method', ['only' => ['edit','update']]);
        $this->middleware('permission:delete_payment_method', ['only' => ['destroy','postDeleteAll']]);
        $this->table = $table;
        $this->bank = $bank;
        $this->url = route($this->uri.'.index');
    }

    public function index()
    {
        $data['title'] = $this->title;
        $data['subTitle'] = 'edit';
        $data['url'] = $this->url;

        $data['ajax'] = route($this->uri.'.data');
        $data['create'] = route($this->uri.'.create');

        return view($this->folder.'.index',$data);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $index = $this->table->select(['id', 'title', 'description'])
            ->orderBy('id', 'desc');
            return Datatables::of($index)
            ->addColumn('action', function ($index) {
                $tag = Form::open(array("url" => route($this->uri.'.destroy',$index->id), "method" => "DELETE", "class"=>"form"));
                $tag .= (auth()->user()->can('edit_payment_method')) ? "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'>Edit</a>" : '';
                // $tag .= " <a href=".route($this->uri.'.show',$index->id)." class='btn btn-success btn-xs'>Show</a>";
                $tag .= (auth()->user()->can('delete_payment_method')) ? " <button type='submit' class='delete btn btn-danger btn-xs'>Delete</button>" : '';
                $tag .= Form::close();
                return $tag;
            })
            ->rawColumns(['action'])
            ->make(true);
        }
    }

    public function create(FormBuilder $formBuilder)
    {
        // $data['form'] = $formBuilder->create('App\Forms\BankAccountForm', [
        //     'method' => 'POST',
        //     'url' => route($this->uri.'.store')
        // ]);
        $data['method'] = 'POST';
        $data['action'] = route($this->uri.'.store');
        $data['title'] = $this->title;
        $data['subTitle'] = 'create';

        $data['bankForm'] = $formBuilder->create('App\Forms\BankForm');
        $data['methodForm'] = $formBuilder->create('App\Forms\PaymentMethodForm');
        $data['url'] = $this->url;

        return view($this->folder.'.create', $data);
    }

    public function store(Request $request)
    {
        Activity::log('create', $this->title);
        if (count($request->title)) {
            foreach ($request->title as $key => $value) {
                PaymentMethod::create([
                    'user_id' => Auth::id(),
                    'bank_id' => $request->bank_id,
                    'title' => $value,
                    'description' => $request->description[$key],
                ]);
            }
        }

        return redirect(route($this->uri.'.index'))->with('success',trans('message.create'));
    }

    public function edit($id, FormBuilder $formBuilder)
    {
        $data['index'] = $this->table->findOrFail($id);

        $data['title'] = $this->title;
        $data['subTitle'] = 'edit';
        $data['method'] = 'PUT';
        $data['action'] = route($this->uri.'.update', $id);

        $data['url'] = $this->url;
        $data['payment'] = PaymentMethod::where('bank_id', $id)->get();

        return view($this->folder.'.create', $data);
    }

    public function update(Request $request, $id)
    {
        Activity::log('edit', $this->title);
        $val = $request->file_foto;
        if(isset($val)) {
            $directory = base_path('public/logobank');
            $name      = rand(111,9999999).'.'.$val->extension();
            if(!File::isDirectory($directory)){
                File::makeDirectory($directory, 0777);    
            }
            $val->move($directory, $name);

            $item = $this->table->findOrFail($id);
            if(!empty($item->logo)){
                if(File::isDirectory($directory)){
                    Storage::delete('logobank/'.$item->logo);
                }
            }
            $request->merge(['logo' => $name]);

        }else{
            unset($request['logo']);
        }
        
        $this->table->findOrFail($id)->update($request->all());

        return redirect(route($this->uri.'.index'))->with('success', trans('message.update'));
    }
}
