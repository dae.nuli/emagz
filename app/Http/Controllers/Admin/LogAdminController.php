<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Models\LogAdmin;
use App\Traits\CrudTrait;
use Datatables;
use Form;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class LogAdminController extends Controller
{
    private $folder = 'admin.log';
    private $uri = 'admin.log';
    private $title = 'Log Admin';
    private $desc = 'Description';

    use CrudTrait;
    
    public function __construct(LogAdmin $table)
    {
        $this->middleware('permission:list_log', ['only' => ['index','data']]);
        // $this->middleware('permission:create_log', ['only' => ['create','store']]);
        // $this->middleware('permission:edit_log', ['only' => ['edit','update']]);
        $this->middleware('permission:delete_log', ['only' => ['destroy','postDeleteAll']]);
        $this->table = $table;
        $this->url = route($this->uri.'.index');
    }

    public function index()
    {
        $data['title'] = $this->title;
        // $data['subTitle'] = 'edit';
        $data['url'] = $this->url;

        $data['ajax'] = route($this->uri.'.data');
        $data['create'] = route($this->uri.'.create');

        return view($this->folder.'.index',$data);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $index = $this->table->select(['id', 'user_id', 'activity', 'created_at'])
            ->orderBy('id', 'desc');
            return Datatables::of($index)
            ->editColumn('user_id', function ($index) {
                return isset($index->user->name) ? $index->user->name : '-';
            })
            ->editColumn('created_at', function ($index) {
                return Carbon::parse($index->created_at)->format('d F Y, H:i:s');
            })
            ->addColumn('action', function ($index) {
                $tag = Form::open(array("url" => route($this->uri.'.destroy',$index->id), "method" => "DELETE", "class"=>"form"));
                // $tag .= (auth()->user()->can('edit_log')) ? "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'>Edit</a>" : '';
                // $tag .= " <a href=".route($this->uri.'.show',$index->id)." class='btn btn-success btn-xs'>Show</a>";
                $tag .= (auth()->user()->can('delete_log')) ? " <button type='submit' class='delete btn btn-danger btn-xs'>Delete</button>" : '';
                $tag .= Form::close();
                return $tag;
            })
            ->rawColumns(['action'])
            ->make(true);
        }
    }
}
