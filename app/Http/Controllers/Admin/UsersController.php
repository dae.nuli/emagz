<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Kris\LaravelFormBuilder\FormBuilder;
use App\Models\UserRole;
use App\Models\Role;
use App\User;
use App\Helpers\Activity;

use Datatables;
use Form, File, Storage;

class UsersController extends Controller
{
    private $folder = 'admin.users';
    private $uri = 'admin.users';
    private $title = 'Admin';
    private $desc = 'Description';

    public function __construct(User $table)
    {
        $this->middleware('permission:list_user', ['only' => ['index','data']]);
        $this->middleware('permission:create_user', ['only' => ['create','store']]);
        $this->middleware('permission:edit_user', ['only' => ['edit','update']]);
        $this->middleware('permission:delete_user', ['only' => ['destroy','postDeleteAll']]);
        $this->table = $table;
        $this->url = route($this->uri.'.index');
    }

    public function index()
    {
        $data['title'] = $this->title;
        $data['subTitle'] = 'edit';
        $data['url'] = $this->url;

        $data['ajax'] = route($this->uri.'.data');
        $data['create'] = route($this->uri.'.create');

        return view($this->folder.'.index',$data);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $index = $this->table->select(['id', 'name', 'photo','email'])
            ->orderBy('id', 'desc');
            return Datatables::of($index)
            ->editColumn('photo', function ($index) {
                return ($index->photo) ? "<img src='".asset('users/'.$index->photo)."' width='120' alt='img'/>" : '-';
            })
            ->addColumn('role', function ($index) {
                return '<b>'.$index->checkRole($index->id).'</b>';
            })
            ->addColumn('action', function ($index) {
                if ($index->id != 1) {
                    $tag = Form::open(array("url" => route($this->uri.'.destroy',$index->id), "method" => "DELETE", "class"=>"form"));
                    $tag .= (auth()->user()->can('edit_user')) ? "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'>Edit</a>" : '';
                    // $tag .= " <a href=".route($this->uri.'.show',$index->id)." class='btn btn-success btn-xs'>Show</a>";
                    $tag .= (auth()->user()->can('delete_user')) ? " <button type='submit' class='delete btn btn-danger btn-xs'>Delete</button>" : '';
                    $tag .= Form::close();
                return $tag;
                } else {
                    return '-';
                }
            })
            ->rawColumns(['photo','role','action'])
            ->make(true);
        }
    }

    public function create(FormBuilder $formBuilder)
    {
        $data['form'] = $formBuilder->create('App\Forms\UsersForm', [
            'method' => 'POST',
            'url' => route($this->uri.'.store')
        ]);
        $data['title'] = $this->title;
        $data['subTitle'] = 'create';

        $data['url'] = $this->url;

        return view($this->folder.'.create', $data);
    }

    public function edit($id, FormBuilder $formBuilder)
    {
        $users = $this->table->findOrFail($id);
        $role = UserRole::where('user_id', $id)->first();
        $data['form'] = $formBuilder->create('App\Forms\UsersForm', [
            'method' => 'PUT',
            'model' => $users,
            'url' => route($this->uri.'.update', $id)
        ])->modify('password', 'password', [
            'attr' => ['required' => null],
            'value' => ''
        ])->modify('role', 'select', [
            'choices' => Role::pluck('role', 'id')->toArray(),
            'empty_value' => '- Please Select -',
            'selected' => isset($role->cfg_role_id) ? $role->cfg_role_id : '',
            'label' => 'Role',
            'attr' => ['required' => '', 'class' => 'form-control show-tick']
        ]);
        $data['title'] = $this->title;
        $data['subTitle'] = 'edit';

        $data['url'] = $this->url;

        return view($this->folder.'.create', $data);
    }

    public function store(Request $request)
    {
        Activity::log('create', $this->title);
        $val = $request->file_foto;
        if(isset($val)) {
            $directory = base_path('public/users');
            $name      = str_random(10).'.'.$val->extension();
            if(!File::isDirectory($directory)){
                File::makeDirectory($directory, 0777);    
            }
            $val->move($directory, $name);
            
            $request->merge(['photo' => $name]);
        }else{
            unset($request['photo']);
        }

        $request->merge([
            'password' => bcrypt($request->password)
        ]);

        $tb = $this->table->create($request->all());

        // UserRole::create(['bp_user_id' => $tb->id, 'cfg_role_id' => $request->role]);
        $ur = new UserRole;
        $ur->user_id = $tb->id;
        $ur->cfg_role_id = $request->role;
        $ur->save();

        return redirect(route($this->uri.'.index'))->with('success',trans('message.create'));
    }

    public function update(Request $request, $id)
    {
        Activity::log('edit', $this->title);
        $val = $request->file_foto;
        if(isset($val)) {
            $directory = base_path('public/users');
            $name      = rand(111,9999999).'.'.$val->extension();
            if(!File::isDirectory($directory)){
                File::makeDirectory($directory, 0777);    
            }
            $val->move($directory, $name);

            $item = $this->table->findOrFail($id);
            if(!empty($item->photo)){
                if(File::isDirectory($directory)){
                    Storage::delete('users/'.$item->photo);
                }
            }
            $request->merge(['photo' => $name]);

        }else{
            unset($request['photo']);
        }

        if(empty($request->password)){
            unset($request['password']);
        } else {
            $request->merge(['password' => bcrypt($request->password)]);
        }

        $this->table->findOrFail($id)->update($request->all());

        UserRole::where('user_id', $id)->update(['cfg_role_id' => $request->role]);
        
        return redirect(route($this->uri.'.index'))->with('success', trans('message.update'));
    }
    
    public function destroy($id)
    {
        Activity::log('delete', $this->title);
        $tb = $this->table->findOrFail($id);
        if ($tb->id != 1) {
	        if (!empty($tb->photo)) {
	            Storage::delete('users/'.$tb->photo);
	        }
	        $tb->delete();
	        return response()->json(['msg' => true,'success' => trans('message.delete')]);
        }
        // return redirect(route($this->uri.'.index'))->with('success',trans('message.delete'));
    }
}
