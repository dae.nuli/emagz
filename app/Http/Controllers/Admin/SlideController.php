<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Kris\LaravelFormBuilder\FormBuilder;
// use App\Traits\CrudTrait;
use Carbon\Carbon;

use App\Models\Slide;
use App\Helpers\Activity;

use Datatables;
use Form, File, Storage;

class SlideController extends Controller
{
    private $folder = 'admin.slide';
    private $uri = 'admin.slide';
    private $title = 'Marketing';
    private $desc = 'Description';

    // use CrudTrait;

    public function __construct(Slide $table)
    {
        $this->middleware('permission:list_slide', ['only' => ['index','data']]);
        $this->middleware('permission:create_slide', ['only' => ['create','store']]);
        $this->middleware('permission:edit_slide', ['only' => ['edit','update']]);
        $this->middleware('permission:delete_slide', ['only' => ['destroy','postDeleteAll']]);
        $this->table = $table;
        $this->url = route($this->uri.'.index');
    }

    public function index()
    {
        $data['title'] = $this->title;
        // $data['subTitle'] = 'edit';
        $data['url'] = $this->url;

        $data['ajax'] = route($this->uri.'.data');
        $data['create'] = route($this->uri.'.create');

        return view($this->folder.'.index',$data);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $index = $this->table->select(['id', 'title', 'description', 'filename', 'is_active'])
            ->orderBy('id', 'desc');
            return Datatables::of($index)
            ->editColumn('is_active', function ($index) {
                if($index->is_active == 1) {
                    return '<span class="badge bg-teal">AKTIF</span>';
                } else {
                    return '<span class="badge bg-orange">TIDAK AKTIF</span>';
                }
            })
            ->editColumn('description', function ($index) {
                return str_limit($index->description, 10);
            })
            ->editColumn('filename', function ($index) {
                return ($index->filename) ? "<img src='".asset('slidemarketing/'.$index->filename)."' width='120' alt='img'/>" : '-';
            })
            ->addColumn('action', function ($index) {
                $tag = Form::open(array("url" => route($this->uri.'.destroy',$index->id), "method" => "DELETE", "class"=>"form"));
                $tag .= (auth()->user()->can('edit_slide')) ? "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'>Edit</a>" : '';
                // $tag .= (auth()->user()->can('detail_slide')) ? " <a href=".route($this->uri.'.show', $index->id)." class='btn btn-success btn-xs'>Check</a>" : '';
                $tag .= (auth()->user()->can('delete_slide')) ? " <button type='submit' class='delete btn btn-danger btn-xs'>Delete</button>" : '';
                $tag .= Form::close();
                return $tag;
            })
            ->rawColumns(['is_active','filename','action'])
            ->make(true);
        }
    }

    public function create(FormBuilder $formBuilder)
    {
        $data['form'] = $formBuilder->create('App\Forms\SlideForm', [
            'method' => 'POST',
            'url' => route($this->uri.'.store')
        ]);
        $data['title'] = $this->title;
        $data['subTitle'] = 'create';

        $data['url'] = $this->url;

        return view($this->folder.'.create', $data);
    }

    public function store(Request $request)
    {
        Activity::log('create', $this->title);
        $val = $request->attachment;
        if(isset($val)) {
            $directory = base_path('public/slidemarketing');
            $name      = str_random(10).'.'.$val->extension();
            if(!File::isDirectory($directory)){
                File::makeDirectory($directory, 0777);    
            }
            $val->move($directory, $name);
            
            $request->merge(['filename' => $name]);
        }else{
            unset($request['filename']);
        }

        $tb = $this->table->create($request->all());

        return redirect(route($this->uri.'.index'))->with('success',trans('message.create'));
    }

    public function edit($id, FormBuilder $formBuilder)
    {
        $users = $this->table->findOrFail($id);
        $data['form'] = $formBuilder->create('App\Forms\SlideForm', [
            'method' => 'PUT',
            'model' => $users,
            'url' => route($this->uri.'.update', $id)
        ])
        ->modify('filename', 'static', [
            'template' => 'admin.slide.edit_file',
            // 'value' => function ($val) {
            //     return $val;
            // }
        ])
        ->modify('is_active', 'choice', [
            // 'label' => 'Status',
            // 'attr' => ['required' => ''],
            // 'choices' => [1 => 'YES', 0 => 'NO'],
            // 'choice_options' => [
            //     'wrapper' => ['class' => 'radio'],
            //     'label_attr' => ['class' => 'col-lg-10 col-md-10 col-sm-8 col-xs-7'],
            // ],
            'selected' => null,
            // 'expanded' => true,
            // 'multiple' => false
        ]);
        $data['title'] = $this->title;
        $data['subTitle'] = 'edit';

        $data['url'] = $this->url;

        return view($this->folder.'.create', $data);
    }

    public function update(Request $request, $id)
    {
        Activity::log('edit', $this->title);
        $val = $request->attachment;
        if(isset($val)) {
            $directory = base_path('public/slidemarketing');
            $name      = str_random(10).'.'.$val->extension();
            if(!File::isDirectory($directory)){
                File::makeDirectory($directory, 0777);    
            }
            $val->move($directory, $name);

            $item = $this->table->findOrFail($id);
            if(!empty($item->filename)){
                if(File::isDirectory($directory)){
                    Storage::delete('slidemarketing/'.$item->filename);
                }
            }
            $request->merge(['filename' => $name]);

        }else{
            unset($request['filename']);
        }
        
        $this->table->findOrFail($id)->update($request->all());
        
        return redirect(route($this->uri.'.index'))->with('success', trans('message.update'));
    }

    public function destroy($id)
    {
        Activity::log('delete', $this->title);
        $tb = $this->table->findOrFail($id);
        if (!empty($tb->filename)) {
            Storage::delete('slidemarketing/'.$tb->filename);
        }
        $tb->delete();
        return response()->json(['msg' => true,'success' => trans('message.delete')]);
        // return redirect(route($this->uri.'.index'))->with('success',trans('message.delete'));
    }
}
