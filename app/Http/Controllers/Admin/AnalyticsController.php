<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Traits\CrudTrait;
use App\Models\Buletin;
use App\Models\Order;
use App\Models\News;
use App\Models\Comment;
use App\Models\Rating;
use Carbon\Carbon;
use Datatables;
use Form;
use DB;

class AnalyticsController extends Controller
{
    private $folder = 'admin.analytics';
    private $uri = 'admin.analytics';
    private $title = 'Analytics';
    private $desc = 'Description';

    use CrudTrait;

    public function __construct(Buletin $buletin, News $news, Order $order)
    {
        $this->middleware('permission:list_analytics', ['only' => ['index','data']]);
        $this->buletin = $buletin;
        $this->order = $order;
        $this->news = $news;
        $this->url = route($this->uri.'.index');
    }

    public function index()
    {
        $data['title'] = $this->title;
        $data['url'] = $this->url;

        $data['buletin'] = route($this->uri.'.data.buletin');
        $data['news'] = route($this->uri.'.data.news');

        return view($this->folder.'.index',$data);
    }

    public function dataBuletin(Request $request)
    {
        if ($request->ajax()) {
            // $index = $this->buletin->select(['id', 'buletin_title', 'edition'])
            // ->orderBy('id', 'desc');
            $index = $this->order->select('bp_buletin_id', DB::raw('COUNT(bp_buletin_id) as total_buletin'))
                                ->groupBy('bp_buletin_id')
                                // ->orderBy('total_buletin', 'desc')
                                ->get();
            return Datatables::of($index)
            ->addColumn('buletin_title', function ($index) {
                return $index->buletin->buletin_title;
            })
            ->addColumn('edition', function ($index) {
                return $index->buletin->edition;
            })
            ->addColumn('article', function ($index) {
                return $index->getArticle($index->bp_buletin_id);
                // return $index->buletin->bestArticle;
            })
            ->addColumn('rating', function ($index) {
                // $rating = Rating::whereHas('article', function ($query) use ($index) {
                //             $query->where('bp_buletin_id', $index->bp_buletin_id);
                //         })->pluck('bp_article_id','rating');
                // return $rating;
                return $index->getRate($index->bp_buletin_id);
            })
            ->addColumn('sold', function ($index) {
                return $index->total_buletin;
            })
            ->addColumn('testimoni', function ($index) {
                $comment = Comment::whereHas('article', function ($query) use ($index) {
                                $query->where('bp_buletin_id', $index->bp_buletin_id);
                            })->count();
                return $comment;
            })
            // ->addColumn('action', function ($index) {
            //     // $tag = Form::open(array("url" => route($this->uri.'.destroy',$index->id), "method" => "DELETE", "class"=>"form"));
            //     $tag = (auth()->user()->can('edit_analytics')) ? "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'>Edit</a>" : '';
            //     // $tag .= (auth()->user()->can('delete_analytics')) ? " <button type='submit' class='delete btn btn-danger btn-xs'>Delete</button>" : '';
            //     // $tag .= Form::close();
            //     return $tag;
            // })
            // ->rawColumns(['action'])
            ->make(true);
        }
    }

    public function dataNews(Request $request)
    {
        if ($request->ajax()) {
            $index = $this->news->select(['id', 'title', 'date_published'])
            ->orderBy('id', 'desc');
            return Datatables::of($index)
            ->addColumn('read', function ($index) {
                return '-';
            })
            ->editColumn('date_published', function ($index) {
                return ($index->date_published) ? Carbon::parse($index->date_published)->format('d F Y, H:i:s') : '-';
            })
            ->addColumn('testimoni', function ($index) {
                return '-';
            })
            ->addColumn('rating', function ($index) {
                return '-';
            })
            ->make(true);
        }
    }
}
