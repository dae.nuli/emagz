<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Kris\LaravelFormBuilder\FormBuilder;
use App\Traits\CrudTrait;
use App\Models\BankAccount;
use App\Models\PaymentMethod;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Activity;


use Datatables;
use Form, File, Storage;

class BankAccountController extends Controller
{
    private $folder = 'admin.bank';
    private $uri = 'admin.bank';
    private $title = 'Akun Bank';
    private $desc = 'Description';

    use CrudTrait;

    public function __construct(BankAccount $table)
    {
        $this->middleware('permission:list_bank', ['only' => ['index','data']]);
        $this->middleware('permission:create_bank', ['only' => ['create','store']]);
        $this->middleware('permission:edit_bank', ['only' => ['edit','update']]);
        $this->middleware('permission:delete_bank', ['only' => ['destroy','postDeleteAll']]);
        $this->table = $table;
        $this->url = route($this->uri.'.index');
    }

    public function index()
    {
        $data['title'] = $this->title;
        $data['subTitle'] = 'edit';
        $data['url'] = $this->url;

        $data['ajax'] = route($this->uri.'.data');
        $data['create'] = route($this->uri.'.create');

        return view($this->folder.'.index',$data);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $index = $this->table->select(['id', 'bank_name', 'account_name', 'logo', 'description', 'account_no', 'is_active'])
            ->orderBy('id', 'desc');
            return Datatables::of($index)
            ->editColumn('logo', function ($index) {
                return ($index->logo) ? "<img src='".asset('logobank/'.$index->logo)."' width='120' alt='img'/>" : '-';
            })
            ->editColumn('is_active', function ($index) {
                if($index->is_active) {
                    return '<span class="badge bg-teal">AKTIF</span>';
                } else {
                    return '<span class="badge bg-pink">TIDAK AKTIF</span>';
                }
            })
            ->addColumn('action', function ($index) {
                $tag = Form::open(array("url" => route($this->uri.'.destroy',$index->id), "method" => "DELETE", "class"=>"form"));
                $tag .= (auth()->user()->can('edit_bank')) ? "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'>Edit</a>" : '';
                // $tag .= " <a href=".route($this->uri.'.show',$index->id)." class='btn btn-success btn-xs'>Show</a>";
                $tag .= (auth()->user()->can('delete_bank')) ? " <button type='submit' class='delete btn btn-danger btn-xs'>Delete</button>" : '';
                $tag .= Form::close();
                return $tag;
            })
            ->rawColumns(['is_active','logo','action'])
            ->make(true);
        }
    }

    public function create(FormBuilder $formBuilder)
    {
        // $data['form'] = $formBuilder->create('App\Forms\BankAccountForm', [
        //     'method' => 'POST',
        //     'url' => route($this->uri.'.store')
        // ]);
        // $data['title'] = $this->title;
        // $data['subTitle'] = 'create';

        // $data['url'] = $this->url;
        $data['method'] = 'POST';
        $data['action'] = route($this->uri.'.store');
        $data['title'] = $this->title;
        $data['subTitle'] = 'create';

        $data['bankForm'] = $formBuilder->create('App\Forms\BankAccountForm');
        $data['methodForm'] = $formBuilder->create('App\Forms\PaymentMethodForm');
        $data['url'] = $this->url;

        return view($this->folder.'.create', $data);
    }

    public function store(Request $request)
    {
        Activity::log('create', $this->title, $request->bank_name);
        $val = $request->file_logo;
        if(isset($val)) {
            $directory = base_path('public/logobank');
            $name      = str_random(10).'.'.$val->extension();
            if(!File::isDirectory($directory)){
                File::makeDirectory($directory, 0777);    
            }
            $val->move($directory, $name);
            
            $request->merge(['logo' => $name]);
        }else{
            unset($request['logo']);
        }

        $tb = $this->table->create($request->all());
        
        if (count($request->title_method)) {
            foreach ($request->title_method as $key => $value) {
                PaymentMethod::create([
                    'user_id' => Auth::id(),
                    'bank_id' => $tb->id,
                    'title' => $value,
                    'description' => $request->description_method[$key],
                ]);
            }
        }

        return redirect(route($this->uri.'.index'))->with('success',trans('message.create'));
    }

    public function edit($id, FormBuilder $formBuilder)
    {
        $data['index'] = $this->table->findOrFail($id);

        $data['title'] = $this->title;
        $data['subTitle'] = 'edit';
        $data['method'] = 'PUT';
        $data['action'] = route($this->uri.'.update', $id);

        $data['url'] = $this->url;
        $data['methods'] = PaymentMethod::where('bank_id', $id)->get();

        // $users = $this->table->findOrFail($id);
        // $data['form'] = $formBuilder->create('App\Forms\BankAccountForm', [
        //     'method' => 'PUT',
        //     'model' => $users,
        //     'url' => route($this->uri.'.update', $id)
        // ]);
        // $data['title'] = $this->title;
        // $data['subTitle'] = 'edit';

        // $data['url'] = $this->url;

        return view($this->folder.'.edit', $data);
    }

    public function update(Request $request, $id)
    {
        Activity::log('edit', $this->title, $request->bank_name);
        // Activity::log('edit', $this->title);
        $val = $request->file_foto;
        if(isset($val)) {
            $directory = base_path('public/logobank');
            $name      = rand(111,9999999).'.'.$val->extension();
            if(!File::isDirectory($directory)){
                File::makeDirectory($directory, 0777);    
            }
            $val->move($directory, $name);

            $item = $this->table->findOrFail($id);
            if(!empty($item->logo)){
                if(File::isDirectory($directory)){
                    Storage::delete('logobank/'.$item->logo);
                }
            }
            $request->merge(['logo' => $name]);

        }else{
            unset($request['logo']);
        }
        
        $this->table->findOrFail($id)->update($request->all());
        
        if (count($request->title_method)) {
            foreach ($request->title_method as $key => $value) {
                if (isset($request->method_id[$key])) {
                    $pm = PaymentMethod::find($request->method_id[$key]);
                    $pm->title = $value;
                    $pm->description = $request->description_method[$key];
                    $pm->save();
                } else {
                    PaymentMethod::create([
                        'user_id' => Auth::id(),
                        'bank_id' => $id,
                        'title' => $value,
                        'description' => $request->description_method[$key],
                    ]);
                }
            }
        }
        return redirect(route($this->uri.'.index'))->with('success', trans('message.update'));
    }
    
    public function destroy($id)
    {
        // Activity::log('delete', $this->title);
        Activity::log('delete', $this->title, $tb->bank_name);
        $tb = $this->table->findOrFail($id);
        $tb->delete();
        return response()->json(['msg' => true,'success' => trans('message.delete')]);
    }

    public function deleteItem($id)
    {
        // Activity::log('delete', $this->title);
        $pm = PaymentMethod::findOrFail($id);
        Activity::log('delete', $this->title, $pm->title);
        $pm->delete();
        return redirect()->back()->with('success', trans('message.delete'));
        // return response()->json(['msg' => true,'success' => trans('message.delete')]);
    }
}
