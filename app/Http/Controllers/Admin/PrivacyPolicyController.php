<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Traits\CrudTrait;
use App\Models\PrivacyPolicy;
use Datatables;
use Form;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

use App\Helpers\Activity;
use App\Models\PushNotif;

use LaravelFCM\Message\Topics;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class PrivacyPolicyController extends Controller
{
    private $folder = 'admin.privacy_policy';
    private $uri = 'admin.privacy';
    private $title = 'Privacy Policy';
    private $desc = 'Description';

    use CrudTrait;

    public function __construct(PrivacyPolicy $table)
    {
        $this->middleware('permission:list_privacy', ['only' => ['index','data']]);
        $this->middleware('permission:create_privacy', ['only' => ['create','store']]);
        $this->middleware('permission:edit_privacy', ['only' => ['edit','update']]);
        $this->middleware('permission:delete_privacy', ['only' => ['destroy','postDeleteAll']]);
        $this->table = $table;
        $this->url = route($this->uri.'.index');
    }

    public function index()
    {
        $data['title'] = $this->title;
        // $data['subTitle'] = 'edit';
        $data['url'] = $this->url;

        $data['ajax'] = route($this->uri.'.data');
        $data['create'] = route($this->uri.'.create');

        return view($this->folder.'.index',$data);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $index = $this->table->select(['id', 'author', 'status'])
            ->orderBy('id', 'desc');
            return Datatables::of($index)
            ->editColumn('status', function ($index) {
                return ($index->status) ? 'Release' : 'Expired';
            })
            // ->editColumn('date_published', function ($index) {
            //     return ($index->date_published) ? Carbon::parse($index->date_published)->format('d F Y, H:i:s') : '-';
            // })
            ->addColumn('action', function ($index) {
                $tag = Form::open(array("url" => route($this->uri.'.destroy',$index->id), "method" => "DELETE", "class"=>"form"));
                $tag .= (auth()->user()->can('edit_privacy')) ? "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'>Edit</a>" : '';
                // $tag .= " <a href=".route($this->uri.'.show',$index->id)." class='btn btn-success btn-xs'>Show</a>";
                $tag .= (auth()->user()->can('delete_privacy')) ? " <button type='submit' class='delete btn btn-danger btn-xs'>Delete</button>" : '';
                $tag .= Form::close();
                return $tag;
            })
            ->rawColumns(['action'])
            ->make(true);
        }
    }

    public function create(FormBuilder $formBuilder)
    {
        $data['form'] = $formBuilder->create('App\Forms\PrivacyPolicyForm', [
            'method' => 'POST',
            'url' => route($this->uri.'.store')
        ])->modify('author', 'text', [
            'value' => function () {
                return Auth::user()->name;
            }
        ]);
        $data['title'] = $this->title;
        $data['subTitle'] = 'create';

        $data['url'] = $this->url;

        return view($this->folder.'.create', $data);
    }

    public function edit($id, FormBuilder $formBuilder)
    {
        $users = $this->table->findOrFail($id);
        $data['form'] = $formBuilder->create('App\Forms\PrivacyPolicyForm', [
            'method' => 'PUT',
            'model' => $users,
            'url' => route($this->uri.'.update', $id)
        ]);
        $data['title'] = $this->title;
        $data['subTitle'] = 'edit';

        $data['url'] = $this->url;

        return view($this->folder.'.create', $data);
    }

    public function store(Request $request)
    {
        Activity::log('create', $this->title);
        $this->table->create($request->all());

        if (!empty($request->status)) {
            
            $notificationBuilder = new PayloadNotificationBuilder('Privacy Policy');
            $notificationBuilder->setBody('Privacy Policy')
                            ->setSound('default');
                            
            $notification = $notificationBuilder->build();

            $topic = new Topics();
            $topic->topic('dev-pp');

            $topicResponse = FCM::sendToTopic($topic, null, $notification, null);

            PushNotif::create([
                'user_id' => Auth::id(),
                'title' => 'Privacy Policy',
                'content' => $request->content,
                'is_published' => 1,
                // 'is_published' => !empty($request->is_published) ? 1 : 0,
                'published_date' => Carbon::parse($request->created_at)->format('Y-m-d H:i:s'),
                'topic' => 'dev-toc',
                'feature_source' => 'privacypolicy'
            ]);
        }
        return redirect(route($this->uri.'.index'))->with('success',trans('message.create'));
    }
}
