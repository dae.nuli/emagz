<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Kris\LaravelFormBuilder\FormBuilder;
use App\Traits\CrudTrait;
use App\Models\TopUp;
use Carbon\Carbon;
use App\Helpers\Activity;

use App\Models\BpUser;
use App\Models\BankAccount;
use App\Models\TopUpStatus;
use App\Models\TopUpType;

use Datatables;
use Form, File, Storage;

class TopupController extends Controller
{
    private $folder = 'admin.topup';
    private $uri = 'admin.topUp';
    private $title = 'Topup';
    private $desc = 'Description';

    use CrudTrait;

    public function __construct(TopUp $table)
    {
        $this->middleware('permission:list_topup', ['only' => ['index','data']]);
        $this->middleware('permission:create_topup', ['only' => ['create','store']]);
        $this->middleware('permission:detail_topup', ['only' => ['show']]);
        $this->middleware('permission:edit_topup', ['only' => ['edit','update']]);
        $this->middleware('permission:delete_topup', ['only' => ['destroy','postDeleteAll']]);
        $this->table = $table;
        $this->url = route($this->uri.'.index');
    }

    public function index()
    {
        $data['title'] = $this->title;
        // $data['subTitle'] = 'edit';
        $data['url'] = $this->url;

        $data['ajax'] = route($this->uri.'.data');
        $data['create'] = route($this->uri.'.create');

        return view($this->folder.'.index',$data);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $index = $this->table->select(['id', 'bp_user_id', 'amount', 'topup_date', 'confirmation_date', 'status'])
            // $index = $this->table->select(['id', 'bp_user_id', 'amount', 'topup_date', 'cfg_topup_status_id', 'confirmation_date', 'status'])
            ->orderBy('id', 'desc');
            return Datatables::of($index)
            ->editColumn('bp_user_id', function ($index) {
                if($index->bp_user_id) {
                    // return 1;
                    return isset($index->users->full_name) ? $index->users->full_name : '-';
                } else {
                    return '-';
                }
            })
            ->editColumn('amount', function ($index) {
                if($index->amount) {
                    return 'Rp '.number_format($index->amount, 0, '', '.');
                } else {
                    return 0;
                }
            })
            ->editColumn('confirmation_date', function ($index) {
                return ($index->confirmation_date) ? Carbon::parse($index->confirmation_date)->format('d F Y, H:i:s') : '-';
            })
            ->editColumn('status', function ($index) {
                if($index->status == 2) {
                    return '<span class="badge bg-teal">ACCEPTED</span>';
                } elseif ($index->status == 0) {
                    return '<span class="badge bg-orange">NEW</span>';
                } elseif ($index->status == 1) {
                    return '<span class="badge bg-red">REJECTED</span>';
                }
            })
            ->addColumn('action', function ($index) {
                $tag = Form::open(array("url" => route($this->uri.'.destroy',$index->id), "method" => "DELETE", "class"=>"form"));
                $tag .= (auth()->user()->can('edit_topup')) ? "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'>Edit</a>" : '';
                $tag .= (auth()->user()->can('detail_topup')) ? " <a href=".route($this->uri.'.show', $index->id)." class='btn btn-success btn-xs'>Check</a>" : '';
                $tag .= (auth()->user()->can('delete_topup')) ? " <button type='submit' class='delete btn btn-danger btn-xs'>Delete</button>" : '';
                $tag .= Form::close();
                return $tag;
            })
            ->rawColumns(['status','action'])
            ->make(true);
        }
    }

    public function show($id, FormBuilder $formBuilder)
    {
        $users = $this->table->findOrFail($id);
        $data['users'] = $users;
        $data['form'] = $formBuilder->create('App\Forms\TopUpForm', [
            'method' => 'POST',
            'model' => $users,
            'url' => route($this->uri.'.confirmation', $id)
        ])
        ->modify('bp_user_id', 'select', [
            'attr' => [
                'disabled' => '',
            ]
        ])
        ->modify('bank_id', 'select', [
            'attr' => [
                'disabled' => '',
            ]
        ])
        ->modify('account_name', 'text', [
            'attr' => [
                'disabled' => '',
            ]
        ])
        ->modify('topup_code', 'text', [
            'attr' => [
                'disabled' => '',
            ]
        ])
        ->modify('cfg_topup_type_id', 'select', [
            'attr' => [
                'disabled' => '',
            ]
        ])
        // ->modify('cfg_topup_status_id', 'select', [
        //     'attr' => [
        //         'disabled' => '',
        //     ]
        // ])
        ->modify('amount', 'text', [
            'attr' => [
                'disabled' => '',
            ],
            'value' => function ($val) {
                return 'Rp '.number_format($val, 0, '', '.');
            }
        ])
        ->modify('topup_date', 'text', [
            'attr' => [
                'disabled' => '',
                'class' => 'datetimepicker form-control'
            ],
            'value' => function ($val) {
                return Carbon::parse($val)->format('Y/m/d H:i');
            }
        ])->modify('confirmation_date', 'text', [
            // 'label' => 'Tanggal',
            'attr' => [
                'disabled' => '',
                'class' => 'datetimepicker form-control'
            ],
            'value' => function ($val) {
                return !empty($val) ? Carbon::parse($val)->format('Y/m/d H:i') : '';
            }
        ])
        ->modify('file_attachment', 'static', [
            'template' => 'admin.topup.detail_attachment',
            'value' => function ($val) {
                return $val;
            }
        ]);
        $data['title'] = $this->title;
        $data['subTitle'] = 'Detail';

        $data['url'] = $this->url;

        return view($this->folder.'.detail',$data);
    }

    public function postConfirmation(Request $request, $id)
    {
        if ($request->status == 2) {
            $tp = TopUp::find($id);
            $tp->status = $request->status;
            $tp->confirmation_date = Carbon::now();
            $tp->save();

            $bpUser = BpUser::find($tp->bp_user_id);
            $bpUser->amount_balance = $bpUser->amount_balance + $tp->amount;
            $bpUser->save();
            Activity::log('topup_approved', $this->title, $bpUser->full_name.' - amount Rp '.number_format($tp->amount, 0, '', '.'));
            // Activity::log('topup_approved', $this->title, $bpUser->full_name);
        } elseif ($request->status == 1) {
            $tp = TopUp::find($id);
            $tp->status = 1;
            $tp->confirmation_date = Carbon::now();
            $tp->save();
            Activity::log('topup_rejected', $this->title, $tp->users->full_name.' - amount Rp '.number_format($tp->amount, 0, '', '.'));
            // Activity::log('topup_rejected', $this->title, $tp->users->full_name);
        }
        return redirect(route($this->uri.'.index'))->with('success',trans('message.confirmation'));
    }

    public function create(FormBuilder $formBuilder)
    {
        $data['form'] = $formBuilder->create('App\Forms\TopUpForm', [
            'method' => 'POST',
            'url' => route($this->uri.'.store')
        ])
        ->remove('status');
        $data['title'] = $this->title;
        $data['subTitle'] = 'create';

        $data['url'] = $this->url;

        return view($this->folder.'.create', $data);
    }

    public function store(Request $request)
    {
        $val = $request->attachment;
        if(isset($val)) {
            $directory = base_path('public/topupattachment');
            $name      = str_random(10).'.'.$val->extension();
            if(!File::isDirectory($directory)){
                File::makeDirectory($directory, 0777);    
            }
            $val->move($directory, $name);
            
            $request->merge(['file_attachment' => $name]);
        }else{
            unset($request['file_attachment']);
        }

        $tb = $this->table->create($request->all());
        $tbUser = $this->table->find($tb->id);
        Activity::log('create', $this->title, $tbUser->users->full_name.' - amount Rp '.number_format($request->amount, 0, '', '.'));

        return redirect(route($this->uri.'.index'))->with('success',trans('message.create'));
    }

    // public function edit($id, FormBuilder $formBuilder)
    // {
    //     $topUp = $this->table->findOrFail($id);
    //     // $data['index'] = $this->table->findOrFail($id);
    //     $data['index'] = $topUp;

    //     $data['title'] = $this->title;
    //     $data['subTitle'] = 'edit';
    //     $data['method'] = 'PUT';
    //     $data['action'] = route($this->uri.'.update', $id);

    //     $data['topUpForm'] = $formBuilder->create('App\Forms\TopUpForm');
    //     $data['model'] = $topUp;
    //     $data['url'] = $this->url;
    //     // $data['customers'] = BpUser::orderBy('id', 'asc')->get();
    //     // $data['bank'] = BankAccount::orderBy('id', 'asc')->get();
    //     // $data['topUpStatus'] = TopUpStatus::orderBy('id', 'asc')->get();
    //     // $data['topUpType'] = TopUpType::orderBy('id', 'asc')->get();

    //     return view($this->folder.'.edit', $data);
    // }
    public function edit($id, FormBuilder $formBuilder)
    {
        $users = $this->table->findOrFail($id);
        $data['form'] = $formBuilder->create('App\Forms\TopUpForm', [
            'method' => 'PUT',
            'model' => $users,
            'url' => route($this->uri.'.update', $id)
        ])->modify('topup_date', 'text', [
            // 'label' => 'Tanggal',
            'attr' => [
                'required' => '',
                'class' => 'datetimepicker form-control'
            ],
            'value' => function ($val) {
                return Carbon::parse($val)->format('Y/m/d H:i');
            }
        ])->modify('confirmation_date', 'text', [
            // 'label' => 'Tanggal',
            'attr' => [
                'disabled' => '',
                'required' => '',
                'class' => 'datetimepicker form-control'
            ],
            'value' => function ($val) {
                return !empty($val) ? Carbon::parse($val)->format('Y/m/d H:i') : '';
            }
        ])
        ->modify('file_attachment', 'static', [
        // ->addAfter('attachment', 'bayar', 'static', [
            'template' => 'admin.topup.attachment',
            // 'value' => function ($val) {
            //     return $val;
            // }
            // 'value' => $this->getFieldValues()
            // 'value' => function ($val) {
            //     // $tp = TopUp::find($id);
            //     return $val->topup_date;
            //     // return '<img id="blah" src="'.asset('topupattachment/'.$val).'" alt="" width="200" />';
            // }
            //     return !empty($val) ? ;
            // }
        ])
        ->remove('status');
        $data['title'] = $this->title;
        $data['subTitle'] = 'edit';

        $data['url'] = $this->url;

        return view($this->folder.'.create', $data);
    }

    public function update(Request $request, $id)
    {
        // Activity::log('edit', $this->title);
        $val = $request->attachment;
        if(isset($val)) {
            $directory = base_path('public/topupattachment');
            $name      = str_random(10).'.'.$val->extension();
            if(!File::isDirectory($directory)){
                File::makeDirectory($directory, 0777);    
            }
            $val->move($directory, $name);

            $item = $this->table->findOrFail($id);
            if(!empty($item->file_attachment)){
                if(File::isDirectory($directory)){
                    Storage::delete('topupattachment/'.$item->file_attachment);
                }
            }
            $request->merge(['file_attachment' => $name]);

        }else{
            unset($request['file_attachment']);
        }
        
        $this->table->findOrFail($id)->update($request->all());
        
        $tbUser = $this->table->find($id);
        Activity::log('edit', $this->title, $tbUser->users->full_name.' - amount Rp '.number_format($request->amount, 0, '', '.'));
        // Activity::log('edit', $this->title, $tbUser->users->full_name);
        return redirect(route($this->uri.'.index'))->with('success', trans('message.update'));
    }
    
    public function destroy($id)
    {
        $tb = $this->table->findOrFail($id);
        Activity::log('delete', $this->title, $tb->users->full_name.' - amount Rp '.number_format($tb->amount, 0, '', '.'));
        // Activity::log('delete', $this->title, $tb->users->full_name);
        $tb->delete();
        return response()->json(['msg' => true,'success' => trans('message.delete')]);
    }
}
