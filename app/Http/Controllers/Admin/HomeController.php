<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Http\Controllers\Controller;
use App\Helpers\Activity;
use Carbon\Carbon;
use App\Models\BpUser;
use App\Models\TopUp;
use App\Models\Buletin;
use App\Models\Order;
use App\Models\Comment;
use App\Models\Message;
use App\Models\Article;
use App\Models\MessageReply;
use DB;


class HomeController extends Controller
{
    private $folder = 'admin.home';
    private $uri = 'admin.home';
    private $title = 'Home';
    private $desc = 'Description';

    public function __construct(MessageReply $reply, BpUser $bpUser, TopUp $topUp, Order $order, Comment $comment, Message $message, Buletin $buletin, Article $article)
    {
        // $this->middleware('permission:list_customer', ['only' => ['index','data']]);
        // $this->middleware('permission:create_customer', ['only' => ['create','store']]);
        // $this->middleware('permission:edit_customer', ['only' => ['edit','update']]);
        // $this->middleware('permission:delete_customer', ['only' => ['destroy','postDeleteAll']]);
        $this->bpUser = $bpUser;
        $this->topUp = $topUp;
        $this->order = $order;
        $this->comment = $comment;
        $this->message = $message;
        $this->article = $article;
        $this->reply = $reply;
        $this->url = route($this->uri.'.index');
    }

    public function index()
    {
        $data['title'] = $this->title;
        $data['totalCustomer'] = $this->bpUser->count();
        $data['totalTopup'] = $this->topUp->where('status', 2)->sum('amount');
        $data['totalComment'] = $this->comment->where('is_approved', 0)->count();
        $message = $this->message->where('is_read', 0)->count();
        $reply = $this->reply->where('is_read', 0)->where('is_admin', 0)->count();
        $data['totalMessage'] = ($message + $reply);
        $data['topUp'] = $this->topUp->where('status', 0)->orderBy('id', 'desc')->take(5)->get();
        $data['bestBuletin'] = $this->order->select('bp_buletin_id', DB::raw('COUNT(bp_buletin_id) as total_buletin'))
                                ->groupBy('bp_buletin_id')
                                ->orderBy('total_buletin', 'desc')
                                ->take(5)
                                ->get();
        $data['feed'] = $this->message->where('is_read', 0)
                        ->orWhereHas('reply', function ($query) {
                            $query->where('is_admin', 0)->where('is_read', 0);
                        })
                        ->orderBy('id', 'desc')->first();
        $data['url'] = $this->url;
        return view($this->folder.'.index',$data);
    }

    public function show($id, FormBuilder $formBuilder)
    {
        $users = $this->topUp->findOrFail($id);
        $data['users'] = $users;
        $data['form'] = $formBuilder->create('App\Forms\TopUpForm', [
            'method' => 'POST',
            'model' => $users,
            'url' => route($this->uri.'.topup.confirmation', $id)
        ])
        ->modify('bp_user_id', 'select', [
            'attr' => [
                'disabled' => '',
            ]
        ])
        ->modify('bank_id', 'select', [
            'attr' => [
                'disabled' => '',
            ]
        ])
        ->modify('account_name', 'text', [
            'attr' => [
                'disabled' => '',
            ]
        ])
        ->modify('topup_code', 'text', [
            'attr' => [
                'disabled' => '',
            ]
        ])
        ->modify('cfg_topup_type_id', 'select', [
            'attr' => [
                'disabled' => '',
            ]
        ])
        // ->modify('cfg_topup_status_id', 'select', [
        //     'attr' => [
        //         'disabled' => '',
        //     ]
        // ])
        ->modify('amount', 'text', [
            'attr' => [
                'disabled' => '',
            ],
            'value' => function ($val) {
                return 'Rp '.number_format($val, 0, '', '.');
            }
        ])
        ->modify('topup_date', 'text', [
            'attr' => [
                'disabled' => '',
                'class' => 'datetimepicker form-control'
            ],
            'value' => function ($val) {
                return Carbon::parse($val)->format('Y/m/d H:i');
            }
        ])->modify('confirmation_date', 'text', [
            // 'label' => 'Tanggal',
            'attr' => [
                'disabled' => '',
                'class' => 'datetimepicker form-control'
            ],
            'value' => function ($val) {
                return !empty($val) ? Carbon::parse($val)->format('Y/m/d H:i') : '';
            }
        ])
        ->modify('file_attachment', 'static', [
            'template' => 'admin.topup.detail_attachment',
            'value' => function ($val) {
                return $val;
            }
        ]);
        $data['title'] = $this->title;
        $data['subTitle'] = 'Detail';

        $data['url'] = $this->url;

        return view('admin.topup.detail',$data);
    }

    public function postConfirmation(Request $request, $id)
    {
        if ($request->status == 2) {
            $tp = TopUp::find($id);
            $tp->status = $request->status;
            $tp->confirmation_date = Carbon::now();
            $tp->save();

            $bpUser = BpUser::find($tp->bp_user_id);
            $bpUser->amount_balance = $bpUser->amount_balance + $tp->amount;
            $bpUser->save();
            Activity::log('topup_approved', 'Topup', $bpUser->full_name.' - amount Rp '.number_format($tp->amount, 0, '', '.'));
            // Activity::log('topup_approved', 'Topup', $bpUser->full_name);
        } elseif ($request->status == 1) {
            $tp = TopUp::find($id);
            $tp->status = 1;
            $tp->confirmation_date = Carbon::now();
            $tp->save();
            Activity::log('topup_rejected', 'Topup', $tp->users->full_name.' - amount Rp '.number_format($tp->amount, 0, '', '.'));
            // Activity::log('topup_rejected', 'Topup', $tp->users->full_name);
        }
        return redirect(route($this->uri.'.index'))->with('success',trans('message.confirmation'));
    }
}
