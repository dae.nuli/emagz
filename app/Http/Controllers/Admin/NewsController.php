<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Kris\LaravelFormBuilder\FormBuilder;
use App\Models\News;
use App\Models\PushNotif;
use App\Traits\CrudTrait;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Activity;

use Datatables;
use Form, File, Storage;
use Carbon\Carbon;

use LaravelFCM\Message\Topics;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;
class NewsController extends Controller
{
    private $folder = 'admin.news';
    private $uri = 'admin.news';
    private $title = 'News';
    private $desc = 'Description';

    use CrudTrait;

    public function __construct(News $table)
    {
        $this->middleware('permission:list_news', ['only' => ['index','data']]);
        $this->middleware('permission:create_news', ['only' => ['create','store']]);
        $this->middleware('permission:edit_news', ['only' => ['edit','update']]);
        $this->middleware('permission:delete_news', ['only' => ['destroy','postDeleteAll']]);
        $this->table = $table;
        $this->url = route($this->uri.'.index');
    }

    public function index()
    {
        $data['title'] = $this->title;
        $data['url'] = $this->url;

        // $data['action'] = route($this->uri.'.delete.all');
        $data['ajax'] = route($this->uri.'.data');
        $data['create'] = route($this->uri.'.create');

        return view($this->folder.'.index',$data);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $index = $this->table->select(['id', 'title', 'tag', 'date_published', 'thumbnail','is_published'])
            ->orderBy('id', 'desc');
            return Datatables::of($index)
            ->editColumn('thumbnail', function ($index) {
                return ($index->thumbnail) ? "<img src='".asset('news_thumbnail/'.$index->thumbnail)."' width='120' alt='img'/>" : '-';
            })
            ->editColumn('date_published', function ($index) {
                return ($index->date_published) ? Carbon::parse($index->date_published)->format('d F Y, H:i:s') : '-';
            })
            ->editColumn('is_published', function ($index) {
                if($index->is_published) {
                    return '<span class="badge bg-teal">PUBLISHED</span>';
                } else {
                    return '<span class="badge bg-pink">UNPUBLISHED</span>';
                }
            })
            ->addColumn('action', function ($index) {
                    $tag = Form::open(array("url" => route($this->uri.'.destroy',$index->id), "method" => "DELETE", "class"=>"form"));
                    $tag .= (auth()->user()->can('edit_news')) ? "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'>Edit</a>" : '';
                    $tag .= ($index->is_published) ? " <a href=".route($this->uri.'.unpublish',$index->id)." class='btn btn-warning btn-xs unpublish'>UNPUBLISH</a>" : " <a href=".route($this->uri.'.publish',$index->id)." class='btn btn-success btn-xs publish'>PUBLISH</a>";
                    // $tag .= " <a href=".route($this->uri.'.show',$index->id)." class='btn btn-success btn-xs'>Show</a>";
                    $tag .= (auth()->user()->can('delete_news')) ? " <button type='submit' class='delete btn btn-danger btn-xs'>Delete</button>" : '';
                    $tag .= Form::close();
                    return $tag;
            })
            ->rawColumns(['action', 'is_published', 'thumbnail'])
            ->make(true);
        }
    }

    public function create(FormBuilder $formBuilder)
    {
        $data['form'] = $formBuilder->create('App\Forms\NewsForm', [
            'method' => 'POST',
            'url' => route($this->uri.'.store')
        ]);
        $data['title'] = $this->title;
        $data['subTitle'] = 'create';

        $data['url'] = $this->url;

        return view($this->folder.'.create', $data);
    }

    public function edit($id, FormBuilder $formBuilder)
    {
        $users = $this->table->findOrFail($id);
        $data['form'] = $formBuilder->create('App\Forms\NewsForm', [
            'method' => 'PUT',
            'model' => $users,
            'url' => route($this->uri.'.update', $id)
        ])->modify('date_published', 'text', [
            'label' => 'Tanggal',
            'attr' => [
                'required' => '',
                'class' => 'datetimepicker form-control'
            ],
            'value' => function ($val) {
            	return Carbon::parse($val)->format('Y/m/d H:i');
            }
        ]);
        $data['title'] = $this->title;
        $data['subTitle'] = 'edit';

        $data['url'] = $this->url;

        return view($this->folder.'.create', $data);
    }

    public function store(Request $request)
    {
        Activity::log('create', $this->title, $request->title);
        // Activity::log('create', $this->title);
 // dd($request->all());
        $val = $request->file_foto;
        if(isset($val)) {
            $directory = base_path('public/news_thumbnail');
            $name      = str_random(10).'.'.$val->extension();
            if(!File::isDirectory($directory)){
                File::makeDirectory($directory, 0777);    
            }
            $val->move($directory, $name);
            
            $request->merge(['thumbnail' => $name]);
        }else{
            unset($request['thumbnail']);
        }

        //$insert = array_merge($request->except('content'),$arr);
        $tb = $this->table->create($request->except('content'));

        \DB::statement("update bp_news set content ='".$request->content."'  where id =".$tb->id." ");

        if (!empty($request->is_published)) {

            $notificationBuilder = new PayloadNotificationBuilder($request->title);
            $notificationBuilder->setBody($request->title)
                            ->setSound('default');
                            
            $notification = $notificationBuilder->build();

            $topic = new Topics();
            $topic->topic('dev-news');

            $topicResponse = FCM::sendToTopic($topic, null, $notification, null);

            PushNotif::create([
                'user_id' => Auth::id(),
                'title' => $request->title,
                'content' => $request->content,
                'is_published' => 1,
                'published_date' => Carbon::parse($request->date_published)->format('Y-m-d H:i:s'),
                'topic' => 'dev-news',
                'feature_source' => 'news'
            ]);

        }

        return redirect(route($this->uri.'.index'))->with('success',trans('message.create'));
    }

    public function update(Request $request, $id)
    {
        Activity::log('edit', $this->title, $request->title);
        // Activity::log('edit', $this->title);
        $val = $request->file_foto;
        if(isset($val)) {
            $directory = base_path('public/news_thumbnail');
            $name      = rand(111,9999999).'.'.$val->extension();
            if(!File::isDirectory($directory)){
                File::makeDirectory($directory, 0777);    
            }
            $val->move($directory, $name);

            $item = $this->table->findOrFail($id);
            if(!empty($item->thumbnail)){
                if(File::isDirectory($directory)){
                    Storage::delete('news_thumbnail/'.$item->thumbnail);
                }
            }
            $request->merge(['thumbnail' => $name]);

        }else{
            unset($request['thumbnail']);
        }

        if(empty($request->is_published)){
            $request->merge(['is_published' => 0]);
        }

        $this->table->findOrFail($id)->update($request->all());

        return redirect(route($this->uri.'.index'))->with('success', trans('message.update'));
    }
    
    public function destroy($id)
    {
        // Activity::log('delete', $this->title);
        $tb = $this->table->findOrFail($id);
        Activity::log('delete', $this->title, $tb->title);
        if (!empty($tb->thumbnail)) {
            Storage::delete('news_thumbnail/'.$tb->thumbnail);
        }
        $tb->delete();
        return response()->json(['msg' => true,'success' => trans('message.delete')]);
    }

    public function publish($id)
    {
        if (!empty($id)) {
            $tb = $this->table->find($id);

            if (!$tb->is_published) {

                $notificationBuilder = new PayloadNotificationBuilder($tb->title);
                $notificationBuilder->setBody($tb->title)
                                ->setSound('default');
                                
                $notification = $notificationBuilder->build();

                $topic = new Topics();
                $topic->topic('dev-news');

                $topicResponse = FCM::sendToTopic($topic, null, $notification, null);
                
                PushNotif::create([
                    'user_id' => Auth::id(),
                    'title' => $tb->title,
                    'content' => $tb->title,
                    'is_published' => 1,
                    'published_date' => Carbon::parse($tb->date_published)->format('Y-m-d H:i:s'),
                    'topic' => 'dev-news',
                    'feature_source' => 'news'
                ]);

                $tb->update(['is_published' => 1]);
            }
            return response()->json(['msg' => true,'success' => trans('message.publish')]);
            // return redirect(route($this->uri.'.index'))->with('success', trans('message.publish'));
        }   
    }

    public function unPublish($id)
    {
        if (!empty($id)) {
            $tb = $this->table->find($id);

            if ($tb->is_published) {
                $tb->update(['is_published' => 0]);
            }
            return response()->json(['msg' => true,'success' => trans('message.unpublish')]);
            // return redirect(route($this->uri.'.index'))->with('success', trans('message.unpublish'));
        }   
    }
}
