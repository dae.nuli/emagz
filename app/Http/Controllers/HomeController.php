<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Helpers\MCrypt;
use File;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function test($value='')
    {

          $data['title']='Forgot Password Code';
                          $data['body']='Kode Reset Password anda :  ';
                          $data['mailTo']='racker2000@gmail.com';
       // return $data;
        $result=Mail::send('welcome',['data'=>$data], function ($message) use ($data) {
                            $message->to('racker2000@gmail.com')->subject('Forgot Password Code - Alumni');
                        });
    }

    public function testEn()
    {
        $iv ='fedcba9876543210';
            $mcrypt = new MCrypt($iv);
            $file=base_path('public/buletin/file/YLXfKP4ZrN.pdf');
            $content = file_get_contents($file);
            $encrypted = $mcrypt->encrypt($content,true);
            $fp = fopen('buku-enc.epub', 'w');
            fwrite($fp, $encrypted);
            fclose($fp);
            // $mcrypt = new MCrypt();
            // $file='./buku2.epub';
            // $content = file_get_contents($file);
            // $encrypted = $mcrypt->encrypt($content,true);
        // return File::get(base_path('public/buletin/file/YLXfKP4ZrN.pdf'));
    }
}
