<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use Hash;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Models\BpUser;
use App\Models\Order;
use App\Models\LogUserSession;
use Carbon\Carbon;
use Validator;
use Form, File, Storage;
use App\Helpers\MCrypt;
class DownloadController extends Controller
{
    public function __construct(BpUser $bpUser )
    {
        $this->bpUser = $bpUser;
     
        $this->middleware('jwt.auth');
    }

    public function getEbook(Request $request)
    {
       
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return responseApi(['messages' => 'token expired'],4011);
        }


        $validator = Validator::make($request->all(), [
            'buletin_id' => 'required',           
        ]);

        if ($validator->fails()) {
            return responseApi(['messages' => $validator->errors()],4120);
        }

     //   return $user->id;
        $buletin= Order::with('buletin')
                ->where(['bp_user_id'=>$user->id ,
                'bp_buletin_id'=>$request->buletin_id ,
                'is_purchase_succeeded'=>1 ])->firstOrFail();
        
        $file_ori = $buletin->buletin['filename'];   
        $token = substr($buletin->purchase_success_token,0,16);
        $unik = substr ($token,0,5);

        $file=$unik.$file_ori;
        $path = public_path('file_buletin/'.$file);

        if(File::exists($path)==false)
        {
             
            $lokasi = public_path('file_buletin/'.$file);
            $mcrypt = new MCrypt($token);  
           //$mcrypt = new MCrypt();  

            $file=public_path('buletin/file/'.$file_ori);
            $content = file_get_contents($file);
            $encrypted = $mcrypt->encrypt($content,true);
            
            File::put($lokasi, $encrypted);

          /*  $fp = fopen($lokasi, 'w');
            fwrite($fp, $encrypted);
            fclose($fp);*/
            //dd('tidak di engkirips');
        }


        //return $file;
        $result['purchase_success_token']=$token;
        $result['url_download']=url('file_buletin/'.$unik.$file_ori);
        $result['filename']=$unik.$file_ori;
                
        return responseApi($result,2001);

    }

    public function decrypt(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'file' => 'required',           
            'purchase_success_token' => 'required',           
        ]);

        if ($validator->fails()) {
            return responseApi(['messages' => $validator->errors()],4120);
        }

            $file=$request->file;
            $lokasi = public_path('file_buletin/'.$file);
            $content = file_get_contents($lokasi);
          
            $mcrypt = new MCrypt($request->purchase_success_token);  
            $decrypted = $mcrypt->decrypt($content,true);

            $hasil = public_path('file_buletin/decrp-'.$file);
           /* $fp = fopen($hasil, 'w');
            fwrite($fp, $decrypted);
            fclose($fp);*/
            File::put($hasil, $decrypted);


            $result['purchase_success_token']=$request->purchase_success_token;
            $result['url_download_']=url('file_buletin/decrp-'.$file);
      
            return responseApi($result,2001);
    }
   
  
}
