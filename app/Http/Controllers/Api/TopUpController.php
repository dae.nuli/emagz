<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TopUpStatus;
use App\Models\TopUpType;
use App\Models\TopUp;
use App\Models\Order;
use Validator;
use JWTAuth;
use File;
use Storage;
use Carbon\Carbon;

class TopUpController extends Controller
{
    public function __construct(TopUp $topup, TopUpStatus $topupStatus, TopUpType $topupType)
    {
        $this->topup = $topup;
        $this->topupStatus = $topupStatus;
        $this->topupType = $topupType;
        $this->middleware('jwt.auth');
    }

    public function postTopup(Request $request)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return responseApi(['messages' => 'token expired'],4011);
        }

		$validator = Validator::make($request->all(), [
            'account_name' => 'required',
            'bank_name' => 'required',
            'total_payment' => 'required',
            'payment_date' => 'required',
            'reference_code' => 'required',
            'bank_id' => 'required'
            // 'bank_name' => 'required',
        ]);

        if ($validator->fails()) {
            return responseApi(['messages' => $validator->errors()],4120);
        } else {
            $val = $request->attachment;
            if(isset($val)) {
                $directory = base_path('public/topupattachment');
                $name      = rand(111,9999999).'.'.$val->extension();
                if(!File::isDirectory($directory)){
                    File::makeDirectory($directory, 0777);    
                }
                $val->move($directory, $name);

                // $item = $this->bpUser->findOrFail($user->id);
                // if(!empty($item->photo_filename)){
                //     if(File::isDirectory($directory)){
                //         Storage::delete('topupattachment/'.$item->photo_filename);
                //     }
                // }
                $request->merge(['file_attachment' => $name]);

            }else{
                unset($request['file_attachment']);
            }

            // $status = $this->topupStatus->where('is_default', 1)->first();
            $type = $this->topupType->where('is_default', 1)->first();

            TopUp::create([
                'bp_user_id' => $user->id,
                'bank_id' => $request->bank_id,
                'bank_name' => $request->bank_name,
                'topup_code' => ($request->reference_code) ? $request->reference_code : '-',
                'amount' => $request->total_payment,
                'topup_date' => $request->payment_date,
                'account_name' => $request->account_name,
                'file_attachment' => $name,
                'cfg_topup_type_id' => ($type) ? $type->id : 0,
                'cfg_topup_status_id' => 0,
                // 'cfg_topup_status_id' => ($status) ? $status->id : 0,
            ]);



        	// $art = $this->article->find($request->article_id);
        	// if (!empty($art)) {
	        // 	$rating = Rating::updateOrCreate([
			      //   		'bp_user_id' => $user->id,
			      //   		'bp_article_id' => $request->article_id
         //                    ],[
			      //   		'rating' => $request->rating
		       //  		]);
	        return responseApi(['messages' => 'Topup berhasil'], 2000);
        	// } else {
        	// return responseApi(['messages' => 'Data artikel tidak ditemukan'],4015);
        	// }
	    }
    }

    public function historyTopup()
    {
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return responseApi(['messages' => 'token expired'],4011);
        }

       
        $tb_order = Order::where('bp_user_id', $user->id)->get();
        $tb_topup = TopUp::where('bp_user_id',$user->id)->get();

        $data = [];

    
        $arr_order = $tb_order->map(function ($order) {
            return [
                  'date'=> Carbon::parse($order->created_at)->format('Y-m-d H:i:s'),
                  'subject'=>isset($order->buletin->buletin_title) ? "Donation for '".$order->buletin->buletin_title."'" : '-',
                  'total'=>'Rp '.number_format($order->amount, 0, '', '.'),
                  'status'=>($order->is_purchase_succeeded) ? 'Success' : 'Failed'
                ];
        });


        $arr_topup = $tb_topup->map(function ($topup) {
            return [
                  'date'=> Carbon::parse($topup->topup_date)->format('Y-m-d H:i:s'),
                  'subject'=> 'Top Up Wallet',
                  'total'=> 'Rp '.number_format($topup->amount, 0, '', '.'),
                  'status'=>  $topup->checkStatus($topup->status)
                  // 'status'=>  isset($topup->checkStatus($topup->status)) ? $topup->checkStatus($topup->status) : '-'
                  // 'status'=> isset($topup->status->topup_status) ? $topup->status->topup_status : '-'
                ];
        });

        $data = $arr_order->merge($arr_topup)
                ->sortByDesc('date')
                ->values();
                
        if (count($data)) {
            return responseApi(['data' => $data ], 2001);
        } else {
            return responseApi(['messages' => 'Data history tidak ditemukan'],4015);
        }
    }

}
