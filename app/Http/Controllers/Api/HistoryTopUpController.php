<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TopUp;

use JWTAuth;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use App\Transformers\HistoryTopUpTransformer;
use League\Fractal\Serializer\DataArraySerializer;

class HistoryTopUpController extends Controller
{
    public function __construct(TopUp $topup)
    {
        $this->topup = $topup;
        $this->middleware('jwt.auth');
    }

    public function index(Request $request)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return responseApi(['messages' => 'token expired'],4011);
        }
        // return responseApi(['data' => ['amount' => $user->amount_balance]],2001);


        $index = $this->topup->orderBy('id', 'desc')
                    ->where('bp_user_id', $user->id)
                    ->get();
        $resource = new Collection($index, new HistoryTopUpTransformer());
        return $this->output($resource);
    }

    public function output($resource)
    {
        $manager = new Manager();
        $manager->setSerializer(new DataArraySerializer());
        $mng = $manager->createData($resource)->toArray();
        return responseApi($mng, 2001);
    }
}
