<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
// use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Auth;
// use Illuminate\Support\Facades\Password;
use Illuminate\Http\Request;
use App\Models\BpUser;
use Validator;
use DB;

class ResetPasswordController extends Controller
{
    // use ResetsPasswords;

    public function __construct(BpUser $table)
    {
        $this->table = $table;
        $this->middleware('guest');
    }

    public function reset(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required|max:6',
            'email' => 'required|email|max:255',
            'password' => 'required|confirmed|min:6',
        ]);

        if ($validator->fails()) {
            return responseApi(['messages' => $validator->errors()], 4120);
        } else {

        	$user = $this->table->where('email', $request->email)->first();
        	if (!$user) {
              	return responseApi(['messages' => trans('passwords.user')],4015);
            } 
            if ($user->is_suspended) {
                return responseApi(['reason' => $user->suspended_reason],4012);
            }
            $PR = DB::table('password_resets')
	                ->where('email', $request->email)
	                ->where('token', $request->token)
	                ->count();
            if ($PR) {
            	$this->updatePassword($user, $request->password);
            	$this->deleteToken($request->email);
        		return responseApi(['messages' => trans('passwords.reset')],2000);
                // return response()->json(trans('passwords.reset'));
            } else {
        		return responseApi(['messages' => trans('passwords.token')], 4011);
                // return response()->json(trans($response), 202);
            }
	    }
    }

    public function updatePassword($user, $password)
    {
		$tb = $this->table->find($user->id);
		$tb->password = bcrypt($password);
		$tb->save();
    }

    public function deleteToken($email)
    {
		DB::table('password_resets')
		->where('email', $email)
		->delete();
    }
}
