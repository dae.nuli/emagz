<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\News;
use App\Models\Buletin;

use JWTAuth;

use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use App\Transformers\FilterBuletinTransformer;
use App\Transformers\FilterNewsTransformer;
use League\Fractal\Serializer\DataArraySerializer;

class FilterAndSearchController extends Controller
{
    public function __construct(News $news, Buletin $buletin)
    {
        $this->news = $news;
        $this->buletin = $buletin;
        $this->middleware('jwt.auth');
    }

    public function filterNews(Request $request) 
    {
        $news = $this->news->orderBy('date_published', 'desc');
        if (!empty($request->year)) {
            $news = $news->whereYear('date_published', $request->year);
        }
        
        if (!empty($request->month)) {
            $news = $news->whereMonth('date_published', $request->month);
        }

        if (!empty($request->title)) {
            $news = $news->where('title', 'ILIKE', "%".$request->title."%");
        }

        $data = $news->where('is_published', 1)->get();
        if (count($data)) {
            $resource = new Collection($data, new FilterNewsTransformer());
            return $this->output($resource);
        } else {
            return responseApi(['messages' => 'Data berita tidak ditemukan'],4015);
        }
    }

    public function filterBuletin(Request $request) 
    {
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return responseApi(['messages' => 'token expired'],4011);
        }

        $buletin = $this->buletin->orderBy('date_published', 'desc');
        if (!empty($request->year)) {
            $buletin = $buletin->whereYear('date_published', $request->year);
        }
        
        if (!empty($request->month)) {
            $buletin = $buletin->whereMonth('date_published', $request->month);
        }

        if (!empty($request->title)) {
            $buletin = $buletin->whereHas('article', function ($query) use ($request) {
                            $query->where('title', 'ILIKE', "%".$request->title."%");
                        });
        }

        $data = $buletin->get();
        if (count($data)) {
            $resource = new Collection($data, new FilterBuletinTransformer($user->id));
            return $this->output($resource);
        } else {
            return responseApi(['messages' => 'Data buletin tidak ditemukan'],4015);
        }
    }

    public function output($resource)
    {
        $manager = new Manager();
        $manager->setSerializer(new DataArraySerializer());
        $mng = $manager->createData($resource)->toArray();
        return responseApi($mng, 2001);
    }
}
