<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Message;
use App\Models\MessageReply;
use Validator;
use JWTAuth;
use File;
use Storage;
use Carbon\Carbon;

use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use App\Transformers\FeedbackTransformer;
use League\Fractal\Serializer\DataArraySerializer;

class FeedbackController extends Controller
{
    public function __construct(Message $message, MessageReply $reply)
    {
        $this->message = $message;
        $this->reply = $reply;
        $this->middleware('jwt.auth');
    }

    public function index(Request $request)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return responseApi(['messages' => 'token expired'],4011);
        }
        $data = $this->message->orderBy('id', 'desc')
                ->where('bp_user_id', $user->id)
                ->get();
        
        if (count($data)) {
            $resource = new Collection($data, new FeedbackTransformer(null));
            return $this->output($resource);
        } else {
            return responseApi(['messages' => 'Data feedback tidak ditemukan'],4015);
        }
    }

    public function detail($id)
    {
        $data = $this->message->find($id);
        $resource = new Item($data, new FeedbackTransformer($id));
        MessageReply::where('bp_message_id', $id)->where('is_admin', 1)->update(['is_read' => 1]);
        return $this->output($resource);
    }

    public function postFB(Request $request)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return responseApi(['messages' => 'token expired'],4011);
        }

        $validator = Validator::make($request->all(), [
            // 'bp_user_id' => 'required',
            'title' => 'required',
            'content' => 'required'
            // 'is_read' => 'required'
        ]);

        if ($validator->fails()) {
            return responseApi(['messages' => $validator->errors()],4120);
        } else {
            
            $this->message->create([
                'bp_user_id' => $user->id,
                'title' => $request->title,
                'content' => $request->content,
                'is_read' => 0
            ]);
            return responseApi(['messages' => 'Feedback berhasil dikirim'], 2000);
        }
    }

    public function postReply(Request $request)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return responseApi(['messages' => 'token expired'],4011);
        }

        $validator = Validator::make($request->all(), [
            'message_id' => 'required',
            // 'user_id' => 'required',
            'content' => 'required'
        ]);

        if ($validator->fails()) {
            return responseApi(['messages' => $validator->errors()],4120);
        } else {
            
            $this->reply->create([
                'bp_message_id' => $request->message_id,
                'user_id' => $user->id,
                'content' => $request->content,
                'is_admin' => 0,
                'is_read' => 0
            ]);
            return responseApi(['messages' => 'Berhasil mengirim feedback balasan'], 2000);
        }
    }

    public function output($resource)
    {
        $manager = new Manager();
        $manager->setSerializer(new DataArraySerializer());
        $mng = $manager->createData($resource)->toArray();
        return responseApi($mng, 2001);
    }

}
