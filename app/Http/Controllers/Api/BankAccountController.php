<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\BankAccount;

use JWTAuth;

use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use App\Transformers\BankTransformer;
use League\Fractal\Serializer\DataArraySerializer;

class BankAccountController extends Controller
{
    public function __construct(BankAccount $table)
    {
        $this->table = $table;
        $this->middleware('jwt.auth');
    }

    public function index(Request $request)
    {
        $data = $this->table->where('is_active', 1)->get();
        // return responseApi(['data' => $data],2001);
        
        $resource = new Collection($data, new BankTransformer());
        return $this->output($resource);
    }

    public function output($resource)
    {
        $manager = new Manager();
        $manager->setSerializer(new DataArraySerializer());
        $mng = $manager->createData($resource)->toArray();
        return responseApi($mng, 2001);
    }
}
