<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\BpUser;
use App\Models\Article;
use App\Models\Comment;
use Validator;
use JWTAuth;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use App\Transformers\CommentTransformer;
use League\Fractal\Serializer\DataArraySerializer;

class CommentController extends Controller
{
    public function __construct(Article $article, Comment $comment)
    {
        $this->article = $article;
        $this->comment = $comment;
        $this->middleware('jwt.auth');
    }

    public function postComment(Request $request)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return responseApi(['messages' => 'token expired'],4011);
        }

		$validator = Validator::make($request->all(), [
            'comment' => 'required',
            'article_id' => 'required'
        ]);

        if ($validator->fails()) {
            return responseApi(['messages' => $validator->errors()],4120);
        } else {
        	$art = $this->article->find($request->article_id);
        	if (!empty($art)) {
	        	$comment = Comment::create([
			        		'bp_user_id' => $user->id,
			        		'bp_article_id' => $request->article_id,
                            'comment' => $request->comment
		        		]);

                $resource = new Item($comment, new CommentTransformer());
                return $this->output($resource);
		        // return responseApi(['data' => $result], 2002);
        	} else {
            	return responseApi(['messages' => 'Data artikel tidak ditemukan'],4015);
        	}
	    }
    }

    public function listComment($id)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return responseApi(['messages' => 'token expired'],4011);
        }

        if (empty($id)) {
            return responseApi(['messages' => 'Artikel id harus diisi'],4120);
        } else {
        	$art = $this->comment
                    ->where('is_approved', 1)
                    ->where('bp_article_id', $id)->get();
        	if (count($art)) {

                $resource = new Collection($art, new CommentTransformer());
                return $this->output($resource);
		        // return responseApi(['data' => count($art)], 2001);
        	} else {
            	return responseApi(['messages' => 'Data artikel tidak ditemukan'],4015);
        	}
	    }
    }

    public function output($resource)
    {
        $manager = new Manager();
        $manager->setSerializer(new DataArraySerializer());
        $mng = $manager->createData($resource)->toArray();
        return responseApi($mng, 2001);
    }
}
