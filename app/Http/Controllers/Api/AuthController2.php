<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use Hash;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Models\BpUser;
use App\Models\LogUserToken;
use Carbon\Carbon;
use Validator;

use App\Notifications\SendFcm;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;
use LaravelFCM\Message\Topics;
class AuthController2 extends Controller
{
    public function __construct(BpUser $bpuser)
    {
        $this->bpuser = $bpuser;
        $this->middleware('jwt.auth', ['except' => ['index','authenticate', 'getToken']]);
    }

    public function index()
    {
       // $guzzle->setDefaultOption('verify', false);

       $notificationBuilder = new PayloadNotificationBuilder('my title');
        $notificationBuilder->setBody('Hello world')
                            ->setSound('default');
                            
        $notification = $notificationBuilder->build();

        $topic = new Topics();
        $topic->topic('globals');

        $topicResponse = FCM::sendToTopic($topic, null, $notification, null);

       dd( $topicResponse);
        $topicResponse->shouldRetry();
        $topicResponse->error();

     /*  $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder('my title');
        $notificationBuilder->setBody('Hello world')
                            ->setSound('default');
                            
        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['a_data' => 'my_data']);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $token = "a_registration_from_your_database";

        $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

        $downstreamResponse->numberSuccess();
        $downstreamResponse->numberFailure();
        $downstreamResponse->numberModification();

        //return Array - you must remove all this tokens in your database
        $downstreamResponse->tokensToDelete(); 

        //return Array (key : oldToken, value : new token - you must change the token in your database )
        $downstreamResponse->tokensToModify(); 

        //return Array - you should try to resend the message to the tokens in the array
        $downstreamResponse->tokensToRetry();*/


    }   

    protected function getToken()
    {
        return hash_hmac('sha256', str_random(40), config('app.key'));
    }

    public function authenticate(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|min:6',
        ]);
      
           $credentials = [
                'email' => $request->email,
                'password' => $request->password,
                'is_validated' => 1
            ];

        if ($validator->fails()) {
            return responseApi($validator->errors(),4120);
        }
                       
                $ch = $this->bpuser->where('email', $request->email)->firstOrFail();
     
                    if (! Hash::check($request->password, $ch->password)) {

                        return responseApi('password dont match',4010);

                    }else{
                        if ($ch->is_suspended == 1) {
                            return responseApi(['reason'=>$ch->suspended_reason],4012);

                        } else {
                            if ($ch->is_active == 0 && $ch->is_validated == 0) {
                                // $request->session()->put('token', $this->getToken());
                                $token = $this->getToken();
                                $log = LogUserToken::where('bp_user_id', $ch->id)
                                    ->where('is_taken', 0)
                                    ->where('expired_date', '>=', Carbon::now())
                                    ->orderBy('id','desc')
                                    ->first();
                                    $log->token = $token;
                                    $log->save();

                                return responseApi(['token'=>$token],4013);
                                     
                               
                            } elseif ($ch->is_active == 1 && $ch->is_validated == 0) {
                                     
                                return responseApi($ch,2005);                           
 
                            }
                        }
                    } 
                

            try {
                if (! $token = JWTAuth::attempt($credentials)) {
                     responseApi('failed creating token ',4010);
                }
            } catch (JWTException $e) {
                 responseApi('error jwauth exception  ',4011);
            }

            $user = $this->bpuser->where('email', $request->email)->first();
    
            return responseApi($user->toArray(),2000);
        
    }
}
