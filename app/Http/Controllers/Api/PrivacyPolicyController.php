<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PrivacyPolicy;

// use JWTAuth;

class PrivacyPolicyController extends Controller
{
    public function __construct(PrivacyPolicy $table)
    {
        $this->table = $table;
        $this->folder = 'api.term';
        // $this->middleware('jwt.auth');
    }

    public function index(Request $request)
    {
        // $data = $this->table->where('is_active', 1)->get();
        // return responseApi(['data' => $data],2001);
        $data['term'] = $this->table->where('status', 1)->first();
        return view($this->folder.'.index', $data);
    }
}
