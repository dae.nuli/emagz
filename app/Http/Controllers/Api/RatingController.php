<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\BpUser;
use App\Models\Article;
use App\Models\Rating;
use Validator;
use JWTAuth;

class RatingController extends Controller
{
    public function __construct(Article $article, Rating $rating)
    {
        $this->article = $article;
        $this->rating = $rating;
        $this->middleware('jwt.auth');
    }

    public function postRating(Request $request)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return responseApi(['messages' => 'token expired'],4011);
        }

		$validator = Validator::make($request->all(), [
            'rating' => 'required',
            'article_id' => 'required'
        ]);

        if ($validator->fails()) {
            return responseApi(['messages' => $validator->errors()],4120);
        } else {
        	$art = $this->article->find($request->article_id);
        	if (!empty($art)) {
	        	$rating = Rating::updateOrCreate([
			        		'bp_user_id' => $user->id,
			        		'bp_article_id' => $request->article_id
                            ],[
			        		'rating' => $request->rating
		        		]);
		        return responseApi(['data' => $rating], 2002);
        	} else {
            	return responseApi(['messages' => 'Data artikel tidak ditemukan'],4015);
        	}
	    }
    }

    public function listRating($id)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return responseApi(['messages' => 'token expired'],4011);
        }

        if (empty($id)) {
            return responseApi(['messages' => 'Artikel id harus diisi'],4120);
        } else {
        	$tbl = $this->rating->where('bp_user_id', $user->id)->where('bp_article_id', $id)->first();
        	if (!empty($tbl)) {
		        return responseApi(['data' => $tbl], 2001);
        	} else {
            	return responseApi(['messages' => 'Data artikel tidak ditemukan'],4015);
        	}
	    }
    }
}
