<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\News;
use App\Models\BpUser;
use App\Models\NewsNotif;

use JWTAuth;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use App\Transformers\NewsTransformer;
use League\Fractal\Serializer\DataArraySerializer;

class NewsController extends Controller
{
    public function __construct(News $table, NewsNotif $newsNotif)
    {
        $this->table = $table;
        $this->newsNotif = $newsNotif;
        $this->middleware('jwt.auth', ['except' => ['output']]);
    }

    public function index(Request $request)
    {
        $index = $this->table->orderBy('id', 'desc')
                    ->where('is_published', 1)
		            ->get();
        $resource = new Collection($index, new NewsTransformer());
        return $this->output($resource);
    }

    public function show($id)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return responseApi(['messages' => 'token expired'],4011);
        }
        $index = $this->table->where('id', $id)->where('is_published', 1)->firstOrFail();
        $check = $this->newsNotif->where('bp_user_id', $user->id)->where('bp_news_id', $id)->count();
        if (!$check) {
            $this->newsNotif->create(['bp_user_id' => $user->id, 'bp_news_id' => $id]);
        }
        $resource = new Item($index, new NewsTransformer());
        return $this->output($resource);
    }

    public function output($resource)
    {
        $manager = new Manager();
        $manager->setSerializer(new DataArraySerializer());
        $mng = $manager->createData($resource)->toArray();
        return responseApi($mng, 2001);
    }

    public function notif()
    {
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return responseApi(['messages' => 'token expired'],4011);
        }
        $count = News::whereDoesntHave('notifS', function ($query) use ($user) {
            $query->where('bp_user_id', $user->id);
        })->where('is_published', 1)->count();
        return responseApi(['data' => ['count' => $count]], 2000);
    }
}
