<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
// use Illuminate\Support\Facades\Password;
use Validator;
use DB;
use App\Models\BpUser;
use App\Notifications\ResetPassword;

class ForgotPasswordController extends Controller
{
    // use SendsPasswordResetEmails;

    public function __construct(BpUser $table)
    {
        $this->table = $table;
        $this->middleware('guest');
    }

    public function getResetToken(Request $request)
    {
		$validator = Validator::make($request->all(), [
            'email' => 'required|email|max:255',
        ]);
        if ($validator->fails()) {
            return responseApi(['messages' => $validator->errors()],4120);
        } else {
            $user = $this->table->where('email', $request->email)->first();
            if (!$user) {
              return responseApi(['messages' => trans('passwords.user')],4015);
            } 
            if ($user->is_suspended) {
                return responseApi(['reason' => $user->suspended_reason],4012);
            } 
            $token = rand(111111, 999999);
            $this->sendEmail($user, $token);
            return responseApi(['messages' => 'We have e-mailed your password reset token!'], 2000);
            // return response()->json(['token' => $token]);
        }
    }

    public function sendEmail($user, $token)
    {
        if (DB::table('password_resets')->where('email', $user->email)->count()) {
            DB::table('password_resets')
                ->where('email', $user->email)
                ->update(['token' => $token]);
        } else {
            DB::table('password_resets')->insert(
                ['email' => $user->email, 'token' => $token]
            );
        }
        // Notification::send($this->table->find($user->, new WorkoutAssigned($workout));
        $user->notify(new ResetPassword($token));
        // $this->table->find($user->id)->notify(new ResetPassword($token));
    }
}
