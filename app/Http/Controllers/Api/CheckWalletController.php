<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\BpUser;

use JWTAuth;

class CheckWalletController extends Controller
{
    public function __construct(BpUser $table)
    {
        $this->table = $table;
        $this->middleware('jwt.auth');
    }

    public function index(Request $request)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return responseApi(['messages' => 'token expired'],4011);
        }
        return responseApi(['data' => ['amount' => $user->amount_balance]],2001);
    }
}
