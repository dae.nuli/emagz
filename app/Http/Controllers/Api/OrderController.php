<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Buletin;
use App\Models\BpUser;
use App\Models\Order;

use Validator;
use JWTAuth;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use App\Transformers\BuletinTransformer;
use App\Transformers\BuyBuletinTransformer;
use League\Fractal\Serializer\DataArraySerializer;
use Carbon\Carbon;
use Hash;

class OrderController extends Controller
{
    public function __construct(Buletin $buletin, Order $order, BpUser $bpuser)
    {
        $this->bpuser = $bpuser;
        $this->buletin = $buletin;
        $this->order = $order;
        $this->middleware('jwt.auth', ['except' => ['getToken']]);
    }

    public function index(Request $request)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return responseApi(['messages' => 'token expired'],4011);
        }

		$validator = Validator::make($request->all(), [
            'password' => 'required|min:6',
            'buletin_id' => 'required',
        ]);

        if ($validator->fails()) {
            return responseApi(['messages' => $validator->errors()],4120);
        } else {
        	$getUser = $this->bpuser->where('email', $user->email)->firstOrFail();
            if (!empty($getUser)) {

                if (Hash::check($request->password, $getUser->password)) {
                	$tbBul = $this->buletin->where('id', $request->buletin_id)
					        	->where('is_published', 1)
					        	->first();
		        	if (!empty($tbBul)) {
				        $tbUser = $this->bpuser->where('id', $user->id)->first();
				        if ($tbUser->amount_balance) {
				        	// $tbBul = $this->buletin->where('id', $request->buletin_id)
							      //   	->where('is_published', 1)
							      //   	->first();
				        	if ($tbBul->is_free) {
				        		$this->order->create([
				        			'bp_user_id' => $user->id,
				        			'bp_buletin_id' => $request->buletin_id,
				        			'order_code' => str_random(10),
				        			'amount' => $tbBul->price,
				        			'purchase_date' => Carbon::now(),
				        			'purchase_success_token' => $this->getToken(),
				        			'is_purchase_succeeded' => 1,
				        			'payload' => 1,
			        			]);
			        			$this->buletin->where('id', $request->buletin_id)
					        			->update(['purchased_count' => $tbBul->purchased_count+1]);
						        return responseApi(['data' => ['url' => asset('buletin/file/'.$tbBul->filename)]], 2006);
				        	} else {
				        		if ($tbUser->amount_balance >= $tbBul->price) {
					        		$this->order->create([
					        			'bp_user_id' => $user->id,
					        			'bp_buletin_id' => $request->buletin_id,
					        			'order_code' => str_random(10),
					        			'amount' => $tbBul->price,
					        			'purchase_date' => Carbon::now(),
					        			'purchase_success_token' => $this->getToken(),
					        			'is_purchase_succeeded' => 1,
					        			'payload' => 1,
				        			]);
				        			$this->buletin->where('id', $request->buletin_id)
						        			->update(['purchased_count' => $tbBul->purchased_count+1]);

				        			$sisaUang = ($tbUser->amount_balance - $tbBul->price);

				        			$this->bpuser->where('id', $user->id)->update(['amount_balance' => $sisaUang]);
							        return responseApi(['data' => ['url' => asset('buletin/file/'.$tbBul->filename)]], 2006);
						        	// return responseApi(['messages' => 'Transaksi berhasil'], 2006);
				        		} else {
						            return responseApi(['messages' => 'Balance anda tidak cukup, silahkan lakukan topup'], 4016);
				        		}
				        	}

				        } else {
				            return responseApi(['messages' => 'Balance anda tidak cukup, silahkan lakukan topup'], 4016);
				        }
	        		} else {
				        return responseApi(['messages' => 'Data buletin tidak ditemukan'], 4015);
	        		}	

				} else {
                	return responseApi(['messages' => 'password dont match'],4010);
                }
            } else {
                return responseApi(['messages' => 'password dont match'],4010);
            }
	    }
    }

    protected function getToken()
    {
        return hash_hmac('sha256', str_random(40), config('app.key'));
    }
}
