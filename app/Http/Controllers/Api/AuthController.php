<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use Hash;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Models\BpUser;
use App\Models\LogUserToken;
use App\Models\LogUserSession;
use Carbon\Carbon;
use Validator;

class AuthController extends Controller
{
    public function __construct(BpUser $bpUser, LogUserToken $logToken)
    {
        $this->bpUser = $bpUser;
        $this->logToken = $logToken;
        $this->middleware('jwt.auth', ['except' => ['authenticate', 'getToken']]);
    }

    protected function getToken()
    {
        return hash_hmac('sha256', str_random(40), config('app.key'));
    }

    public function authenticate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|min:6',
        ]);
        if ($validator->fails()) {
            return responseApi(['messages' => $validator->errors()],4120);
        } else {
            
            $credentials = $request->only('email', 'password');
                $ch = $this->bpUser->where('email', $request->email)->firstOrFail();
                if (!empty($ch)) {
                    if (Hash::check($request->password, $ch->password)) {
                        if ($ch->is_suspended == 1) {
                            return responseApi(['reason' => $ch->suspended_reason],4012);
                        } else {
                            if ($ch->is_active == 0) {
                                $token = $this->getToken();
                                
                                $log = $this->logToken->where('bp_user_id', $ch->id)->first();
                                $log->token = $token;
                                $log->save();

                                return responseApi(['token' => $token],4013);
                            }
                        }
                    } else {
                        return responseApi(['messages' => 'password dont match'],4010);
                    }
                } else {
                    return responseApi(['messages' => 'user not found'],4010);
                }

            try {
                if (! $token = JWTAuth::attempt($credentials)) {
                    return responseApi(['messages' => 'failed creating token'],4010);
                }
            } catch (JWTException $e) {
                return responseApi(['messages' => 'error jwauth exception'],4011);
            }

            $user = $this->bpUser->where('email', $request->email)->first()->toArray();
            $user = array_add($user, 'login_date', Carbon::now()->toDateTimeString());
            return responseApi([
                'token' => $token,
                'data' => $user
            ],2000);
        }
    }

    public function userSessionLogin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required',
            'token' => 'required',
            'device' => 'required',
            'login_date' => 'required'
        ]);
        if ($validator->fails()) {
            return responseApi(['messages' => $validator->errors()],4120);
        } else {
            $user = $this->bpUser->where('uid', $request->customer_id)->first();
            if (!empty($user)) {
                $lus = LogUserSession::create([
                        'bp_user_id' => $user->id,
                        'token' => $request->token,
                        'device_id' => $request->device,
                        'login_date' => $request->login_date,
                        'is_active' => 1
                    ]);
                // $lus = new LogUserSession;
                // $lus->bp_user_id = $user->id;
                // $lus->token = $request->token;
                // $lus->device_id = $request->device;
                // $lus->login_date = $request->login_date;
                // $lus->is_active = 1;
                // $lus->save();
                return responseApi(['messages' => 'Customer session login has been created'],2002);
            } else {
                return responseApi(['messages' => 'Customer id not found'],4015);
            }
        }
    }

    public function userSessionLogout(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required',
            'token' => 'required',
            'device' => 'required'
        ]);
        if ($validator->fails()) {
            return responseApi(['messages' => $validator->errors()],4120);
        } else {
            $user = $this->bpUser->where('uid', $request->customer_id)->first();
            if (!empty($user)) {
                $log = LogUserSession::where('bp_user_id', $user->id)
                        ->where('token', $request->token)
                        ->update(['logout_date' => Carbon::now()]);
                if ($log) {
                    return responseApi(['messages' => 'Customer session logout has been created'],2002);
                } else {
                    return responseApi(['messages' => 'Token not found'],4015);
                }
            } else {
                return responseApi(['messages' => 'Customer id not found'],4015);
            }
        }
    }
}
