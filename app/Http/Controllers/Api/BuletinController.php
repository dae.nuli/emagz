<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Buletin;
use App\Models\BpUser;
use App\Models\BuletinNotif;

use JWTAuth;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use App\Transformers\BuletinTransformer;
use League\Fractal\Serializer\DataArraySerializer;

class BuletinController extends Controller
{
    public function __construct(Buletin $table, BuletinNotif $buletinNotif)
    {
        $this->table = $table;
        $this->buletinNotif = $buletinNotif;
        $this->middleware('jwt.auth', ['except' => ['output']]);
    }

    public function index(Request $request)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return responseApi(['messages' => 'token expired'],4011);
        }
        $index = $this->table->orderBy('id', 'desc')
                    ->where('is_published', 1)
		            ->get();
        $resource = new Collection($index, new BuletinTransformer($user->id));
        return $this->output($resource);
    }

    public function show($id)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return responseApi(['messages' => 'token expired'],4011);
        }
        $index = $this->table->where('id', $id)->where('is_published', 1)->firstOrFail();
        $check = $this->buletinNotif->where('bp_user_id', $user->id)->where('bp_buletin_id', $id)->count();
        if (!$check) {
            $this->buletinNotif->create(['bp_user_id' => $user->id, 'bp_buletin_id' => $id]);
        }
        $resource = new Item($index, new BuletinTransformer($user->id));
        return $this->output($resource);
    }

    public function output($resource)
    {
        $manager = new Manager();
        $manager->setSerializer(new DataArraySerializer());
        $mng = $manager->createData($resource)->toArray();
        return responseApi($mng, 2001);
    }

    public function notif()
    {
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return responseApi(['messages' => 'token expired'],4011);
        }
        $count = Buletin::whereDoesntHave('notif', function ($query) use ($user) {
            $query->where('bp_user_id', $user->id);
        })->where('is_published', 1)->count();
        return responseApi(['data' => ['count' => $count]], 2000);
    }
}
