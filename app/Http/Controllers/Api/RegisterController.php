<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Models\BpUser;
use App\Models\LogUserToken;
use Ramsey\Uuid\Uuid;
use Carbon\Carbon;

use App\Notifications\SendActivationEmail;
use JWTAuth;
use Hash;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use App\Transformers\RegisterTransformer;
use File;
use Storage;
 
class RegisterController extends Controller
{
    public function __construct(BpUser $bpUser, LogUserToken $logToken)
    {
        $this->bpUser = $bpUser;
        $this->logToken = $logToken;
        $this->middleware('jwt.auth', ['except' => ['register', 'pushVerification', 'getToken', 'requestCodeActivation']]);
    }

    public function register(Request $request)
    {
		$validator = Validator::make($request->all(), [
            'full_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:bp_users,email',
            'password' => 'required|min:6',
        ]);

        $fractal = new Manager();

        if ($validator->fails()) {
            return responseApi(['messages' => $validator->errors()],4120);
        } else {
            $tb = BpUser::create([
                    'uid' => Uuid::uuid4()->getHex(),
                    'full_name' => $request->full_name,
                    'email' => $request->email,
                    'password' => bcrypt($request->password)
                ]);
        	// $tb = new BpUser;
        	// $tb->uid = Uuid::uuid4()->getHex();
        	// $tb->full_name = $request->full_name;
        	// $tb->email = $request->email;
        	// $tb->password = bcrypt($request->password);
        	// $tb->save();

            $token = $this->getToken();

            // LogUserToken::where('bp_user_id', $tb->id)->delete();
            $random = rand(111111, 999999);
            $lut = $this->logToken->where('code_activation', $random)->count();
            while ($lut) {
                $random = rand(111111, 999999);
                $lut = $this->logToken->where('code_activation', $random)->count();
            }
            $lt = $this->logToken->create([
                    'bp_user_id' => $tb->id,
                    'token' => $token,
                    'is_taken' => 0,
                    'code_activation' => $random,
                    'expired_date' => Carbon::now()->addHours(24),
                ]);
        	// $log_token = new LogUserToken;
        	// $log_token->bp_user_id = $tb->id;
         //    $log_token->token = $token;
         //    $log_token->is_taken = 0;
        	// $log_token->code_activation = rand(111111, 999999);
        	// $log_token->expired_date = Carbon::now()->addHours(24);
        	// $log_token->save();

            $tb1 = $this->bpUser->where('id',$tb->id)->first();
        	$tb->notify(new SendActivationEmail($tb1));

            return responseApi([
                'token' => $token,
                'data' => $tb
            ],2000);
        }
    }

    public function editProfile(Request $request)
    {
        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return responseApi(['messages' => 'token expired'],4011);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return responseApi(['messages' => 'token expired'],4011);

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return responseApi(['messages' => 'token invalid'],4011);

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return responseApi(['messages' => 'token absent'],4011);

        }

        $data['full_name'] = 'required';
        $data['email'] = 'email|unique:bp_users,email,'.$user->id;
        $data['phone_number'] = 'required';
        $data['address'] = 'required';
        $data['country'] = 'required';
        $data['province'] = 'required';
        $data['city'] = 'required';
        $data['subdistrict'] = 'required';
        // if (!empty($request->password) || !empty($request->password_confirmation)) {
        //     $data['password'] = 'required|confirmed|min:6';
        // }
        if (!empty($request->old_password) || !empty($request->new_password)) {
            $data['new_password'] = 'required|min:6';
        }

        $validator = Validator::make($request->all(), $data);

        if ($validator->fails()) {
            return responseApi($validator->errors(),4120);
        } else {

            $val = $request->file_foto;
            if(isset($val)) {
                $directory = base_path('public/customers');
                $name      = rand(111,9999999).'.'.$val->extension();
                if(!File::isDirectory($directory)){
                    File::makeDirectory($directory, 0777);    
                }
                $val->move($directory, $name);

                $item = $this->bpUser->findOrFail($user->id);
                if(!empty($item->photo_filename)){
                    if(File::isDirectory($directory)){
                        Storage::delete('customers/'.$item->photo_filename);
                    }
                }
                $request->merge(['photo_filename' => $name]);

            }else{
                unset($request['photo_filename']);
            }

            if (!empty($request->new_password) && !empty($request->old_password)) {
                if (Hash::check($request->old_password, $user->password)) {
                    $request->merge(['password' => bcrypt($request->new_password)]);
                } else {
                    return responseApi(['messages' => 'Password not match'], 4010);
                }
                    // $checkPass = $this->bpUser->where()
            }

            // if (!empty($request->password)) {
            //     $request->merge(['password' => bcrypt($request->password)]);
            // }
            $request->merge(['is_validated' => 1]);
            $tbUser = $this->bpUser->findOrFail($user->id)->update($request->all());
            $cust = collect($this->bpUser->findOrFail($user->id));
            // $cust = $this->bpUser->findOrFail($user->id)->toArray();
            $dataUser = $cust->merge(['photo_filename' => asset('customers/'.$cust['photo_filename'])])->all();
            return responseApi([
                'data' => $dataUser
            ],2000);
        }

    }

    public function pushVerification(Request $request)
    {
        $lg = $this->logToken->where('token', $request->token)->first();
        if (!empty($lg)) {

            $validator = Validator::make($request->all(), [
                'code.0' => 'required',
                'code.1' => 'required',
                'code.2' => 'required',
                'code.3' => 'required',
                'code.4' => 'required',
                'code.5' => 'required',
            ]);

            if ($validator->fails()) {
                return responseApi(['code' => 'The code field is required.'],4120);
            } else {

                $lut = $this->logToken->where('code_activation', implode('', $request->code))
                            ->where('is_taken', 0)
                            ->first();
                if (!empty($lut)) {
                    if ($lut->expired_date >= Carbon::now()) {
                        $lut->is_taken = 1;
                        $lut->save();

                        $this->bpUser->find($lut->bp_user_id)->update(['is_active' => 1]);

                        $tb = $this->bpUser->find($lut->bp_user_id);

                        // $tb->is_active = 1;
                        // $tb->save();
                        $token = JWTAuth::fromUser($tb);

                        return responseApi([
                            'token' => $token,
                            'data' => $tb
                        ],2000);
                    } else {
                        return responseApi(['messages' => 'Your code has been expired, please request for new code'],4010);
                    }
                } else {
                    return responseApi(['messages' => 'Code verification not found'],4010);
                }

                // $log_token = LogUserToken::where('code_activation', implode('', $request->code))
                // ->where('expired_date', '>=', Carbon::now())
                // ->where('is_taken', 0)
                // ->first();
                // if (!empty($log_token)) {
                //     $log_token->is_taken = 1;
                //     $log_token->save();

                //     $tb = $this->bpUser->find($log_token->bp_user_id);
                //     $tb->is_active = 1;
                //     $tb->save();

                //     $token = JWTAuth::fromUser($tb);
                //     return responseApi([
                //         'token' => $token,
                //         'data' => $tb
                //     ],2000);

                // } else {

                //     return responseApi(['messages' => 'Code verification not found, please reset code activation'],4010);

                // }
            }
        } else {
            return responseApi(['messages' => 'token invalid'],4011);
        }

    }

    protected function getToken()
    {
        return hash_hmac('sha256', str_random(40), config('app.key'));
    }

    public function requestCodeActivation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|min:6',
        ]);

        if ($validator->fails()) {
            return responseApi(['messages' => $validator->errors()],4120);
        } else {
            $usr = $this->bpUser->where('email', $request->email)
                        ->first();
                if (!empty($usr)) {
                    if (Hash::check($request->password, $usr->password)) {
                        if ($usr->is_suspended == 1) {
                            return responseApi(['messages' => $usr->suspended_reason],4012);
                        } else {
                            if ($usr->is_active == 0) {

                                $token = $this->getToken();
                                
                                $this->logToken->where('bp_user_id', $usr->id)->delete();

                                $random = rand(111111, 999999);
                                $lut = $this->logToken->where('code_activation', $random)->count();
                                while ($lut) {
                                    $random = rand(111111, 999999);
                                    $lut = $this->logToken->where('code_activation', $random)->count();
                                }
                                $lt = $this->logToken->create([
                                        'bp_user_id' => $usr->id,
                                        'token' => $token,
                                        'is_taken' => 0,
                                        'code_activation' => $random,
                                        'expired_date' => Carbon::now()->addHours(24),
                                    ]);

                                // $log = $this->logToken->where('bp_user_id', $usr->id)
                                //     ->where('is_taken', 0)
                                //     ->orderBy('id','desc')
                                //     ->first();

                                // $log->token = $token;
                                // $log->code_activation = rand(111111, 999999);
                                // $log->expired_date = Carbon::now()->addHours(24);
                                // $log->save();

                                $tb = $this->bpUser->find($usr->id);

                                $usr->notify(new SendActivationEmail($tb));
                                return responseApi([
                                    'token' => $token,
                                ],4014);

                            } else {
                                return responseApi(['messages' => 'Your account has been actived'],4015);
                            }
                        }
                    } else {
                        return responseApi(['messages' => 'password dont match'],4010);
                    }
                } else {
                    return responseApi(['messages' => 'user not found'],4010);
                }
        }

    }
}
