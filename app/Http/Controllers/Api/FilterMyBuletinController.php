<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Buletin;

use JWTAuth;

use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use App\Transformers\MyBuletinTransformer;
use League\Fractal\Serializer\DataArraySerializer;

class FilterMyBuletinController extends Controller
{
    public function __construct(Buletin $buletin)
    {
        $this->buletin = $buletin;
        $this->middleware('jwt.auth');
    }

    public function filterBuletin(Request $request) 
    {
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return responseApi(['messages' => 'token expired'],4011);
        }

        $buletin = $this->buletin->orderBy('date_published', 'desc');
        if (!empty($request->year)) {
            $buletin = $buletin->whereYear('date_published', $request->year);
        }
        
        if (!empty($request->month)) {
            $buletin = $buletin->whereMonth('date_published', $request->month);
        }

        if (!empty($request->title)) {
            $buletin = $buletin->whereHas('article', function ($query) use ($request) {
                            $query->where('title', 'ILIKE', "%".$request->title."%");
                        });
            // $buletin = $buletin->where('buletin_title', 'ILIKE', "%".$request->title."%");
        }

        $data = $buletin->whereHas('order', function ($query) use ($user) {
                        $query->where('bp_user_id', $user->id);
                    })
                    ->get();
        if (count($data)) {
            $resource = new Collection($data, new MyBuletinTransformer($user->id));
            return $this->output($resource);
        } else {
            return responseApi(['messages' => 'Data buletin tidak ditemukan'],4015);
        }
    }

    public function output($resource)
    {
        $manager = new Manager();
        $manager->setSerializer(new DataArraySerializer());
        $mng = $manager->createData($resource)->toArray();
        return responseApi($mng, 2001);
    }
}
