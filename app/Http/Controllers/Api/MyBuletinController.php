<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Buletin;
use App\Models\BpUser;

use JWTAuth;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use App\Transformers\MyBuletinTransformer;
use League\Fractal\Serializer\DataArraySerializer;

class MyBuletinController extends Controller
{
    public function __construct(Buletin $table)
    {
        $this->table = $table;
        $this->middleware('jwt.auth', ['except' => ['output']]);
    }

    public function index(Request $request)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return responseApi(['messages' => 'token expired'],4011);
        }
        $index = $this->table->orderBy('id', 'desc')
                    // ->where('is_published', 1)
                    ->whereHas('order', function ($query) use ($user) {
                        $query->where('bp_user_id', $user->id);
                    })
		            ->get();
        $resource = new Collection($index, new MyBuletinTransformer($user->id));
        return $this->output($resource);
    }

    public function output($resource)
    {
        $manager = new Manager();
        $manager->setSerializer(new DataArraySerializer());
        $mng = $manager->createData($resource)->toArray();
        return responseApi($mng, 2001);
    }
}
