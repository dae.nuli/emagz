<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class PrivacyPolicyForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('author', 'text', [
                'label' => 'Penulis',
                'attr' => ['required' => '']
            ])
            ->add('content', 'textarea', [
                // 'label' => 'Isi Berita',
                'attr' => [
                    'required' => '',
                    'id' => 'summernote'
                ]
            ])
            ->add('status', 'choice', [
                // 'label' => 'Module',
                'choices' => [1 => 'Release', 0 => 'Expired'],
                'choice_options' => [
                    'wrapper' => ['class' => 'radio'],
                    'label_attr' => ['class' => 'col-lg-10 col-md-10 col-sm-8 col-xs-7'],
                ],
                // 'choice_options' => [
                //     'wrapper' => ['class' => 'choice-wrapper'],
                //     'label_attr' => ['class' => 'label-class'],
                // ],
                // 'selected' => ['en', 'fr'],
                'expanded' => true,
                'multiple' => false
            ]);
            // ->add('url', 'text', [
            //     'value' => 'termsandconditions',
            //     // 'label' => 'Deskripsi',
            //     'attr' => ['required' => '', 'disabled' => '']
            // ]);
    }
}
