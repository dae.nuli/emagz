<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class NewsForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('title', 'text', [
                'label' => 'Judul',
                'attr' => ['required' => '']
            ])
            ->add('tag', 'text', [
                'attr' => [
                    'data-role' => 'tagsinput'
                ]
        	])
            ->add('content', 'textarea', [
                'label' => 'Isi Berita',
                'attr' => [
	                'required' => '',
                    'id' => 'summernote'
                    // 'id' => 'ckeditor'
	                // 'id' => 'tinymce'
                ]
            ])
            ->add('file_foto', 'file', [
                // 'label' => 'Thumbnail',
                // 'attr' => [
                //     'id' => 'imgInp'
                // ]
                'template' => 'admin.news.dropzone'
            ])
            ->add('date_published', 'text', [
                'label' => 'Tanggal',
                'attr' => [
                    'required' => '',
                    'class' => 'datetimepicker form-control'
                ]
            ])
            ->add('is_published', 'checkbox', [
                'value' => 1,
                'label' => 'Publish'
            ]);
    }
}
