<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class CreateBuletinForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('buletin_title', 'text', [
                'label' => 'Judul',
                'attr' => ['required' => '']
            ])
            ->add('edition', 'text', [
                'label' => 'Edisi',
                'attr' => ['required' => '']
            ])
            ->add('buletin_code', 'text', [
                'label' => 'Kode Buletin',
                'attr' => ['required' => '']
            ])
            ->add('price', 'text', [
                'label' => 'Harga',
                'attr' => [
                    'required' => '',
                    'class' => 'price form-control'
                ]
            ])
            ->add('tag', 'text', [
                'label' => 'Tag',
                'attr' => [
	                'required' => '',
                    'data-role' => 'tagsinput'
                ]
            ])
            ->add('date_published', 'text', [
                'label' => 'Tanggal',
                'attr' => [
                    'required' => '',
                    'class' => 'datetimepicker form-control'
                ]
            ])
            ->add('file_buletin', 'file', [
                'label' => 'File Buletin'
            ])
            ->add('file_thumbnail', 'file', [
                // 'label' => 'File Thumbnail'
                'template' => 'admin.buletin.file_thumbnail'
            ])
            ->add('is_free', 'checkbox', [
                'value' => 1,
                'label' => 'Gratis'
            ])
            ->add('is_published', 'checkbox', [
                'value' => 1,
                'label' => 'Published'
            ]);
    }
}
