<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class SettingForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('variable', 'text', [
                // 'label' => 'version',
                'attr' => ['required' => '']
            ])
            
            ->add('value', 'textarea', [
                // 'label' => 'Isi Berita',
                'attr' => [
                    'required' => '',
                    'id' => 'summernote'
                ]
            ]);
            
                 
    }
}
