<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class RoleForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('role', 'text', [
                'label' => 'Role',
                'attr' => ['required' => '']
            ])
            ->add('description', 'text', [
                'label' => 'Deskripsi',
                'attr' => ['required' => '']
            ]);
    }
}
