<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;
use App\Models\Role;
class UsersForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('name', 'text', [
                'label' => 'Nama',
                'attr' => ['required' => '']
            ])
            ->add('email', 'email', [
                'label' => 'Email',
                'attr' => ['required' => '']
            ])
            ->add('password', 'password', [
                'label' => 'Password',
                'attr' => ['required' => '']
            ])
            ->add('file_foto', 'file', [
                'label' => 'Foto'
            ])
            ->add('role', 'select', [
                'choices' => Role::pluck('role', 'id')->toArray(),
                'empty_value' => '- Please Select -',
                'label' => 'Role',
                'attr' => ['required' => '', 'class' => 'form-control show-tick']
            ]);
    }
}
