<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class CreateArticleForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('title_article[]', 'text', [
                'label' => 'Judul',
                'attr' => ['required' => '']
            ])
            ->add('description_article[]', 'textarea', [
                'label' => 'Deskripsi',
                'attr' => [
                    'required' => '',
                    'class' => 'form-control no-resize',
                    'cols' => 30,
                    'rows' => 5
                ]
            ])
            ->add('author_article[]', 'text', [
                'label' => 'Penulis',
                'attr' => ['required' => '']
            ])
            ->add('file_thumbnail_article[]', 'file', [
                'template' => 'admin.buletin.file_thumbnail_article'
                // 'label' => 'File Thumbnail'
            ]);
    }
}
