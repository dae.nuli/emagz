<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;
use App\Models\BankAccount;

class BankForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('bank_id', 'select', [
                'choices' => BankAccount::pluck('bank_name', 'id')->toArray(),
                'empty_value' => '- Please Select -',
                'label' => 'Nama Bank',
                'attr' => ['required' => '', 'class' => 'form-control show-tick']
            ]);
    }
}
