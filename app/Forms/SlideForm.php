<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class SlideForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('title', 'text', [
                'label' => 'Judul',
                'attr' => ['required' => '']
            ])
            ->add('description', 'textarea', [
                'label' => 'Deskripsi',
                'attr' => [
                    'required' => '',
                    'class' => 'form-control no-resize',
                    'cols' => 30,
                    'rows' => 5
                ]
            ])
            ->add('filename', 'file', [
                'template' => 'admin.slide.create_file'
                // 'label' => 'Gambar',
                // 'attr' => ['id' => 'imgInp']
            ])
            ->add('is_active', 'choice', [
                'label' => 'Status',
                'attr' => ['required' => ''],
                'choices' => [1 => 'YES', 0 => 'NO'],
                'choice_options' => [
                    'wrapper' => ['class' => 'radio'],
                    'label_attr' => ['class' => 'col-lg-10 col-md-10 col-sm-8 col-xs-7'],
                ],
                // 'choice_options' => [
                //     'wrapper' => ['class' => 'choice-wrapper'],
                //     'label_attr' => ['class' => 'label-class'],
                // ],
                'selected' => [0],
                'expanded' => true,
                'multiple' => false
            ]);
    }
}
