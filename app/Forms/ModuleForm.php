<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class ModuleForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('module', 'text', [
                'label' => 'Module',
                'attr' => ['required' => '']
            ])
            ->add('description', 'text', [
                'label' => 'Deskripsi',
                'attr' => ['required' => '']
            ]);
    }
}
