<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class TopupTypeForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('topup_type', 'number', [
                'label' => 'Tipe',
                'attr' => ['required' => '']
            ])
            ->add('description', 'text', [
                'label' => 'Keterangan',
                'attr' => ['required' => '']
            ])
            ->add('is_default', 'choice', [
                'label' => 'Default',
                'attr' => ['required' => ''],
                'choices' => [1 => 'YES', 0 => 'NO'],
                'choice_options' => [
                    'wrapper' => ['class' => 'radio'],
                    'label_attr' => ['class' => 'col-lg-10 col-md-10 col-sm-8 col-xs-7'],
                ],
                // 'choice_options' => [
                //     'wrapper' => ['class' => 'choice-wrapper'],
                //     'label_attr' => ['class' => 'label-class'],
                // ],
                // 'selected' => ['en', 'fr'],
                'expanded' => true,
                'multiple' => false
            ]);
            // ->add('is_default', 'select', [
            //     'choices' => [1 => 'YES', 0 => 'NO'],
            //     'label' => 'Default',
            //     'attr' => ['required' => '', 'class' => 'form-control show-tick']
            // ]);
    }
}
