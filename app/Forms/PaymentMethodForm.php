<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;
use App\Models\PaymentMethod;

class PaymentMethodForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('title_method[]', 'text', [
                'label' => 'Title',
                'attr' => ['required' => '']
            ])
            ->add('description_method[]', 'textarea', [
                'label' => 'Description',
                'attr' => [
                    'required' => '',
                    'class' => 'form-control summernote'
                ]
            ]);
    }
}
