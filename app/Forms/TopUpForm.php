<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;
use App\Models\BpUser;
use App\Models\BankAccount;
use App\Models\TopUpStatus;
use App\Models\TopUpType;

class TopUpForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('bp_user_id', 'select', [
                'choices' => BpUser::pluck('full_name', 'id')->toArray(),
                'empty_value' => '- Please Select -',
                'label' => 'Customer',
                'attr' => ['required' => '', 'class' => 'form-control show-tick']
            ])
            ->add('bank_id', 'select', [
                'choices' => BankAccount::pluck('bank_name', 'id')->toArray(),
                'empty_value' => '- Please Select -',
                'label' => 'Nama Bank',
                'attr' => ['required' => '', 'class' => 'form-control show-tick']
            ])
            ->add('topup_code', 'text', [
                // 'label' => 'version',
                'attr' => ['required' => '']
            ])
            ->add('amount', 'number', [
                'attr' => ['required' => '']
            ])
            ->add('topup_date', 'text', [
                'attr' => [
                    'required' => '',
                    'class' => 'datetimepicker form-control'
                ]
            ])
            ->add('account_name', 'text', [
                // 'label' => 'version',
                'attr' => ['required' => '']
            ])
            ->add('confirmation_date', 'text', [
                'attr' => [
                    'disabled' => '',
                    // 'required' => '',
                    'class' => 'datetimepicker form-control'
                ]
            ])
            ->add('file_attachment', 'file', [
                'template' => 'admin.topup.create_attachment'
                // 'label' => 'Foto'
            ])
            ->add('cfg_topup_type_id', 'select', [
                'choices' => TopUpType::pluck('topup_type', 'id')->toArray(),
                'empty_value' => '- Please Select -',
                'label' => 'Tipe Topup',
                'attr' => ['required' => '', 'class' => 'form-control show-tick']
            ])
            // ->add('cfg_topup_status_id', 'select', [
            //     'choices' => TopUpStatus::pluck('topup_status', 'id')->toArray(),
            //     'empty_value' => '- Please Select -',
            //     'label' => 'Status Topup',
            //     'attr' => ['required' => '', 'class' => 'form-control show-tick']
            // ])
            ->add('status', 'choice', [
                // 'label' => 'Status',
                'choices' => [2 => 'APPROVED', 1 => 'REJECTED'],
                'choice_options' => [
                    'wrapper' => ['class' => 'radio'],
                    'label_attr' => ['class' => 'col-lg-10 col-md-10 col-sm-8 col-xs-7'],
                ],
                // 'choice_options' => [
                //     'wrapper' => ['class' => 'choice-wrapper'],
                //     'label_attr' => ['class' => 'label-class'],
                // ],
                // 'selected' => ['en', 'fr'],
                'expanded' => true,
                'multiple' => false
            ]);
            // ->add('url', 'text', [
            //     'value' => 'termsandconditions',
            //     // 'label' => 'Deskripsi',
            //     'attr' => ['required' => '', 'disabled' => '']
            // ]);
    }
}
