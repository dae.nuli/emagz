<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class CustomersForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('full_name', 'text', [
                'label' => 'Nama',
                'attr' => ['required' => '']
            ])
            ->add('address', 'text', [
                'label' => 'Alamat',
                'attr' => ['required' => '']
            ])
            ->add('subdistrict', 'text', [
                'label' => 'Kecamatan',
                'attr' => ['required' => '']
            ])
            ->add('city', 'text', [
                'label' => 'Kota',
                'attr' => ['required' => '']
            ])
            ->add('province', 'text', [
                'label' => 'Provinsi',
                'attr' => ['required' => '']
            ])
            ->add('country', 'text', [
                'label' => 'Negara',
                'attr' => ['required' => '']
            ])
            ->add('email', 'email', [
                'attr' => ['required' => '']
            ])
            ->add('password', 'password', [
                'attr' => ['required' => '']
            ])
            ->add('file_foto', 'file', [
                'label' => 'Foto'
            ])
            ->add('is_validated', 'checkbox', [
                'value' => 1,
                'label' => 'Diterima'
            ])
            ->add('is_suspended', 'checkbox', [
                'value' => 1,
                'label' => 'Suspend'
            ])
            ->add('suspended_reason', 'text', [
                'label' => 'Reason',
                'attr' => [
                    'required' => '',
                    // 'id' => 'hidden',
                ],
                'wrapper' => ['class' => 'row clearfix hidden-reason']
            ]);
    }
}
