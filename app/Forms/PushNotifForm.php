<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class PushNotifForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('title', 'text', [
                'label' => 'Judul',
                'attr' => ['required' => '']
            ])
            ->add('content', 'textarea', [
                'label' => 'Isi Berita',
                'attr' => [
	                'required' => '',
                ]
            ])
            // ->add('feature_source', 'text', [
            //     'label' => 'Sumber',
            //     'attr' => ['required' => '']
            // ])
            ->add('published_date', 'text', [
                'label' => 'Tanggal',
                'attr' => [
                    'required' => '',
                    'class' => 'datetimepicker form-control'
                ]
            ]);
    }
}
