<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class BankAccountForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('bank_name', 'text', [
                'label' => 'Nama Bank',
                'attr' => ['required' => '']
            ])
            ->add('account_name', 'text', [
                'label' => 'Nama Akun',
                'attr' => ['required' => '']
            ])
            ->add('file_logo', 'file', [
                'label' => 'Logo'
            ])
            ->add('description', 'textarea', [
                'label' => 'Keterangan',
                'attr' => ['required' => '']
            ])
            ->add('account_no', 'text', [
                'label' => 'Nomor Rekening',
                'attr' => ['required' => '']
            ])
            ->add('is_active', 'choice', [
                'label' => 'Status',
                'attr' => ['required' => ''],
                'choices' => [1 => 'AKTIF', 0 => 'TIDAK AKTIF'],
                'choice_options' => [
                    'wrapper' => ['class' => 'radio'],
                    'label_attr' => ['class' => 'col-lg-10 col-md-10 col-sm-8 col-xs-7'],
                ],
                // 'choice_options' => [
                //     'wrapper' => ['class' => 'choice-wrapper'],
                //     'label_attr' => ['class' => 'label-class'],
                // ],
                // 'selected' => ['en', 'fr'],
                'expanded' => true,
                'multiple' => false
            ]);
    }
}
