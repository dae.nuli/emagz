<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use App\Models\Role;
use App\Models\UserRole;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'photo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'cfg_user_role', 'user_id', 'cfg_role_id');
    }

    public function hasAnyRole($role)
    {
        if (is_array($role)) {
            foreach ($role as $value) {
                if ($this->hasRole($value)) {
                    return true;
                }
            }
        } else {
            if ($this->hasRole($value)) {
                return true;
            }
        }
        return false;
    }

    public function checkRole($user)
    {
        $ur = UserRole::where('user_id', $user)->first();
        $role = Role::where('id', $ur->cfg_role_id)->first();
        return $role->role;
    }

    public function hasRole($role)
    {
        if ($this->roles()->where('role', $role)->first()) {
            return true;
        }
        return false;
    }

    public function dae()
    {
        return 'Nuli';
    }

    public function cachedRoles()
    {
        $userPrimaryKey = $this->primaryKey;
        $cacheKey = 'entrust_roles_for_user_'.$this->$userPrimaryKey;
        return Cache::tags('cfg_user_role')->remember($cacheKey, Config::get('cache.ttl'), function () {
            return $this->roles()->get();
        });
    }


    public function can($permission, $requireAll = false)
    {
        if (is_array($permission)) {
            foreach ($permission as $permName) {
                $hasPerm = $this->can($permName);
                // return $hasPerm;
                if ($hasPerm && !$requireAll) {
                    return true;
                } elseif (!$hasPerm && $requireAll) {
                    return false;
                }
            }

            // If we've made it this far and $requireAll is FALSE, then NONE of the perms were found
            // If we've made it this far and $requireAll is TRUE, then ALL of the perms were found.
            // Return the value of $requireAll;
            return $requireAll;
        } else {
            foreach ($this->cachedRoles() as $role) {
                // Validate against the Permission table
                foreach ($role->cachedPermissions() as $perm) {
                    if (str_is( $permission, $perm->module) ) {
                        return true;
                    }
                }
            }
        }

        return false;
    }
}
