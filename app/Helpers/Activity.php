<?php

namespace App\Helpers;
use App\Models\LogAdmin;
use Illuminate\Support\Facades\Auth;

class Activity
{
    // public function __construct(LogAdmin $table)
    // {
    //     $this->table = $table;
    // }

	public static function log($action, $menu, $title = null)
	{
		return LogAdmin::create([
			'user_id' => Auth::user()->id,
			'activity' => self::save($action, $menu, $title)
			// 'page_url' => url()->current()
		]);
	}

	public static function save($action, $menu, $title)
	{
		if ($action == 'create') {
			return ($title) ? $menu.' : "'.$title.'" was created' : $menu.' was created';
		} elseif ($action == 'edit') {
			return ($title) ? $menu.' : "'.$title.'" was updated' : $menu.' was updated';
		} elseif ($action == 'delete') {
			return ($title) ? $menu.' : "'.$title.'" was deleted' : $menu.' was deleted';
		} elseif ($action == 'topup_rejected') {
			return ($title) ? $menu.' : "'.$title.'" was rejected' : $menu.' was rejected';
		} elseif ($action == 'topup_approved') {
			return ($title) ? $menu.' : "'.$title.'" was approved' : $menu.' was approved';
		} elseif ($action == 'banned') {
			return ($title) ? $menu.' : "'.$title.'" was banned' : $menu.' was banned';
		} else {
			return '-';
		}
	}
}