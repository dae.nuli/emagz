<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;

class Role extends Model
{
    protected $table = 'cfg_role';
	public $timestamps = false;
	
    protected $fillable = [
	    'role', 'description'
    ];

    public function users()
    {
        return $this->belongsToMany(User::class, 'cfg_user_role', 'cfg_role_id', 'user_id');
    }
    // public function perms()
    // {
    //     return $this->belongsToMany(Module::class, RoleModule::class);
    // }

    public function cachedPermissions()
    {
        $rolePrimaryKey = $this->primaryKey;
        $cacheKey = 'entrust_permissions_for_role_'.$this->$rolePrimaryKey;
        return Cache::tags('cfg_role_module')->remember($cacheKey, Config::get('cache.ttl'), function () {
            return $this->perms()->get();
        });
    }

    public function perms()
    {
        return $this->belongsToMany(Module::class, 'cfg_role_module', 'cfg_role_id', 'cfg_module_id');
    }
}
