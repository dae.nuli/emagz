<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $table = 'bp_rating';
    protected $dates = ['deleted_at'];

    protected $fillable = [
	    'bp_user_id', 'bp_article_id', 'rating', 'created_by', 'updated_by'
    ];

    public function article()
    {
    	return $this->belongsTo(Article::class, 'bp_article_id');
    }

    public function bpuser()
    {
    	return $this->belongsTo(BpUser::class, 'bp_user_id');
    }
}