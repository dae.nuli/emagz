<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoleModule extends Model
{
    protected $table = 'cfg_role_module';
	public $timestamps = false;
    protected $primaryKey = null;
    public $incrementing = false;
	
    protected $fillable = [
	    'cfg_role_id', 'cfg_module_id'
    ];
}
