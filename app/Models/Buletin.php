<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class Buletin extends Model
{
    use SoftDeletes;
    protected $table = 'bp_buletin';
    protected $dates = ['deleted_at'];

    protected $fillable = [
	    'user_id', 'buletin_code', 'edition', 'buletin_title', 'price', 'tag', 'filename', 
	    'thumbnail_filename', 'purchased_count', 'is_free', 'is_published', 'date_published', 
	    'created_by', 'updated_by'
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($model) {
            foreach ($model->attributes as $key => $value) {
                $model->{$key} = (empty($value) && $value != 0) ? null : $value;
            }
        });

        static::creating(function ($model) {

        	$model->user_id = Auth::id();
        	$model->date_published = Carbon::parse($model->attributes['date_published'])->format('Y-m-d H:i:s');
        	$model->created_by = Auth::user()->name;

        });

        static::updating(function ($model) {
        	$model->date_published = Carbon::parse($model->attributes['date_published'])->format('Y-m-d H:i:s');
        	$model->updated_by = Auth::user()->name;
        });
    }

    // public function article()
    // {
    //     return $this->hasMany(Article::class,'bp_buletin_id','id');
    //     // return $this->hasMany(Article::class);
    // }

    public function notif()
    {
        return $this->hasMany(BuletinNotif::class, 'bp_buletin_id');
    }

    public function order()
    {
        return $this->hasMany(Order::class, 'bp_buletin_id');
    }

    public function article()
    {
        return $this->hasMany(Article::class, 'bp_buletin_id');
    }

    public function getArtikel()
    {
        return $this->hasOne(Article::class, 'bp_buletin_id');
    }

    public function bestArticle()
    {
        $rate = $this->hasMany(Article::class, 'bp_buletin_id')->with('rate')->get();
        // $arr_rate = $rate->map(function ($rat) {
        //     return ;
        // });
        // return $this->hasMany(Article::class, 'bp_buletin_id')->with('rate')->get();
        // foreach ($rate as $key => $value) {
        //     # code...
        // }
    }

}