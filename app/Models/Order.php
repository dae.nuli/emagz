<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
class Order extends Model
{
    protected $table = 'bp_order';

    protected $fillable = [
	    'bp_user_id', 'bp_buletin_id', 'order_code', 'amount', 'purchase_date', 
	    'purchase_success_token', 'is_purchase_succeeded', 'payload',
	    'created_by', 'updated_by'
    ];

    public function buletin()
    {
    	return $this->belongsTo(Buletin::class, 'bp_buletin_id');
    }

    public function getRate($buletinId)
    {
        $rate = Rating::select('bp_article_id', DB::raw('avg(rating) as average'))
                    ->groupBy('bp_article_id')
                    ->whereHas('article', function ($qq) use ($buletinId) {
                        $qq->whereHas('buletin', function ($rr) use ($buletinId) {
                            $rr->where('id', $buletinId);
                        });
                    })
                    ->get();
            return count($rate) ? number_format($rate->max('average'), 1) : 0;
                    // return $rate->max('average');
    }

    public function getArticle($buletinId)
    {
        $rate = Rating::select('bp_article_id', DB::raw('avg(rating) as average'))
                    ->groupBy('bp_article_id')
                    ->whereHas('article', function ($qq) use ($buletinId) {
                        $qq->whereHas('buletin', function ($rr) use ($buletinId) {
                            $rr->where('id', $buletinId);
                        });
                    })
                    ->get();
            $rat = $rate->map(function ($qr) {
                    return [
                    // 'buletin_id' => $qr->article->bp_buletin_id,
                    // 'artikel_id' => $qr->bp_article_id,
                    'judul_artikel' => $qr->article->title,
                    'average_rating' => $qr->average,
                    ];
                });
            $articles = $rat->where('average_rating', $rat->max('average_rating'))->first();
            return !empty($articles) ? $articles['judul_artikel'] : '-';
    }
}