<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogUserSession extends Model
{
    protected $table = 'log_user_session';

    protected $fillable = [
	    'bp_user_id', 'token', 'device_id', 'login_date', 
	    'logout_date', 'is_active', 'created_by', 'updated_by'
    ];
}
