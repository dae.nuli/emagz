<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewsNotif extends Model
{
    protected $table = 'user_news_notification';

    protected $fillable = [
	    'bp_user_id', 'bp_news_id'
    ];

    public function news()
    {
    	return $this->belongsTo(News::class, 'bp_news_id');
    }
}
