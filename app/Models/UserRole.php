<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    protected $table = 'cfg_user_role';
	public $timestamps = false;
	public $incrementing=false;
    protected $primaryKey = null;
    protected $fillable = [
	    'user_id', 'cfg_role_id'
    ];

    // public function bpuser()
    // {
    // 	return $this->belongsTo(BpUser::class);
    // }

    // public function role()
    // {
    // 	return $this->belongsTo(Role::class);
    // }
}
