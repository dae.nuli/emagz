<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PushNotif extends Model
{
    protected $table = 'bp_push_notif';

    protected $fillable = [
	    'user_id', 'title', 'content', 'is_published', 'published_date', 
	    'topic', 'feature_source', 'created_by', 'updated_by'
    ];

    // protected static function boot()
    // {
    //     parent::boot();

    //     static::creating(function ($model) {

    //     	$model->user_id = Auth::id();
    //     	// $model->date_published = Carbon::parse($model->attributes['date_published'])->format('Y-m-d H:i:s');
    //     	$model->published_date = Carbon::parse($model->attributes['date_published'])->format('Y-m-d H:i:s');
    //     	$model->created_by = Auth::user()->name;

    //     });

    //     static::updating(function ($model) {
    //     	$model->published_date = Carbon::parse($model->attributes['date_published'])->format('Y-m-d H:i:s');
    //     	$model->updated_by = Auth::user()->name;
    //     });
    // }
}






