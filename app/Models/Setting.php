<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = 'cfg_setting';
  
    protected $fillable = [
	    'variable', 'value', 'created_by', 'updated_by'
    ];

   
}