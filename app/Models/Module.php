<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $table = 'cfg_module';
	public $timestamps = false;

    protected $fillable = [
	    'module', 'description'
    ];

    public function isModule($roleId, $moduleId)
    {
    	return RoleModule::where('cfg_role_id', $roleId)
		    	->where('cfg_module_id', $moduleId)
		    	->count();
    }
}
