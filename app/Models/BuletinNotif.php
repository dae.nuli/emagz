<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BuletinNotif extends Model
{
    protected $table = 'user_buletin_notification';

    protected $fillable = [
	    'bp_user_id', 'bp_buletin_id'
    ];
    
    public function buletin()
    {
    	return $this->belongsTo(Buletin::class, 'bp_buletin_id');
    }
}
