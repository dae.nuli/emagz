<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use App\User;

class MessageReply extends Model
{
    protected $table = 'bp_message_reply';

    protected $fillable = [
	    'bp_message_id', 'user_id', 'content', 'is_admin', 'is_read',
	    'created_by', 'updated_by'
    ];

    public function user($userId, $isAdmin)
    {
    	if ($isAdmin) {
	        $user = User::find($userId);
	        return !empty($user) ? $user->name : '-';
    	} else {
	        $user = BpUser::find($userId);
	        return !empty($user) ? $user->full_name : '-';
    	}
    }
}