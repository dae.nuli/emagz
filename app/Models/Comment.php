<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Comment extends Model
{
    protected $table = 'bp_comment';

    protected $fillable = [
	    'bp_user_id', 'bp_article_id', 'comment', 'is_approved', 'approved_date', 'created_by', 'updated_by'
    ];

    public function article()
    {
    	return $this->belongsTo(Article::class, 'bp_article_id');
    }

    public function bpuser()
    {
    	return $this->belongsTo(BpUser::class, 'bp_user_id');
    }

    public function countRating($article, $user)
    {
        return Rating::where('bp_article_id', $article)
                ->where('bp_user_id', $user)
                ->first();
    }
}