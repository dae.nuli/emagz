<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class LogAdmin extends Model
{
    protected $table = 'log_admin';

    protected $fillable = [
	    'user_id', 'activity', 'page_url'
    ];

    public function user()
    {
    	return $this->belongsTo(User::class, 'user_id');
    }
}
