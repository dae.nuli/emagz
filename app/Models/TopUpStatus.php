<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TopUpStatus extends Model
{
    protected $table = 'cfg_topup_status';

    protected $fillable = [
	    'topup_status', 'is_default'
    ];
}
