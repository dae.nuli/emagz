<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Article extends Model
{
    protected $table = 'bp_article';

    protected $fillable = [
	    'bp_buletin_id', 'title', 'description', 'rating', 
	    'comment_count', 'thumbnail', 'author',
	    'created_by', 'updated_by'
    ];
    // protected $hidden = ['id', 'bp_buletin_id','thumbnail', 'created_by', 'updated_by', 'updated_at', 'deleted_at'];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {

        	$model->created_by = Auth::user()->name;

        });

        static::updating(function ($model) {
        	$model->updated_by = Auth::user()->name;
        });
    }

    public function buletin()
    {
    	return $this->belongsTo(Buletin::class, 'bp_buletin_id');
    }
 
    public function comment()
    {
        return $this->hasMany(Comment::class, 'bp_article_id');
        // return $this->hasMany(Comment::class, 'id', 'bp_article_id');
    }
 
    public function rate()
    {
        return $this->hasMany(Rating::class, 'bp_article_id');
        // return $this->hasMany(Comment::class, 'id', 'bp_article_id');
    }

    public function rateByUser($id, $user)
    {
        return $this->hasOne(Rating::class, 'bp_article_id')->where('bp_user_id', $user)->where('bp_article_id', $id)->first();
    }

    public function rateByArticle($id)
    {
        return $this->hasOne(Rating::class, 'bp_article_id')->where('bp_article_id', $id)->count();
    }
}