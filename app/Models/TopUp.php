<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TopUp extends Model
{
    protected $table = 'bp_topup';

    protected $fillable = [
	    'bp_user_id', 'bank_id', 'topup_code', 'amount', 'topup_date', 'account_name', 'bank_name', 
        'confirmation_date', 'file_attachment', 'cfg_topup_type_id',
	    // 'confirmation_date', 'file_attachment', 'cfg_topup_type_id', 'cfg_topup_status_id',
	    'created_by', 'updated_by'
    ];

    public function users()
    {
        return $this->belongsTo(BpUser::class, 'bp_user_id');
    }

    public function type()
    {
        return $this->belongsTo(TopUpType::class, 'cfg_topup_type_id');
    }

    public function checkStatus($status)
    {
        if($status == 2) {
            return 'accepted';
        } elseif ($status == 0) {
            return 'new';
        } elseif ($status == 1) {
            return 'rejected';
        }
        // return $this->belongsTo(TopUpStatus::class, 'cfg_topup_status_id');
    }

    // public function status()
    // {
    //     return $this->belongsTo(TopUpStatus::class, 'cfg_topup_status_id');
    // }
}