<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class BpUser extends Authenticatable
{
	use Notifiable;
    protected $table = 'bp_users';

    protected $fillable = [
	    'uid', 'full_name', 'email', 'amount_balance', 
	    'phone_number', 'password', 'photo_filename', 'address', 'subdistrict', 
	    'city', 'province', 'country', 'is_validated', 'is_active', 'is_suspended', 
	    'suspended_reason', 'created_by', 'updated_by'
    ];

    protected $hidden = ['id', 'password', 'created_by', 'updated_by', 'remember_token', 'created_at', 'updated_at'];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($model) {
            foreach ($model->attributes as $key => $value) {
                $model->{$key} = (empty($value) && $value != 0) ? null : $value;
            }
        });
    }

}
