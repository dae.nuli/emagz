<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class TermCondition extends Model
{
    protected $table = 'term_condition';

    protected $fillable = [
	    'user_id', 'version', 'author', 'content', 'status', 'date_published', 'created_by', 'updated_by'
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {

        	$model->user_id = Auth::id();
        	$model->date_published = Carbon::parse($model->attributes['date_published'])->format('Y-m-d H:i:s');
        	$model->created_by = Auth::user()->name;

        });

        static::updating(function ($model) {
        	$model->date_published = Carbon::parse($model->attributes['date_published'])->format('Y-m-d H:i:s');
        	$model->updated_by = Auth::user()->name;
        });
    }

}