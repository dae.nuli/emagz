<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TopUpType extends Model
{
    protected $table = 'cfg_topup_type';

    protected $fillable = [
	    'topup_type', 'description', 'is_default'
    ];
}
