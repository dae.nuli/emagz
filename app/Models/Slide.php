<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Slide extends Model
{
    protected $table = 'cfg_slide';

    protected $fillable = [
	    'title', 'filename', 'description','is_active', 'created_by', 'updated_by'
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($model) {
            foreach ($model->attributes as $key => $value) {
                $model->{$key} = (empty($value) && $value != 0) ? null : $value;
            }
        });

        static::creating(function ($model) {
        	$model->created_by = Auth::user()->name;
        });

        static::updating(function ($model) {
        	$model->updated_by = Auth::user()->name;
        });
    }
}
