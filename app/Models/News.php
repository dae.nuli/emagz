<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class News extends Model
{
    use SoftDeletes;
    protected $table = 'bp_news';
    protected $dates = ['deleted_at'];

    protected $fillable = [
	    'user_id', 'title', 'content', 'tag', 'is_published', 'date_published', 'thumbnail', 'created_by', 'updated_by'
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($model) {
        // 	// dd(Auth::id());
        // 	$model->user_id = Auth::id();
        //     // foreach ($model->attributes as $key => $value) {
        //     //     $model->{$key} = (empty($value) && $value != 0) ? null : $value;
        //     // }
        });

        static::creating(function ($model) {

        	$model->user_id = Auth::id();
        	$model->date_published = Carbon::parse($model->attributes['date_published'])->format('Y-m-d H:i:s');
        	$model->created_by = Auth::user()->name;

        });

        static::updating(function ($model) {
        	$model->date_published = Carbon::parse($model->attributes['date_published'])->format('Y-m-d H:i:s');
        	$model->updated_by = Auth::user()->name;
        });
    }

    public function notifS()
    {
        return $this->hasMany(NewsNotif::class, 'bp_news_id');
    }

    // public function getDatePublishedAttribute($value)
    // {
    //     return Carbon::parse($value)->format('d F Y, H:i:s');
    // }
}
