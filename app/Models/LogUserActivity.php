<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogUserActivity extends Model
{
    protected $table = 'log_user_activity';
}
