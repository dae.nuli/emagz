<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends Model
{
    protected $table = 'bp_message';

    protected $fillable = [
	    'bp_user_id', 'title', 'content', 'is_read', 
	    'created_by', 'updated_by'
    ];

    public function reply()
    {
        return $this->hasMany(MessageReply::class, 'bp_message_id');
    }

    public function bpUser()
    {
        return $this->belongsTo(BpUser::class, 'bp_user_id');
    }
}