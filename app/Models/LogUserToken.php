<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogUserToken extends Model
{
    protected $table = 'log_user_token';

    protected $fillable = [
	    'bp_user_id', 'token', 'code_activation', 'expired_date', 
	    'is_taken', 'created_by', 'updated_by'
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($model) {
            foreach ($model->attributes as $key => $value) {
                $model->{$key} = (empty($value) && $value != 0) ? null : $value;
            }
        });
    }
}
